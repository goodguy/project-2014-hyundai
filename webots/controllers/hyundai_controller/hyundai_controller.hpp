#include "server.hpp"
#include "packet.hpp"

#include <webots/Robot.hpp>
#include <webots/Motor.hpp>
#include <webots/InertialUnit.hpp>
#include <webots/DistanceSensor.hpp>
#include <webots/PositionSensor.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <iostream>
#include <numeric>
#include <mutex>
#include <vector>


namespace goodguy{

    const std::size_t webots_bot_wheel_num = 4;
    const std::size_t webots_upp_joint_num = 2;

    // minimum error to notify main controller that ideal angles are reached
    const double Ess = 0.5 * M_PI / 180.0;

    // Webots controller class
    class hyundai_controller : public webots::Robot {

        public:
            // laser sensor ID index 
            enum SID{
                SID_LOWER,
                SID_UPPER_1,
                SID_UPPER_2
            };

            // session type to communicate with main controller of each kind of module 
            enum session_type{
                LOWER,
                UPPER_RIGHT,
                UPPER_LEFT,
                LASER_RIGHT,
                LASER_LEFT,
                LASER_BOTTOM,
                IMU
            };

        public:

            // constructor
            hyundai_controller()
                : Robot(), m_lower_session(NULL), m_upper_l_session(NULL), m_upper_r_session(NULL) 
            {
                // initialize joing angle and velocity to zero
                for(std::size_t i = 0; i < webots_upp_joint_num; ++i){
                    m_ideal_joint_ang_l[i] = 0;   
                    m_ideal_joint_ang_r[i] = 0;   
                    m_ideal_joint_vel_l[i] = 0;   
                    m_ideal_joint_vel_r[i] = 0;   
                }
                m_ideal_wheel_vel = 0;
                m_is_initialize_wheel = false;
                m_is_initialize_joint_l = false;
                m_is_initialize_joint_r = false;


                std::string l_wheel_header_str("up_down_motor_l_");
                std::string r_wheel_header_str("up_down_motor_r_");

                // get Position sensor object in webots world 
                m_wheel_pos_l = getPositionSensor("up_down_motor_pos_l_1");
                m_wheel_pos_l->enable(getBasicTimeStep());

                double acc = -1;
                // get Motor object in webots world 
                for(std::size_t i = 0; i < webots_bot_wheel_num; ++i){
                    std::string idx_str = boost::lexical_cast<std::string>(i+1);
                    m_wheel_l[i] = getMotor(l_wheel_header_str+idx_str);
                    m_wheel_r[i] = getMotor(r_wheel_header_str+idx_str);
                    m_wheel_l[i]->setAcceleration(acc);
                    m_wheel_r[i]->setAcceleration(acc);
                }

                std::string l_joint_header_str("laser_arm_l_");
                std::string r_joint_header_str("laser_arm_r_");
                std::string l_joint_pos_header_str("laser_arm_pos_l_");
                std::string r_joint_pos_header_str("laser_arm_pos_r_");

                // get Motor and Motor's Position sensor objects in webots world 
                for(std::size_t i = 0; i < webots_upp_joint_num; ++i){
                    std::string idx_str = boost::lexical_cast<std::string>(i+1);
                    m_joint_l[i] = getMotor(l_joint_header_str+idx_str);
                    m_joint_r[i] = getMotor(r_joint_header_str+idx_str);
                    m_joint_pos_l[i] = getPositionSensor(l_joint_pos_header_str+idx_str);
                    m_joint_pos_r[i] = getPositionSensor(r_joint_pos_header_str+idx_str);

                    m_joint_pos_l[i]->enable(getBasicTimeStep());
                    m_joint_pos_r[i]->enable(getBasicTimeStep());
                }

                std::string laser_sensor_str_l("laser_sensor_l");
                std::string laser_sensor_str_r("laser_sensor_r");
                std::string laser_sensor_str_b("laser_sensor_b");


                // get Laser sensor object in webots world
                m_laser_l = getDistanceSensor(laser_sensor_str_l);
                m_laser_l->enable(getBasicTimeStep());
                m_laser_r = getDistanceSensor(laser_sensor_str_r);
                m_laser_r->enable(getBasicTimeStep());
                m_laser_b = getDistanceSensor(laser_sensor_str_b);
                m_laser_b->enable(getBasicTimeStep());

                std::string imu_str("imu");
                // get IMU sensor object in webots world
                m_imu = getInertialUnit(imu_str);
                m_imu->enable(getBasicTimeStep());
            }

            // Set velocity to robot's up-down motion
            void setVelocityBot(double vel){
                for(std::size_t i = 0; i < webots_bot_wheel_num; ++i){
                    m_wheel_l[i]->setPosition(std::numeric_limits<double>::infinity());
                    m_wheel_r[i]->setPosition(std::numeric_limits<double>::infinity());
                    m_wheel_l[i]->setVelocity(vel);
                    m_wheel_r[i]->setVelocity(vel);
                }
            }

            // send imu data to main-controller
            void sendImuData(session_type type, double roll, double pitch, double yaw);

            // send laser data to main-controller
            void sendLaserData(session_type type, double distance);

            // send velocity data of bottom module to main-controller
            void sendVelocity(session_type type, double velocity);

            // send velocity data of upper module to main-controller
            void sendVelocities(session_type type, double vel0, double vel1);

            // send angle data of upper module to main-controller
            void sendAngles(session_type type, double ang0, double ang1);

            // send echo reply to main-controller
            void sendEchoTest(session_type type, const std::string& echo);

            // send complete message to main-controller
            void sendSlaveComplete(session_type type, PacketType packet_type);

            // destructor
            ~hyundai_controller() { };    

            // main control function
            void run();

            // get session pointer of each kind of its type
            session** getLowerSession() { return &m_lower_session; }
            session** getUpperLeftSession() { return &m_upper_l_session; }
            session** getUpperRightSession() { return &m_upper_r_session; }
            session** getLaserLeftSession() { return &m_laser_l_session; }
            session** getLaserRightSession() { return &m_laser_r_session; }
            session** getLaserBottomSession() { return &m_laser_b_session; }
            session** getImuSession() { return &m_imu_session; }


        private:

            // parsing received packet from main-controller
            void parsingPacket(std::vector<unsigned char>& buffer, session_type type);
            // if received echo message
            void routineForEcho(const std::shared_ptr<PacketEcho<std::vector<unsigned char>>> packet, session_type type);
            // if received echo-reply message
            void routineForEchoTest(const std::shared_ptr<PacketEchoTest<std::vector<unsigned char>>> packet, session_type type);
            // if received initialize message
            void routineForInitialize(const std::shared_ptr<PacketInitialize> packet, session_type type);
            // if received move to angles message
            void routineForHostToSlaveAng(const std::shared_ptr<PacketHostToSlaveAng> packet, session_type type);
            // if received angle status message
            void routineForSlaveToHostAng(const std::shared_ptr<PacketSlaveToHostAng> packet, session_type type);
            // if received move to up-down message
            void routineForHostToSlaveVel(const std::shared_ptr<PacketHostToSlaveVel> packet, session_type type);
            // if received message that move to angle with specific velocity 
            void routineForHostToSlaveVel2(const std::shared_ptr<PacketHostToSlaveVel2> packet, session_type type);
            // if received velocity status message
            void routineForSlaveToHostVel(const std::shared_ptr<PacketSlaveToHostVel> packet, session_type type);
            // if received velocities status message
            void routineForSlaveToHostVel2(const std::shared_ptr<PacketSlaveToHostVel2> packet, session_type type);
            // if received ack message
            void routineForAck(const std::shared_ptr<PacketAck> packet, session_type type);
            // if received slave complete message
            void routineForSlaveComplete(const std::shared_ptr<PacketSlaveComplete> packet, session_type type);

        private:
            session* m_lower_session;
            session* m_upper_l_session;
            session* m_upper_r_session;

            session* m_laser_l_session;
            session* m_laser_r_session;
            session* m_laser_b_session;

            session* m_imu_session;

            std::vector<unsigned char> m_received_lower_packet_buffer;
            std::vector<unsigned char> m_received_upper_l_packet_buffer;
            std::vector<unsigned char> m_received_upper_r_packet_buffer;

            std::vector<unsigned char> m_received_laser_l_packet_buffer;
            std::vector<unsigned char> m_received_laser_r_packet_buffer;
            std::vector<unsigned char> m_received_laser_b_packet_buffer;

        private:
            double m_ideal_joint_ang_l[webots_upp_joint_num];
            double m_ideal_joint_ang_r[webots_upp_joint_num];

            double m_ideal_joint_vel_l[webots_upp_joint_num];
            double m_ideal_joint_vel_r[webots_upp_joint_num];

            double m_ideal_wheel_vel;

            bool m_is_initialize_joint_l;
            bool m_is_initialize_joint_r;
            bool m_is_initialize_wheel;

        private:
            webots::Motor* m_wheel_l[webots_bot_wheel_num];
            webots::Motor* m_wheel_r[webots_bot_wheel_num];

            webots::Motor* m_joint_l[webots_upp_joint_num];
            webots::Motor* m_joint_r[webots_upp_joint_num];

            webots::PositionSensor* m_wheel_pos_l;
            webots::PositionSensor* m_joint_pos_l[webots_upp_joint_num];
            webots::PositionSensor* m_joint_pos_r[webots_upp_joint_num];

            webots::DistanceSensor* m_laser_l;
            webots::DistanceSensor* m_laser_r;
            webots::DistanceSensor* m_laser_b;

            webots::InertialUnit* m_imu;

    };

};

