#include "hyundai_controller.hpp"

namespace goodguy{


    // main control function of webots controller
    void hyundai_controller::run() {

        // save previous joint position (left, right 2DOF)
        double prev_pos_wheel = m_wheel_pos_l->getValue() * 180.0 / M_PI;
        double prev_pos_joint_l[2]; 
        prev_pos_joint_l[0] = m_joint_pos_l[0]->getValue() * 180.0 / M_PI;
        prev_pos_joint_l[1] = m_joint_pos_l[1]->getValue() * 180.0 / M_PI;

        double prev_pos_joint_r[2]; 
        prev_pos_joint_r[0] = m_joint_pos_r[0]->getValue() * 180.0 / M_PI;
        prev_pos_joint_r[1] = m_joint_pos_r[1]->getValue() * 180.0 / M_PI;

        // Control is conducted every basic time step
        while (step(getBasicTimeStep()) != -1) {

            // get current wheel position
            double curr_pos_wheel = m_wheel_pos_l->getValue() * 180.0 / M_PI;

            // get current joint position of left
            double curr_pos_joint_l[2]; 
            curr_pos_joint_l[0] = m_joint_pos_l[0]->getValue() * 180.0 / M_PI;
            curr_pos_joint_l[1] = m_joint_pos_l[1]->getValue() * 180.0 / M_PI;

            // get current joint position of right
            double curr_pos_joint_r[2]; 
            curr_pos_joint_r[0] = m_joint_pos_r[0]->getValue() * 180.0 / M_PI;
            curr_pos_joint_r[1] = m_joint_pos_r[1]->getValue() * 180.0 / M_PI;

            double dt = ((double)getBasicTimeStep()) / 1000.0;

            // calculate current wheel's velocity
            double vel_wheel = ((curr_pos_wheel - prev_pos_wheel)/ dt) / (360.0) * 60.0; 
            double vel_joint_l[2];
            double vel_joint_r[2];
            vel_joint_l[0] = ((curr_pos_joint_l[0] - prev_pos_joint_l[0])/ dt) / (360.0) * 60.0; 
            vel_joint_l[1] = ((curr_pos_joint_l[1] - prev_pos_joint_l[1])/ dt) / (360.0) * 60.0; 
            vel_joint_r[0] = ((curr_pos_joint_r[0] - prev_pos_joint_r[0])/ dt) / (360.0) * 60.0; 
            vel_joint_r[1] = ((curr_pos_joint_r[1] - prev_pos_joint_r[1])/ dt) / (360.0) * 60.0; 

            // save previous joint positions
            prev_pos_wheel = curr_pos_wheel;
            prev_pos_joint_l[0] = curr_pos_joint_l[0];
            prev_pos_joint_l[1] = curr_pos_joint_l[1];
            prev_pos_joint_r[0] = curr_pos_joint_r[0];
            prev_pos_joint_r[1] = curr_pos_joint_r[1];

            if(m_lower_session != NULL){
                std::vector<unsigned char> data = m_lower_session->getReceivedBuffer();
                m_received_lower_packet_buffer.insert(m_received_lower_packet_buffer.end(), data.begin(), data.end());

                // parsing received packet from main controller for lower module 
                parsingPacket(m_received_lower_packet_buffer, hyundai_controller::session_type::LOWER);

                // send lower velocity to main controller
                sendVelocity(hyundai_controller::session_type::LOWER, vel_wheel);
            }
            if(m_upper_l_session != NULL){
                std::vector<unsigned char> data = m_upper_l_session->getReceivedBuffer();
                m_received_upper_l_packet_buffer.insert(m_received_upper_l_packet_buffer.end(), data.begin(), data.end());

                // parsing received packet from main controller for upper left module 
                parsingPacket(m_received_upper_l_packet_buffer, hyundai_controller::session_type::UPPER_LEFT);

                // send upper left velocities to main controller
                sendVelocities(hyundai_controller::session_type::UPPER_LEFT, vel_joint_l[0], vel_joint_l[1]);

                // send upper left angles to main controller
                sendAngles(hyundai_controller::session_type::UPPER_LEFT, curr_pos_joint_l[0], curr_pos_joint_l[1]);
            }
            if(m_upper_r_session != NULL){
                std::vector<unsigned char> data = m_upper_r_session->getReceivedBuffer();
                m_received_upper_r_packet_buffer.insert(m_received_upper_r_packet_buffer.end(), data.begin(), data.end());

                // parsing received packet from main controller for upper right module 
                parsingPacket(m_received_upper_r_packet_buffer, hyundai_controller::session_type::UPPER_RIGHT);

                // send upper right velocities to main controller
                sendVelocities(hyundai_controller::session_type::UPPER_RIGHT, vel_joint_r[0], vel_joint_r[1]);

                // send upper right angles to main controller
                sendAngles(hyundai_controller::session_type::UPPER_RIGHT, curr_pos_joint_r[0], curr_pos_joint_r[1]);
            }
            if(m_laser_l_session != NULL){
                std::vector<unsigned char> data = m_laser_l_session->getReceivedBuffer();
                m_received_laser_l_packet_buffer.insert(m_received_laser_l_packet_buffer.end(), data.begin(), data.end());
                double distance = m_laser_l->getValue();

                // send laser data to main controller
                sendLaserData(LASER_LEFT, distance);
            }
            if(m_laser_r_session != NULL){
                std::vector<unsigned char> data = m_laser_r_session->getReceivedBuffer();
                m_received_laser_r_packet_buffer.insert(m_received_laser_r_packet_buffer.end(), data.begin(), data.end());
                double distance = m_laser_r->getValue();

                // send laser data to main controller
                sendLaserData(LASER_RIGHT, distance);
            }
            if(m_laser_b_session != NULL){
                std::vector<unsigned char> data = m_laser_b_session->getReceivedBuffer();
                m_received_laser_b_packet_buffer.insert(m_received_laser_b_packet_buffer.end(), data.begin(), data.end());
                double distance = m_laser_b->getValue();

                // send laser data to main controller
                sendLaserData(LASER_BOTTOM, distance);
            }
            if(m_imu_session != NULL){
                const double* rpy = m_imu->getRollPitchYaw();
                // send imu data to main controller
                sendImuData(IMU, rpy[0], rpy[1], rpy[2]);
            }

            // set joint angles and velocities in webots robot model
            for(std::size_t i = 0; i < webots_upp_joint_num; ++i){
                m_joint_l[i]->setVelocity(m_ideal_joint_vel_l[i]);
                m_joint_l[i]->setPosition(m_ideal_joint_ang_l[i]);
                m_joint_r[i]->setVelocity(m_ideal_joint_vel_r[i]);
                m_joint_r[i]->setPosition(m_ideal_joint_ang_r[i]);
            }
            // set velocity of bottom module in webots robot model
            setVelocityBot(-m_ideal_wheel_vel);


            // initialize step of left upper module
            if(m_is_initialize_joint_l){
                if(std::abs(curr_pos_joint_l[0]-m_ideal_joint_ang_l[0]*180.0/M_PI) < Ess 
                    && std::abs(curr_pos_joint_l[1]-m_ideal_joint_ang_l[1]*180.0/M_PI) < Ess)
                {
                    // send slave complete to main-controller
                    sendSlaveComplete(hyundai_controller::session_type::UPPER_LEFT, PACKET_TYPE_INITIALIZE);
                    m_is_initialize_joint_l = false;
                }
            }
            
            // initialize step of right upper module
            if(m_is_initialize_joint_r){
                if(std::abs(curr_pos_joint_r[0]-m_ideal_joint_ang_r[0]*180.0/M_PI) < Ess 
                    && std::abs(curr_pos_joint_r[1]-m_ideal_joint_ang_r[1]*180.0/M_PI) < Ess)
                {
                    // send slave complete to main-controller
                    sendSlaveComplete(hyundai_controller::session_type::UPPER_RIGHT, PACKET_TYPE_INITIALIZE);
                    m_is_initialize_joint_r = false;
                }
            }
        }
    }

    // send IMU data to main-controller
    void hyundai_controller::sendImuData(session_type type, double roll, double pitch, double yaw){
        std::string packet;
        switch(type){
            case IMU:
                {
                    // packet imitation of EBIMU 
                    packet = boost::str(boost::format("*%.2f,%.2f,%.2f\r\n") % ((roll+1.5708-3.14159)*180.0/3.14159) % (pitch*180.0/3.14) % (-yaw*180.0/3.14));
                    // send packet to main-controller
                    m_imu_session->write(packet);
                }
                break;
            default:
                break;
        }
    }

    // send Laser data to main-controller
    void hyundai_controller::sendLaserData(session_type type, double distance){
        
        static double prev_distance_for_l = 0.0;
        static double prev_distance_for_r = 0.0;
        static double prev_distance_for_b = 0.0;

        std::string packet;

        // jump distance
        //const double jump_distance = 0.15 * 10000000;
        //const double jump_distance = 0.15 * 10000;
        const double jump_distance = 0.15 * 10000*10000000000000;

        // in jumping situation, data is provided after 5 seconds
        const uint32_t jump_max_count = 5 * (1000/((double)getBasicTimeStep()));
        static uint32_t count_jump_for_l = 0;
        static uint32_t count_jump_for_r = 0;
        static uint32_t count_jump_for_b = 0;

        switch(type){
            // For left laser sensor
            case LASER_LEFT:
                if(prev_distance_for_l != 0){
                    // if normal situation
                    if(std::abs(prev_distance_for_l - distance) < jump_distance){
                        // send distance data
                        packet = boost::str(boost::format("g0h+%08d\r\n") % (int)distance);
                        count_jump_for_l = 0;
                        prev_distance_for_l = distance;
                    }
                    // if jumping is occured
                    else{
                        // send error that jumping is occured
                        if(count_jump_for_l++ < jump_max_count){
                            packet = std::string("g0@E330\r\n");
                        }
                        // after 5 seconds
                        else{
                            // send normal distance data
                            packet = boost::str(boost::format("g0h+%08d\r\n") % (int)distance);
                            count_jump_for_l = 0;
                            prev_distance_for_l = distance;
                        }
                    }
                }
                else{
                    // send distance data
                    packet = boost::str(boost::format("g0h+%08d\r\n") % (int)distance);
                    prev_distance_for_l = distance;
                }
                m_laser_l_session->write(packet);
                break;
            // For right laser sensor
            case LASER_RIGHT:
                if(prev_distance_for_r != 0){
                    // if normal situation
                    if(std::abs(prev_distance_for_r - distance) < jump_distance){
                        // send distance data
                        packet = boost::str(boost::format("g1h+%08d\r\n") % (int)distance);
                        count_jump_for_r = 0;
                        prev_distance_for_r = distance;
                    }
                    // if jumping is occured
                    else{
                        // send error that jumping is occured
                        if(count_jump_for_r++ < jump_max_count){
                            packet = std::string("g1@E330\r\n");
                        }
                        // after 5 seconds
                        else{
                            packet = boost::str(boost::format("g1h+%08d\r\n") % (int)distance);
                            count_jump_for_r = 0;
                            prev_distance_for_r = distance;
                        }
                    }
                }
                else{
                    // send distance data
                    packet = boost::str(boost::format("g1h+%08d\r\n") % (int)distance);
                    prev_distance_for_r = distance;
                }
                m_laser_r_session->write(packet);
                break;
            // For bottom laser sensor
            case LASER_BOTTOM:
                if(prev_distance_for_b != 0){
                    // if normal situation
                    if(std::abs(prev_distance_for_b - distance) < jump_distance){
                        // send distance data
                        packet = boost::str(boost::format("g2h+%08d\r\n") % (int)distance);
                        count_jump_for_b = 0;
                        prev_distance_for_b = distance;
                    }
                    // if jumping is occured
                    else{
                        // send error that jumping is occured
                        if(count_jump_for_b++ < jump_max_count){
                            packet = std::string("g2@E330\r\n");
                        }
                        // after 5 seconds
                        else{
                            packet = boost::str(boost::format("g2h+%08d\r\n") % (int)distance);
                            count_jump_for_b = 0;
                            prev_distance_for_b = distance;
                        }
                    }
                }
                else{
                    // send distance data
                    packet = boost::str(boost::format("g2h+%08d\r\n") % (int)distance);
                    prev_distance_for_b = distance;
                }
                m_laser_b_session->write(packet);
                break;
            default:    break;
        }
    }

    // for send up-down velocity to main-controller
    void hyundai_controller::sendVelocity(session_type type, double velocity){
        switch(type){
            case LOWER:
                {
                    // make packet for sending up-down velocity
                    PacketSlaveToHostVel packet(SID::SID_LOWER, velocity);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_lower_session->write(packet_stream);
                    break;
                }
            default:    break;
        }
    }

    // for sending pan-tilt velocities to main-controller
    void hyundai_controller::sendVelocities(session_type type, double vel0, double vel1){

        switch(type){
            case UPPER_LEFT:
                {
                    // make packet for sending left 2-DoF velocities
                    PacketSlaveToHostVel2 packet(SID::SID_UPPER_1, vel0, vel1);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_upper_l_session->write(packet_stream);
                    break;
                }
            case UPPER_RIGHT:
                {
                    // make packet for sending right 2-DoF velocities
                    PacketSlaveToHostVel2 packet(SID::SID_UPPER_2, vel0, vel1);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_upper_r_session->write(packet_stream);
                    break;
                }
            default:    break;
        }
    }

    // for sending pan-tilt angles to main-controller
    void hyundai_controller::sendAngles(session_type type, double ang0, double ang1){

        switch(type){
            case UPPER_LEFT:
                {
                    // make packet for sending left 2-DoF angles
                    PacketSlaveToHostAng packet(SID::SID_UPPER_1, ang0, ang1);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_upper_l_session->write(packet_stream);
                    break;
                }
            case UPPER_RIGHT:
                {
                    // make packet for sending right 2-DoF angles
                    PacketSlaveToHostAng packet(SID::SID_UPPER_2, ang0, ang1);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_upper_r_session->write(packet_stream);
                    break;
                }
            default:    break;
        }
    }

    // for sending reply of echo test 
    void hyundai_controller::sendEchoTest(session_type type, const std::string& echo){
        switch(type){
            case LOWER:
                {
                    // make packet for sending echo reply
                    PacketEchoTest<std::string> packet(SID::SID_LOWER, echo);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_lower_session->write(packet_stream);
                    break;;
                }
            case UPPER_LEFT:
                {
                    // make packet for sending echo reply
                    PacketEchoTest<std::string> packet(SID::SID_UPPER_1, echo);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_upper_l_session->write(packet_stream);
                    break;
                }
            case UPPER_RIGHT:
                {
                    // make packet for sending echo reply
                    PacketEchoTest<std::string> packet(SID::SID_UPPER_2, echo);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_upper_r_session->write(packet_stream);
                    break;
                }
            default:    break;
        }
    }

    // for sending notice that slave task is completed
    void hyundai_controller::sendSlaveComplete(hyundai_controller::session_type type, PacketType packet_type){
        switch(type){
            case LOWER:
                {
                    // make packet for sending notice that slave task is completed
                    PacketSlaveComplete packet(SID::SID_LOWER, packet_type);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_lower_session->write(packet_stream);
                    break;;
                }
            case UPPER_LEFT:
                {
                    // make packet for sending notice that slave task is completed
                    PacketSlaveComplete packet(SID::SID_UPPER_1, packet_type);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_upper_l_session->write(packet_stream);
                    break;
                }
            case UPPER_RIGHT:
                {
                    // make packet for sending notice that slave task is completed
                    PacketSlaveComplete packet(SID::SID_UPPER_2, packet_type);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet
                    m_upper_r_session->write(packet_stream);
                    break;
                }
            default:    break;
        }
    }


    // routine to handle packet for echo
    void hyundai_controller::routineForEcho(const std::shared_ptr<PacketEcho<std::vector<unsigned char>>> packet, hyundai_controller::session_type type){
        // send echo reply
        sendEchoTest(type, packet->getString<std::string>());           
    }

    // routine to handle packet for echo reply
    void hyundai_controller::routineForEchoTest(const std::shared_ptr<PacketEchoTest<std::vector<unsigned char>>> packet, hyundai_controller::session_type type){
        // print echo reply 
        std::cout << "EHCO: " << packet->getString<std::string>() << std::endl;           
    }

    // routine to handle packet for initialization
    void hyundai_controller::routineForInitialize(const std::shared_ptr<PacketInitialize> packet, hyundai_controller::session_type type){
        switch(type){
            case LOWER:
                {
                    // do nothing just send message that this module finished task
                    m_ideal_wheel_vel = 0.0;
                    sendSlaveComplete(type, PACKET_TYPE_INITIALIZE);
                    break;;
                }
            case UPPER_LEFT:
                {
                    // start initialization step for left pan-tilt
                    m_ideal_joint_ang_l[0] = 0.0;
                    m_ideal_joint_ang_l[1] = 0.0;
                    m_is_initialize_joint_l = true;
                    break;
                }
            case UPPER_RIGHT:
                {
                    // start initialization step for right pan-tilt
                    m_ideal_joint_ang_r[0] = 0.0;
                    m_ideal_joint_ang_r[1] = 0.0;
                    m_is_initialize_joint_r = true;
                    break;
                }
            default:    break;
        }
    }

    // routine to handle packet for command which request move pan-tilt to target angles
    void hyundai_controller::routineForHostToSlaveAng(const std::shared_ptr<PacketHostToSlaveAng> packet, hyundai_controller::session_type type){
        switch(type){
            case UPPER_LEFT: 
                {
                    // get angles from packet
                    float pos0 = packet->getMotor1Angle() * M_PI / 180.0;
                    float pos1 = packet->getMotor2Angle() * M_PI / 180.0;
                    // set ideal pan-tilt joint angles with target angles
                    m_ideal_joint_ang_l[0] = pos0;
                    m_ideal_joint_ang_l[1] = pos1;
                    break;
                }
            case UPPER_RIGHT: 
                {
                    // get angles from packet
                    float pos0 = packet->getMotor1Angle() * M_PI / 180.0;
                    float pos1 = packet->getMotor2Angle() * M_PI / 180.0;
                    // set ideal pan-tilt joint angles with target angles
                    m_ideal_joint_ang_r[0] = pos0;
                    m_ideal_joint_ang_r[1] = pos1;
                    break;
                }
            default:    
                break;
        }
    }

    // routine to handle packet for status of pan-tilt angles
    void hyundai_controller::routineForSlaveToHostAng(const std::shared_ptr<PacketSlaveToHostAng> packet, hyundai_controller::session_type type){
        // do nothing
    }

    // routine to handle packet for command which request move up-down with target velocity
    void hyundai_controller::routineForHostToSlaveVel(const std::shared_ptr<PacketHostToSlaveVel> packet, hyundai_controller::session_type type){
        //std::cout << "Host To Slave Velocity: \t" << packet->getMotorVelocity()  << std::endl;;            
        switch(type){
            case LOWER: 
                {
                    // get up-down velocity from packet
                    float velocity = packet->getMotorVelocity();
                    // set ideal up-down velocity with target velocity
                    m_ideal_wheel_vel = velocity;
                }
            default:    
                break;
        }
    }

    // routine to handle packet for command which request move pan-tilt with target velocity
    void hyundai_controller::routineForHostToSlaveVel2(const std::shared_ptr<PacketHostToSlaveVel2> packet, hyundai_controller::session_type type){
        switch(type){
            case UPPER_LEFT: 
                {
                    // get pan-tilt velocities from packet
                    float vel0 = packet->getMotor1Velocity() * M_PI / 180;
                    float vel1 = packet->getMotor2Velocity() * M_PI / 180;
                    // set pan-titl velocities with target velocities
                    m_ideal_joint_vel_l[0] = vel0;
                    m_ideal_joint_vel_l[1] = vel1;
                    break;
                }
            case UPPER_RIGHT: 
                {
                    // get pan-tilt velocities from packet
                    float vel0 = packet->getMotor1Velocity() * M_PI / 180;
                    float vel1 = packet->getMotor2Velocity() * M_PI / 180;
                    // set pan-titl velocities with target velocities
                    m_ideal_joint_vel_r[0] = vel0;
                    m_ideal_joint_vel_r[1] = vel1;
                    break;
                }
            default:    
                break;
        }
    }

    // routine to handle packet for status of up-down velocity
    void hyundai_controller::routineForSlaveToHostVel(const std::shared_ptr<PacketSlaveToHostVel> packet, hyundai_controller::session_type type){
        // do nothing
    }
    // routine to handle packet for status of pan-tilt velocities
    void hyundai_controller::routineForSlaveToHostVel2(const std::shared_ptr<PacketSlaveToHostVel2> packet, hyundai_controller::session_type type){
        // do nothing
    }

    // routine to handle packet for ACK
    void hyundai_controller::routineForAck(const std::shared_ptr<PacketAck> packet, hyundai_controller::session_type type){
        // do nothing
    }

    // routine to handle packet for notice that slave completed task
    void hyundai_controller::routineForSlaveComplete(const std::shared_ptr<PacketSlaveComplete> packet, hyundai_controller::session_type type){
        // do nothing
    }

    // parsing packet from received packet stream
    void hyundai_controller::parsingPacket(std::vector<unsigned char>& buffer,hyundai_controller::session_type type){

        // split packet streams using header stream
        std::vector<std::string> splitted_packet = splitPacket(buffer);
        buffer.clear();

        for(auto s = splitted_packet.begin(); s != splitted_packet.end(); ++s){
            try{
                std::vector<unsigned char> packet_stream(s->begin(), s->end());

                // parse a packet from a packet stream 
                std::shared_ptr<Packet> packet = parsePacketFromStream(packet_stream);

                // go to packet handling routine as per packet type
                switch(packet->getPacketType()){
                    case PACKET_TYPE_ECHO: 
                        {
                            auto packet_echo = std::dynamic_pointer_cast<PacketEcho<std::vector<unsigned char>>>(packet);
                            routineForEcho(packet_echo, type);
                            break;
                        }
                    case PACKET_TYPE_ECHO_TEST:
                        {
                            auto packet_echo_test = std::dynamic_pointer_cast<PacketEchoTest<std::vector<unsigned char>>>(packet);
                            routineForEchoTest(packet_echo_test, type);
                            break;
                        }
                    case PACKET_TYPE_INITIALIZE:
                        {
                            auto packet_init = std::dynamic_pointer_cast<PacketInitialize>(packet);
                            routineForInitialize(packet_init, type);
                            break;
                        }
                    case PACKET_TYPE_HOST_TO_SLAVE_ANG:
                        {
                            auto packet_ang = std::dynamic_pointer_cast<PacketHostToSlaveAng>(packet);
                            routineForHostToSlaveAng(packet_ang, type);
                            break;
                        }
                    case PACKET_TYPE_SLAVE_TO_HOST_ANG:
                        {
                            auto packet_ang = std::dynamic_pointer_cast<PacketSlaveToHostAng>(packet);
                            routineForSlaveToHostAng(packet_ang, type);
                            break;
                        }
                    case PACKET_TYPE_HOST_TO_SLAVE_VEL:
                        {
                            auto packet_vel = std::dynamic_pointer_cast<PacketHostToSlaveVel>(packet);
                            routineForHostToSlaveVel(packet_vel, type);
                            break;
                        }
                    case PACKET_TYPE_HOST_TO_SLAVE_VEL2:
                        {
                            auto packet_vel = std::dynamic_pointer_cast<PacketHostToSlaveVel2>(packet);
                            routineForHostToSlaveVel2(packet_vel, type);
                            break;
                        }
                    case PACKET_TYPE_SLAVE_TO_HOST_VEL:
                        {
                            auto packet_vel = std::dynamic_pointer_cast<PacketSlaveToHostVel>(packet);
                            routineForSlaveToHostVel(packet_vel, type);
                            break;
                        }
                    case PACKET_TYPE_SLAVE_TO_HOST_VEL2:
                        {
                            auto packet_vel = std::dynamic_pointer_cast<PacketSlaveToHostVel2>(packet);
                            routineForSlaveToHostVel2(packet_vel, type);
                            break;
                        }
                    case PACKET_TYPE_ACK:
                        {
                            auto packet_ack = std::dynamic_pointer_cast<PacketAck>(packet);
                            routineForAck(packet_ack, type);
                            break;
                        }
                    case PACKET_TYPE_SLAVE_COMPLETE:
                        {
                            auto packet_slave_complete = std::dynamic_pointer_cast<PacketSlaveComplete>(packet);
                            routineForSlaveComplete(packet_slave_complete, type);
                            break;
                        }

                    default: throw UnknownPacketException();
                }


            } catch (ShortPacketException& e){
                // if received short packet 
                if(s->size() != 0)
                    std::cerr << "ShortPacketException: " << s->size() << std::endl;
                if(s ==  splitted_packet.end()-1){
                    // if this packet is tail, then push back into original buffer 
                    printPacket(s->begin(), s->end());
                    buffer.insert(buffer.begin(), s->begin(), s->end());
                }
                continue;
            } catch (ChecksumFailureException& e){
                std::cerr << "ChecksumFailureException" << std::endl;
                continue;
            } catch (UnknownPacketException& e){
                std::cerr << "UnknownPacketException" << std::endl;
                continue;
            } catch (InvalidDataException& e){
                std::cerr << "InvalidDataException" << std::endl;
                continue;
            }
        }

    }

};



// startup main code
int main(int argc, char **argv)
{

    // define port for each module
#ifdef ROBOT_1
    unsigned int port_for_lower = 3111;
    unsigned int port_for_upper_l = 3112;
    unsigned int port_for_upper_r = 3113;
    unsigned int port_for_laser_l = 3114;
    unsigned int port_for_laser_r = 3115;
    unsigned int port_for_laser_b = 3116;
    unsigned int port_for_imu = 3117;
#elif ROBOT_2
    unsigned int port_for_lower = 3211;
    unsigned int port_for_upper_l = 3212;
    unsigned int port_for_upper_r = 3213;
    unsigned int port_for_laser_l = 3214;
    unsigned int port_for_laser_r = 3215;
    unsigned int port_for_laser_b = 3216;
    unsigned int port_for_imu = 3217;
#elif ROBOT_3
    unsigned int port_for_lower = 3311;
    unsigned int port_for_upper_l = 3312;
    unsigned int port_for_upper_r = 3313;
    unsigned int port_for_laser_l = 3314;
    unsigned int port_for_laser_r = 3315;
    unsigned int port_for_laser_b = 3316;
    unsigned int port_for_imu = 3317;
#endif



    // define webots controller object
    goodguy::hyundai_controller* controller = new goodguy::hyundai_controller();

    // make thread to control robot in webots
    boost::thread thread_for_run([&]() { controller->run(); });

    try{

        // open server to recevie connection request from main-controller
        boost::asio::io_service io;
        goodguy::server server_for_lower(io, port_for_lower, controller->getLowerSession());
        goodguy::server server_for_upper_l(io, port_for_upper_l, controller->getUpperLeftSession());
        goodguy::server server_for_upper_r(io, port_for_upper_r, controller->getUpperRightSession());
        goodguy::server server_for_laser_l(io, port_for_laser_l, controller->getLaserLeftSession());
        goodguy::server server_for_laser_r(io, port_for_laser_r, controller->getLaserRightSession());
        goodguy::server server_for_laser_b(io, port_for_laser_b, controller->getLaserBottomSession());
        goodguy::server server_for_imu(io, port_for_imu, controller->getImuSession());

        // start boost.asio 
        io.run();

    } catch(std::exception& e){
        std::cerr << "ERROR: " << e.what() << std::endl;

    }

    // wait until webots controller is stopped
    thread_for_run.join();

    delete controller;
    return 0;
}
