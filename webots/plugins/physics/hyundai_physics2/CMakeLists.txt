set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DdDOUBLE -DLINUX")

add_library ( hyundai_physics2 SHARED hyundai_physics2.c)

target_link_libraries( hyundai_physics2 GL ode)

set_target_properties( hyundai_physics2 PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${HYUNDAI_SOURCE_DIR}/plugins/physics/hyundai_physics2/ )
set_target_properties( hyundai_physics2 PROPERTIES LIBRARY_OUTPUT_DIRECTORY_DEBUG ${HYUNDAI_SOURCE_DIR}/plugins/physics/hyundai_physics2/ )
set_target_properties( hyundai_physics2 PROPERTIES LIBRARY_OUTPUT_DIRECTORY_RELEASE ${HYUNDAI_SOURCE_DIR}/plugins/physics/hyundai_physics2/ )
set_target_properties( hyundai_physics2 PROPERTIES LIBRARY_OUTPUT_DIRECTORY_MINSIZEREL ${HYUNDAI_SOURCE_DIR}/plugins/physics/hyundai_physics2/ )
set_target_properties( hyundai_physics2 PROPERTIES LIBRARY_OUTPUT_DIRECTORY_RELWITHDEBINFO ${HYUNDAI_SOURCE_DIR}/plugins/physics/hyundai_physics2/ )
