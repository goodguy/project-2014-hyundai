/*
 * File:          
 * Date:          
 * Description:   
 * Author:        
 * Modifications: 
 */

#include <ode/ode.h>
#include <plugins/physics.h>

/*
 * Note: This plugin will become operational only after it was compiled and associated with the current world (.wbt).
 * To associate this plugin with the world follow these steps:
 *  1. In the Scene Tree, expand the "WorldInfo" node and select its "physics" field
 *  2. Then hit the [...] button at the bottom of the Scene Tree
 *  3. In the list choose the name of this plugin (same as this file without the extention)
 *  4. Then save the .wbt by hitting the "Save" button in the toolbar of the 3D view
 *  5. Then revert the simulation: the plugin should now load and execute with the current simulation
 */

const char* l_wheel_1_str[4] = {"UP_DOWN_L_SOLID_1_1", "UP_DOWN_L_SOLID_1_2", "UP_DOWN_L_SOLID_1_3", "UP_DOWN_L_SOLID_1_4"};
const char* r_wheel_1_str[4] = {"UP_DOWN_R_SOLID_1_1", "UP_DOWN_R_SOLID_1_2", "UP_DOWN_R_SOLID_1_3", "UP_DOWN_R_SOLID_1_4"};

const char* l_wheel_2_str[4] = {"UP_DOWN_L_SOLID_2_1", "UP_DOWN_L_SOLID_2_2", "UP_DOWN_L_SOLID_2_3", "UP_DOWN_L_SOLID_2_4"};
const char* r_wheel_2_str[4] = {"UP_DOWN_R_SOLID_2_1", "UP_DOWN_R_SOLID_2_2", "UP_DOWN_R_SOLID_2_3", "UP_DOWN_R_SOLID_2_4"};

const char* l_wheel_3_str[4] = {"UP_DOWN_L_SOLID_3_1", "UP_DOWN_L_SOLID_3_2", "UP_DOWN_L_SOLID_3_3", "UP_DOWN_L_SOLID_3_4"};
const char* r_wheel_3_str[4] = {"UP_DOWN_R_SOLID_3_1", "UP_DOWN_R_SOLID_3_2", "UP_DOWN_R_SOLID_3_3", "UP_DOWN_R_SOLID_3_4"};

static dBodyID robot_1, robot_2, robot_3;
static dBodyID l_wheel_1[4];
static dBodyID r_wheel_1[4];


static dBodyID l_wheel_2[4];
static dBodyID r_wheel_2[4];

static dBodyID l_wheel_3[4];
static dBodyID r_wheel_3[4];

void webots_physics_init(dWorldID world, dSpaceID space, dJointGroupID contactJointGroup) {
    int i = 0; 
    for(i = 0; i < 4; ++i){
        l_wheel_1[i] = dWebotsGetBodyFromDEF(l_wheel_1_str[i]);
        if(l_wheel_1[i] != NULL) dBodySetGravityMode(l_wheel_1[i], 0);
        r_wheel_1[i] = dWebotsGetBodyFromDEF(r_wheel_1_str[i]);
        if(r_wheel_1[i] != NULL) dBodySetGravityMode(r_wheel_1[i], 0);
        l_wheel_2[i] = dWebotsGetBodyFromDEF(l_wheel_2_str[i]);
        if(l_wheel_2[i] != NULL) dBodySetGravityMode(l_wheel_2[i], 0);
        r_wheel_2[i] = dWebotsGetBodyFromDEF(r_wheel_2_str[i]);
        if(r_wheel_2[i] != NULL) dBodySetGravityMode(r_wheel_2[i], 0);
        l_wheel_3[i] = dWebotsGetBodyFromDEF(l_wheel_3_str[i]);
        if(l_wheel_3[i] != NULL) dBodySetGravityMode(l_wheel_3[i], 0);
        r_wheel_3[i] = dWebotsGetBodyFromDEF(r_wheel_3_str[i]);
        if(r_wheel_3[i] != NULL) dBodySetGravityMode(r_wheel_3[i], 0);
    }

    dWebotsConsolePrintf("START PHYSICS");

    robot_1 = dWebotsGetBodyFromDEF("ROBOT_1");
    if(robot_1 != NULL) dBodySetGravityMode(robot_1, 0);

    robot_2 = dWebotsGetBodyFromDEF("ROBOT_2");
    if(robot_2 != NULL) dBodySetGravityMode(robot_2, 0);

    robot_3 = dWebotsGetBodyFromDEF("ROBOT_3");
    if(robot_3 != NULL) dBodySetGravityMode(robot_3, 0);
}

void webots_physics_step() {
    
       int i =0;
       double force = 30;
       double add_force = 0;

       //double roll_offset = 0.035;
       double roll_offset = 0.00;
       double pitch_offset = 0.0;

       for(i = 0; i < 4; ++i){
           if(l_wheel_1[i] != NULL) dBodyAddForce(l_wheel_1[i], 0, 0, force);
           if(r_wheel_1[i] != NULL) dBodyAddForce(r_wheel_1[i], -(force)*cos(roll_offset), -(force*sin(roll_offset)), 0);

           if(l_wheel_2[i] != NULL) dBodyAddForce(l_wheel_2[i], 0, 0, force);
           if(r_wheel_2[i] != NULL) dBodyAddForce(r_wheel_2[i], force, 0, 0);

           if(l_wheel_3[i] != NULL) dBodyAddForce(l_wheel_3[i], force, 0, 0 );
           if(r_wheel_3[i] != NULL) dBodyAddForce(r_wheel_3[i], 0, 0, -force);
     
       }
}

void webots_physics_draw(int pass, const char *view) {
    /*
     * This function can optionally be used to add OpenGL graphics to the 3D view, e.g.
     *   // setup draw style
     *   glDisable(GL_LIGHTING);
     *   glLineWidth(2);
     * 
     *   // draw a yellow line
     *   glBegin(GL_LINES);
     *   glColor3f(1, 1, 0);
     *   glVertex3f(0, 0, 0);
     *   glVertex3f(0, 1, 0);
     *   glEnd();
     */
}

int webots_physics_collide(dGeomID g1, dGeomID g2) {
    /*
     * This function needs to be implemented if you want to overide Webots collision detection.
     * It must return 1 if the collision was handled and 0 otherwise. 
     * Note that contact joints should be added to the contactJointGroup, e.g.
     *   n = dCollide(g1, g2, MAX_CONTACTS, &contact[0].geom, sizeof(dContact));
     *   ...
     *   dJointCreateContact(world, contactJointGroup, &contact[i])
     *   dJointAttach(contactJoint, body1, body2);
     *   ...
     */
    return 0;
}

void webots_physics_cleanup() {
    /*
     * Here you need to free any memory you allocated in above, close files, etc.
     * You do not need to free any ODE object, they will be freed by Webots.
     */
}
