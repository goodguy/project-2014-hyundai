## PATH of BOOST library 
set(BOOST_ROOT_PATH ~/boost-odroid/)

## PATH of cross-compiler toolchain files
set(TOOLCHAIN_ROOT_PATH ~/arm-odroid-linux-gnueabi/)

####################  Do not touch below if you don't know #####################
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_TOOLCHAIN_PREFIX  arm-odroid-linux-gnueabi)
set(CMAKE_C_COMPILER    ${CMAKE_TOOLCHAIN_PREFIX}-gcc)
set(CMAKE_CXX_COMPILER  ${CMAKE_TOOLCHAIN_PREFIX}-g++)
set(CMAKE_FIND_ROOT_PATH   ${BOOST_ROOT_PATH} ${TOOLCHAIN_ROOT_PATH}/arm-odroid-linux-gnueabi/sysroot ${TOOLCHAIN_ROOT_PATH}/arm-odroid-linux-gnueabi/sysroot/usr)
set(CMAKE_C_FLAGS             "-march=armv7-a -mtune=cortex-a9 -mcpu=cortex-a9 -mfloat-abi=softfp -mfpu=neon -DODROID" CACHE STRING "c flags")
set(CMAKE_CXX_FLAGS           "${CMAKE_C_FLAGS}"                    CACHE STRING "c++ flags")
