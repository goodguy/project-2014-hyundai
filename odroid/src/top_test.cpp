#include "packet.hpp"
#include "controller.hpp"


#include <boost/asio.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/timer/timer.hpp>
#include <boost/thread/thread.hpp>

#include <iostream>
#include <string>
#include <vector>

namespace goodguy{

    template<class SOCKET>
        // Upper controller (left pan-tilt module)
        class UpperController : public Controller<SOCKET>{

            public:
                // constructor
                UpperController(SOCKET& socket, int control_time, SID sid)
                    : Controller<SOCKET>(socket, control_time, sid), m_is_run(true)
                { }


                // main control function
                void run(void){

                    while(isRun()){
                        // parsing received packet stream
                        this->parsePacket();

                        // wait control time
                        this->waitControlTime();
                    }
                }

                // check running status
                bool isRun(){
                    m_mutex_for_quit.lock_shared();
                    bool is_run = m_is_run;
                    m_mutex_for_quit.unlock_shared();
                    return is_run;
                }

                // stop main control function
                void stop(void){
                    m_mutex_for_quit.lock_upgrade();
                    m_is_run = false;
                    m_mutex_for_quit.unlock_upgrade();
                }

                // setting pan-tilt velocities 
                void setVelocities(float vel0, float vel1){
                    // make packet using target velocities
                    PacketHostToSlaveVel2 packet(this->m_sid, vel0, vel1);
                    // make packet stream
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet stream to left pan-tilt control board
                    this->write(packet_stream);
                    printPacket(packet_stream.begin(), packet_stream.end());
                }

                // setting pan-tilt angles 
                void setAngles(float angle0, float angle1){
                    // make packet using target angles
                    PacketHostToSlaveAng packet(this->m_sid, angle0, angle1);
                    // make packet stream
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet stream to left pan-tilt control board
                    this->write(packet_stream);
                    printPacket(packet_stream.begin(), packet_stream.end());
                }

                // request pan-tilt initialization 
                void initialize(){
                    // make initialization packet
                    PacketInitialize packet(this->m_sid);
                    // make packet stream
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    // send packet stream to left pan-tilt control board
                    this->write(packet_stream);
                }

                // print current pan-tilt angles
                void printAngles() const noexcept {
                    std::cout << "Angle: " << m_curr_motor1_angle << " ,\t" << m_curr_motor2_angle << std::endl;
                }

                // print current pan-tilt velocities
                void printVelocities() const noexcept {
                    std::cout << "Velocity: " << m_curr_motor1_vel << " ,\t" << m_curr_motor2_vel << std::endl;
                }


            private:
                // routine for received packet of current pan-tilt velocities
                void routineForSlaveToHostVel2(const std::shared_ptr<PacketSlaveToHostVel2> packet){
                    m_curr_motor1_vel = packet->getMotor1Velocity();
                    m_curr_motor2_vel = packet->getMotor2Velocity();
                }

                // routine for received packet of current pan-tilt angles
                void routineForSlaveToHostAng(const std::shared_ptr<PacketSlaveToHostAng> packet){
                    m_curr_motor1_angle = packet->getMotor1Angle();
                    m_curr_motor2_angle = packet->getMotor2Angle();
                    int count = 0;
                    if(count++ % 100 == 0)  printAngles();
                }

                // routine for received packet that slave job is finished
                void routineForSlaveComplete(const std::shared_ptr<PacketSlaveComplete> packet){
                    // get what kinds of job is finished
                    PacketType complete_type = packet->getCompleteType();
                    switch(complete_type){
                        case PACKET_TYPE_HOST_TO_SLAVE_ANG:
                            std::cout << "SLAVE Complete: " << m_curr_motor1_angle << " ,\t" << m_curr_motor2_angle << std::endl;
                            break;
                        case PACKET_TYPE_INITIALIZE:
                            std::cout << "initialize complete !" << std::endl;
                            break;
                        default:
                            std::cout << "complete type: " << complete_type << std::endl;
                            break;
                    }
                }

            private: 
                boost::shared_mutex m_mutex_for_quit;
                bool m_is_run;

                float m_curr_motor1_angle;
                float m_curr_motor2_angle;

                float m_curr_motor1_vel;
                float m_curr_motor2_vel;
        };
}

// get keyboard input in various template
template <typename T>
T getInput()
{
    T result;
    std::cin >> result;
    if (std::cin.fail() || std::cin.get() != '\n'){
        std::cin.clear();
        while (std::cin.get() != '\n');
        throw std::ios_base::failure("Invalid input.");
    }
    return result;
}


int main(){
    boost::asio::io_service io;

#ifdef SIMULATION_MODE      // To use this code in webots simulation
    // define control time
    const int control_time_for_upper_body = 10*1000*1000;

    using boost::asio::ip::tcp;

    // connect to left pan-tilt controller in webots simulator
    tcp::resolver resolver(io);
    tcp::resolver::query query(tcp::v4(), "127.0.0.1", "3112");
    tcp::resolver::iterator iterator = resolver.resolve(query);

    tcp::socket socket(io);
    try{
        // request connection
        boost::asio::connect(socket, iterator);
    }catch(std::exception& e){
        std::cerr << "Exception: " << e.what() << std::endl;
        std::cout << "Connection failed" << std::endl;
        return 0;
    }

    std::cout << "Connection Success" << std::endl;

    // create left pan-tilt controller object operating in webots simulator
    goodguy::UpperController<tcp::socket> upper_body(socket, control_time_for_upper_body, goodguy::SID::UPPER_1);
    // make control thread
    boost::thread thread_for_upper_body(boost::bind(&goodguy::UpperController<tcp::socket>::run, &upper_body));

#else                       // To use this code in real robot 
    // define control time
    const int control_time_for_upper_body = 10*1000*1000;

    // connect to left pan-tilt controller in real robot
    boost::asio::serial_port serial(io);
    serial.open("/dev/ttyBRD_L");

    // setting UART configuration
    serial.set_option(boost::asio::serial_port::baud_rate(1000000));
    serial.set_option(boost::asio::serial_port::flow_control());
    serial.set_option(boost::asio::serial_port::stop_bits());
    serial.set_option(boost::asio::serial_port::character_size());
    serial.set_option(boost::asio::serial_port::parity());

    // create left pan-tilt controller object operating in real robot
    goodguy::UpperController<boost::asio::serial_port> upper_body(serial, control_time_for_upper_body, goodguy::SID::UPPER_1);
    // make control thread
    boost::thread thread_for_upper_body(boost::bind(&goodguy::UpperController<boost::asio::serial_port>::run, &upper_body));
#endif

    // start async read operation using boost.asio
    upper_body.start_read();
    
    // make asio thread
    boost::thread thread_for_io(boost::bind(&boost::asio::io_service::run, &io));


    bool is_stop = false;

    std::cout << "Controller Program for Upper Body" << std::endl;

    // text-based user interface
    while(!is_stop){


        /*****************************
         * q: quit 
         * p: print robot's pan-tilt status 
         * e: send echo message 
         * i: request initialization 
         * v: setting pan-tilt velocities
         * a: setting pan-tilt angles 
         ******************************/

        unsigned char input = 0;
        try{
            // get user command
            input = getInput<unsigned char>();
        } catch(std::exception& e){
            input = 0x00;
        }

        switch(input){
            case 'p':
                {
                    // print pan-tilt angles and velocities
                    upper_body.printAngles();
                    upper_body.printVelocities();
                    break;
                }
            case 'i':
                {
                    std::cout << "Intialize ..." ;
                    // send requestion for pan-tilt initialization
                    upper_body.initialize();
                    break;
                }
            case 'q':   
                {
                    // stop main control function and quit
                    upper_body.stop();
                    io.stop();
                    is_stop = true;
                    break;
                }
            case 'e':
                {
                    // send echo message
                    std::cout << "Echo Message: " ;
                    std::string echo_str = getInput<std::string>();
                    upper_body.sendEcho(echo_str);
                    break;
                }
            case 'v':
                {
                    try{
                        // get target velocities from keyboard
                        std::cout << "Set Velocities (degree per second) - Motor 0: " ;
                        float vel0 = getInput<float>();
                        std::cout << "Set Velocities (degree per second) - Motor 1: " ;
                        float vel1 = getInput<float>();
                        // set target pan-tilt velocities
                        upper_body.setVelocities(vel0, vel1);
                    } catch(std::exception& e){
                        std::cout << "Invalid Input" << std::endl;

                    }
                    break;
                }
            case 'a':
                {
                    try{
                        // get target angles from keyboard
                        std::cout << "Set Angle (degree) - Motor 0: " ;
                        float angle0 = getInput<float>();
                        std::cout << "Set Angle (degree) - Motor 1: " ;
                        float angle1 = getInput<float>();
                        // set target pan-tilt angles
                        upper_body.setAngles(angle0, angle1);
                    } catch(std::exception& e){
                        std::cout << "Invalid Input" << std::endl;

                    }
                    break;
                }

        }
    }

    std::cout << "Program EXIT [wait thread...]" << std::endl;
    thread_for_upper_body.join();
    thread_for_io.join();
    std::cout << "Program EXIT" << std::endl;


    return 0;
}

