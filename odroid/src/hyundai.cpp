#include "packet.hpp"

#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/timer/timer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread/thread.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>

namespace goodguy{

    using namespace boost::numeric;

    struct WrongSelection{};

    // Sensor ID (Laser sensor)
    enum SID{
        SID_B = 2,      // for Bottom
        SID_L = 0,      // for Left
        SID_R = 1       // for Right
    };

    // Board ID
    enum BID{
        BID_LOWER = 0,              // for Up-down motion controller   
        BID_UPPER_LEFT = 1,         // for Left pan-tilt controller
        BID_UPPER_RIGHT = 2         // for Right pan-tilt controller
    };


    
    /********** MEASUREMENT ID ***********
    * ROBOT2                      ROBOT1 *  
    * __ beam2                  beam1 __ *  
    *|   <---------------------------   |*  
    *   !\        mID=2             A !  *  
    *   ! \                        /  !  *   
    *   !  \                      /   !  *   
    *   !   \                    /    !  *   
    *   !    \                  /     !  *   
    *   !     \                /      !  *   
    *   !      \ mID=3        /       !  *
    *   !       \            /        !  *   
    *   !        \          /         !  *   
    *   !         \        /          !  *  
    *   !          \      /           !  *   
    *   !           \    /            !  *   
    *   !            \  /             !  *   
    *   ! mID=4       \/       mID=1  !  *    
    *   !             /\              !  *   
    *   !            /  \             !  *   
    *   !           /    \            !  *   
    *   !          /      \           !  *  
    *   !         /        \          !  *   
    *   !        /          \         !  *   
    *   !       /            \        !  *   
    *   !      /              \       !  *  
    *   !     /  mID=5         \      !  *  
    *   !    /                  \     !  *   
    *   !   /                    \    !  *  
    *   !  /                      \   !  *  
    *   ! /                        \  !  *   
    *   V/             mID=6        V V  *   
    *|__ ---------------------------> __|*  
    *      beam3                beam4    *  
    * ROBOT3                             *  
    ***************************************/

    // Measurement ID
    enum MeasureID{
        MEASURE_VOID = -1,
        MEASURE_1 = 0,
        MEASURE_2 = 1,
        MEASURE_3 = 2,
        MEASURE_4 = 3,
        MEASURE_5 = 4,
        MEASURE_6 = 5
    };


    // struct for sample per point
    struct Sample{
        Sample(uint32_t sample_id, float ang0, float ang1, float dist, float height, float roll, float pitch, float yaw, float time)
            : m_sample_id(sample_id), m_angle0(ang0), m_angle1(ang1), m_distance(dist), m_height(height), 
                m_roll(roll), m_pitch(pitch), m_yaw(yaw) , m_time(time)
        {}

        // print sample
        friend std::ostream& operator<<(std::ostream& os, const Sample& sample){
            os << sample.m_sample_id << "  " << sample.m_angle0 << "  " << sample.m_angle1 << "  " << sample.m_distance << "  " << sample.m_height << "  ";
            os << sample.m_roll << "  " << sample.m_pitch << "  " << sample.m_yaw << "  " << sample.m_time << std::endl;
            return os;
        }


        uint32_t m_sample_id;

        float m_angle0;
        float m_angle1;
        float m_distance;       
        float m_height;
        float m_roll;
        float m_pitch;
        float m_yaw;
        float m_time;
    };

    // struct for Subscan which is a set of samples
    struct Subscan{
        Subscan(uint32_t ssid): m_ssid(ssid) {}
        void push_back(const std::shared_ptr<Sample>& sample){
            m_samples.push_back(sample);
        }
        void clear(){
            m_samples.clear();
        }
        // print Subscan 
        friend std::ostream& operator<<(std::ostream& os, const Subscan subscan){
            for(auto it = subscan.m_samples.begin(); it != subscan.m_samples.end(); ++it)
                os << subscan.m_ssid  << "  " << *(*it);
            return os;
        }
        std::vector<std::shared_ptr<Sample>> m_samples;
        uint32_t m_ssid;
    };

    // struct for Scan which is a set of Subscan
    struct Scan{
        Scan(uint32_t sid): m_sid(sid) {}
        void push_back(const std::shared_ptr<Subscan>& subscan){
            m_subscans.push_back(subscan);
        }
        void clear(){
            m_subscans.clear();
        }
        // print scan 
        friend std::ostream& operator<<(std::ostream& os, const Scan scan){
            for(auto it = scan.m_subscans.begin(); it != scan.m_subscans.end(); ++it)
                for(auto it2 = (*it)->m_samples.begin(); it2 != (*it)->m_samples.end(); ++it2)
                    os << scan.m_sid  << "  " << (*it)->m_ssid << "  " << *(*it2);

            return os;
        }
        std::vector<std::shared_ptr<Subscan>> m_subscans;
        uint32_t m_sid;
    };

    // struct for Measure which is a set of Scan
    struct Measure{
        Measure(): m_mid(MEASURE_VOID) {}
        Measure(MeasureID mid): m_mid(mid) {}
        void push_back(const std::shared_ptr<Scan>& scan){
            m_scans.push_back(scan);
        }

        void setMID(MeasureID mid){
            m_mid = mid;
        }
        void clear(){
            m_scans.clear();
        }
        // print Measure data 
        friend std::ostream& operator<<(std::ostream& os, const Measure measure){
            for(auto it = measure.m_scans.begin(); it != measure.m_scans.end(); ++it)
                for(auto it2 = (*it)->m_subscans.begin(); it2 != (*it)->m_subscans.end(); ++it2)
                    for(auto it3 = (*it2)->m_samples.begin(); it3 != (*it2)->m_samples.end(); ++it3)
                        os << measure.m_mid << "  " << (*it)->m_sid  << "  " << (*it2)->m_ssid << "  " << *(*it3);

            return os;
        }
        std::vector<std::shared_ptr<Scan>> m_scans;
        MeasureID m_mid;

    };


    // Measurement offset to pointing a beam as per mID
    struct MeasureOffset{
        MeasureOffset()
            : m_measure_ang_0(0), m_measure_ang_1(0),
            m_measure_ang_2(0), m_measure_ang_3(0), 
            m_measure_ang_4(0), m_measure_ang_5(0) 
        {}

        // load from known measurement offset
        MeasureOffset(float measure_ang_0, float measure_ang_1, float measure_ang_2, float measure_ang_3, float measure_ang_4, float measure_ang_5 )
            : m_measure_ang_0(measure_ang_0), m_measure_ang_1(measure_ang_1),
            m_measure_ang_2(measure_ang_2), m_measure_ang_3(measure_ang_3), 
            m_measure_ang_4(measure_ang_4), m_measure_ang_5(measure_ang_5) 
        {}

        // std. output of measurement offset
        friend std::ostream& operator<<(std::ostream& os, const MeasureOffset measure_offset){
            os << measure_offset.m_measure_ang_0 << "\n" << measure_offset.m_measure_ang_1 << "\n";
            os << measure_offset.m_measure_ang_2 << "\n" << measure_offset.m_measure_ang_3 << "\n";
            os << measure_offset.m_measure_ang_4 << "\n" << measure_offset.m_measure_ang_5;
            return os;
        }

        // load from std. input of measurement offset 
        friend std::istream& operator>>(std::istream& is, MeasureOffset& measure_offset){
            is >> measure_offset.m_measure_ang_0 >>  measure_offset.m_measure_ang_1;
            is >> measure_offset.m_measure_ang_2 >>  measure_offset.m_measure_ang_3;
            is >> measure_offset.m_measure_ang_4 >>  measure_offset.m_measure_ang_5;
            return is;
        }

        // get measurement offset as per mID
        float getMeasureOffsetAng(MeasureID mid){
            switch(mid){
                case MEASURE_VOID: return 0;
                case MEASURE_1:    return m_measure_ang_0;
                case MEASURE_2:    return m_measure_ang_1;
                case MEASURE_3:    return m_measure_ang_2;
                case MEASURE_4:    return m_measure_ang_3;
                case MEASURE_5:    return m_measure_ang_4;
                case MEASURE_6:    return m_measure_ang_5;
            }
            return 0;
        }

        float m_measure_ang_0;
        float m_measure_ang_1;
        float m_measure_ang_2;
        float m_measure_ang_3;
        float m_measure_ang_4;
        float m_measure_ang_5;

        struct SetFailException {};
    };

    // struct for offset to compensate pan-tilt error occured by kinematics of Hardware
    struct Offset{
        Offset(): m_ang_0(0), m_ang_1(0) {}
        Offset(float offset_0, float offset_1): m_ang_0(offset_0), m_ang_1(offset_1) {}

        // std. output to save offset into file
        friend std::ostream& operator<<(std::ostream& os, const Offset offset){
            os << offset.m_ang_0 << "\n" << offset.m_ang_1;
            return os;
        }

        // load offset from std. file
        friend std::istream& operator>>(std::istream& is, Offset& offset){
            is >> offset.m_ang_0 >>  offset.m_ang_1;
            return is;
        }

        float m_ang_0, m_ang_1;

        struct SetFailException {};
    };


    const int BUFFER_LENGTH = 128;

    template<class SOCKET>
        // Hyundai Controller class
        class HyundaiController{

            // Define Robot state 
            enum RobotState{
                IDLE,               // Nothing to do
                INIT,               // initailize variables and angles
                MOVE_DOWN,          // move down
                SCAN,               // SCAN START
                MOVE_UP,            // Move up
                OFFSET              // Setting measure offset

            };

            // Define Scan state in the SCAN robot state
            enum ScanState{
                SCAN_INIT    = 0,          // tilt up to maximum vertical angle
                SCAN_FAIL    = 1,          // scan fail
                SCAN_LEFT    = 2,          // scan left
                SCAN_RIGHT   = 3,          // scan right
                SCAN_RETURN  = 4,          // scan return 
                SCAN_DOWN    = 5,          // tilt step down 
                SCAN_IDLE    = 6,          // idle...
            };

            public:
            HyundaiController( 
                    SOCKET& socket_for_lower,           // socket to communicate with up-down controller
                    SOCKET& socket_for_upper_l,         // socket to communicate with left pan-tilt controller
                    SOCKET& socket_for_upper_r,         // socket to communicate with right pan-tilt controller
                    SOCKET& socket_for_laser_l,         // socket to communicate with left laser sensor
                    SOCKET& socket_for_laser_r,         // socket to communicate with right laser sensor
                    SOCKET& socket_for_laser_b,         // socket to communicate with bottom laser sensor
                    MeasureOffset measure_offset,       // measurement offset 
                    Offset offset_for_upper_l,          // kinematics offset of left pan-tilt
                    Offset offset_for_upper_r,          // kinematics offset of right pan-tilt
                    int control_time)                   // control time
                : m_socket_for_lower(socket_for_lower),
                m_socket_for_upper_l(socket_for_upper_l),
                m_socket_for_upper_r(socket_for_upper_r),
                m_socket_for_laser_l(socket_for_laser_l),
                m_socket_for_laser_r(socket_for_laser_r),
                m_socket_for_laser_b(socket_for_laser_b),
                m_measure_offset(measure_offset),
                m_offset_for_upper_l(offset_for_upper_l), m_offset_for_upper_r(offset_for_upper_r), 
                m_robot_state(IDLE), m_scan_state_l(SCAN_IDLE), m_scan_state_r(SCAN_IDLE),
                m_control_time(control_time), m_is_scan_start(false), m_is_stop(false), m_is_debug(false),
                m_is_start_laser_l(false), m_is_start_laser_r(false), m_is_start_laser_b(false),
                m_is_jumping_laser_l(false), m_is_jumping_laser_r(false), m_is_jumping_laser_b(false),
                m_is_avail_laser_l(false), m_is_avail_laser_r(false), m_is_avail_laser_b(false),
                m_is_initialize_l(false), m_is_initialize_r(false)
            { }


            // destructor
            ~HyundaiController(){
                setLowerVelocity(0);        // stop up-down motion
                setLeftAngles(0,0);         // go to initial pose of left pan-tilt
                setRightAngles(0,0);        // go to initial pose of right pan-tilt
            }

            void run(void){

                m_mutex_for_comm.lock_shared();
                bool is_stop = m_is_stop;
                m_mutex_for_comm.unlock_shared();

                
                const float long_distance = 12.00;          // define long distance of cargo hold (m)
                const float short_distance = 2.40;          // define short distance of cargo hold (m)
                const float beam_horizontal_width = 0.135;  // define beam's horizontal scannable width (m)

                const float vertical_scan_step = 0.01;      // define tilting scan step in metric (m)
                const float vertical_scan_step_size = 1;    // define the number of tilting scan step 

                const float laser_hz = 100.0;               // define Laser sensor output frequency (hz)
                const float required_sample_num = 2000.0;   // define the number of required sample per scan

                const float Ess = 0.05;                     // define minimum pan-tilt angle error (rad)
                const float Ev = 0.05;                      // define minimum up-down velocity error (m/min)

                const float move_up_velocity = -40;         // define move up velocity (m/min)
                const float move_down_velocity = 40;        // define move down velocity (m/min)

                const float vertical_move_step = 1.0;       // define up-down moving step move step in metric (m)

                const int   max_jumping_recovery_count = 2;         // define maximum number of jumping recovery count per step
                const float jumping_recovery_failed_step = 0.01;    // define horizontal distance to rotate in a recovery failure (m)


                //const float min_vertical_move = 0.8;        // define minimum depth to move down (m)
                //const float max_vertical_move = 18.0;       // define maximum depth to move up (m)
#ifdef ROBOT_1  
                const float min_vertical_move = 1.0;        // define minimum depth to move down (m)
                const float max_vertical_move = 20.0;       // define maximum depth to move up (m)
#elif  ROBOT_2
                const float min_vertical_move = 1.0;        // define minimum depth to move down (m)
                const float max_vertical_move = 20.0;       // define maximum depth to move up (m)
#elif  ROBOT_3
                const float min_vertical_move = 1.0;        // define minimum depth to move down (m)
                const float max_vertical_move = 20.0;       // define maximum depth to move up (m)
#endif



                const float required_scan_time = required_sample_num / laser_hz;                                // required scanning time
                const float diag_distance = std::sqrt(std::pow(long_distance,2)+std::pow(short_distance,2));    // diag distance of cargo hold (m)



                float distance_to_beam_l = 0;
                float distance_to_beam_r = 0;

                // Define measurement ID of each side (left, right) as per Robot 
#ifdef ROBOT_1  
                std::cout << "ROBOT 1 Is STARTED" << std::endl;
                MeasureID measure_id_l = MEASURE_1;
                MeasureID measure_id_r = MEASURE_2;
#elif  ROBOT_2
                std::cout << "ROBOT 2 Is STARTED" << std::endl;
                MeasureID measure_id_l = MEASURE_3;
                MeasureID measure_id_r = MEASURE_4;
#elif  ROBOT_3
                std::cout << "ROBOT 3 Is STARTED" << std::endl;
                MeasureID measure_id_l = MEASURE_5;
                MeasureID measure_id_r = MEASURE_6;
#else 
                std::cout << "ROBOT VOID Is STARTED" << std::endl;
                MeasureID measure_id_l = MEASURE_VOID;
                MeasureID measure_id_r = MEASURE_VOID;
#endif
                // define distance to beam as per each side
                switch(measure_id_l){
                    case MEASURE_1:  distance_to_beam_l = long_distance; break;
                    case MEASURE_3:  distance_to_beam_l = diag_distance; break;
                    case MEASURE_5:  distance_to_beam_l = diag_distance; break;
                    default:         distance_to_beam_l = long_distance; break;
                }
                switch(measure_id_r){
                    case MEASURE_2:  distance_to_beam_r = short_distance; break;
                    case MEASURE_4:  distance_to_beam_r = long_distance;  break;
                    case MEASURE_6:  distance_to_beam_r = short_distance; break;
                    default:         distance_to_beam_r = long_distance;  break;
                }

                // Define constants to use scan step
                const float vertical_scan_step_ang_l = vertical_scan_step / distance_to_beam_l * 180.0 / M_PI; 
                const float vertical_scan_step_ang_r = vertical_scan_step / distance_to_beam_r * 180.0 / M_PI; 

                const float vertical_max_ang_l = vertical_scan_step_ang_l * vertical_scan_step_size;
                const float vertical_max_ang_r = vertical_scan_step_ang_r * vertical_scan_step_size;

                const float horizontal_max_ang_l = beam_horizontal_width / distance_to_beam_l * 180.0 / M_PI  * 4;
                const float horizontal_max_ang_r = beam_horizontal_width / distance_to_beam_r * 180.0 / M_PI  * 4;
                //const float horizontal_max_ang_l = beam_horizontal_width / distance_to_beam_l * 180.0 / M_PI + 2.0 ;
                //const float horizontal_max_ang_r = beam_horizontal_width / distance_to_beam_r * 180.0 / M_PI + 2.0 ;

                const float require_vel_l = horizontal_max_ang_l / required_scan_time;
                const float require_vel_r = horizontal_max_ang_r / required_scan_time;

                std::cout << "Required SCAN VELOCITY: " << require_vel_l << " \t " << require_vel_r << std::endl;
                std::cout << "Vertical Max ANG: " << vertical_max_ang_l << "\t" << vertical_max_ang_r << std::endl;
                std::cout << "Horizontal Max ANG: " << horizontal_max_ang_l << "\t" << horizontal_max_ang_r << std::endl;


                // Set required velocities of 2-DoF pan-tilt (left, right)
                setLeftVelocities(require_vel_l, require_vel_l);
                setRightVelocities(require_vel_r, require_vel_r);

                // stop up-down motion
                setLowerVelocity(0);

                // go to intial pose of 2-DoF pan-tilt
                setLeftAngles(0,0);
                setRightAngles(0,0);

                float initial_horizontal_ang_l = 0.0;
                float initial_horizontal_ang_r = 0.00;

                float step_height = 0.0;

                float scan_desired_horizontal_ang_l = 0.0;
                float scan_desired_vertical_ang_l = 0.0;

                float scan_desired_horizontal_ang_r = 0.0;
                float scan_desired_vertical_ang_r = 0.0;

                uint32_t vertical_step_l = 0;
                uint32_t vertical_step_r = 0;

                uint32_t sid = 0;
                uint32_t left_ssid = 0;
                uint32_t left_sample_id = 0;

                uint32_t right_ssid = 0;
                uint32_t right_sample_id = 0;

                // scan pointer for left & right
                std::shared_ptr<Scan> left_scan_ptr;
                std::shared_ptr<Scan> right_scan_ptr;

                // subscan pointer for left & right
                std::shared_ptr<Subscan> left_subscan_ptr;
                std::shared_ptr<Subscan> right_subscan_ptr;

                // apply mID to  Measurement struct (left, right)  
                m_left_measure.setMID(measure_id_l);
                m_right_measure.setMID(measure_id_r);

                bool jumping_fixed_l = false;
                int jumping_count_l = 0;
                int jumping_recovery_count_l = 0;

                ScanState backup_scan_state_l = SCAN_IDLE;
                float backup_scan_ang_l = 0.0;


                bool jumping_fixed_r = false;
                int jumping_count_r = 0;
                int jumping_recovery_count_r = 0;

                ScanState backup_scan_state_r = SCAN_IDLE;
                float backup_scan_ang_r = 0.0;


                int control_count = 0;

                while(!is_stop){


                    // get current measurement offset to obtain initial angles
                    const float measure_offset_l = m_measure_offset.getMeasureOffsetAng(measure_id_l);
                    const float measure_offset_r = m_measure_offset.getMeasureOffsetAng(measure_id_r);

                    initial_horizontal_ang_l = measure_offset_l ;
                    initial_horizontal_ang_r = measure_offset_r ;

                    // Parsing packet for left pan-tilt controller
                    parsePacketForBoard(BID_UPPER_LEFT, m_recv_packet_for_upper_l);
                    // Parsing packet for right pan-tilt controller
                    parsePacketForBoard(BID_UPPER_RIGHT, m_recv_packet_for_upper_r);
                    // Parsing packet for up-down controller
                    parsePacketForBoard(BID_LOWER, m_recv_packet_for_lower);

                    // Parsing sensor data from Left laser sensor
                    parsePacketForSensor(SID_L, m_recv_packet_for_laser_l);
                    // Parsing sensor data from Right laser sensor
                    parsePacketForSensor(SID_R, m_recv_packet_for_laser_r);
                    // Parsing sensor data from Bottom laser sensor
                    parsePacketForSensor(SID_B, m_recv_packet_for_laser_b);
                    

                    // Finite-State-Machine for the Robot state
                    switch(m_robot_state){
                        case IDLE:
                            {
                                // Do nothing before receving start command from user
                                if(m_is_scan_start){
                                    m_robot_state = MOVE_UP;
                                    std::cout << "MOVE_UP" << std::endl;

                                    // Move up 
                                    setLowerVelocity(move_up_velocity);
                                }
                            }
                            break;
                        case OFFSET:
                            {
                                // Adjust Measurement offset state
                                if((std::abs(m_curr_ang_l[0] - initial_horizontal_ang_l) < Ess) 
                                        && (std::abs(m_curr_ang_l[1] - 0) < Ess) 
                                        &&(std::abs(m_curr_ang_r[0] - initial_horizontal_ang_r) < Ess) 
                                        && (std::abs(m_curr_ang_r[1] - 0) < Ess))
                                {
                                    m_robot_state = IDLE;
                                    std::cout << "Measure Offset finally Applied -> go to IDLE" << std::endl;
                                }
                                else{
                                    // apply adjust measurement offset 
                                    setLeftAngles(initial_horizontal_ang_l, 0);
                                    setRightAngles(initial_horizontal_ang_r, 0);
                                }
                            }
                            break;

                        case MOVE_UP:
                            {
                                // if reached to maximum depth then initialization is start 
                                if(m_curr_dist_b > max_vertical_move)
                                {
                                    // set to initial horizontal angles with measurement offset
                                    setLeftAngles(initial_horizontal_ang_l, 0);
                                    setRightAngles(initial_horizontal_ang_r, 0);
                                    //setLeftVelocities(require_vel_l, require_vel_l);
                                    //setRightVelocities(require_vel_r, require_vel_r);
                                    //setLeftVelocities(require_vel_l, require_vel_l);
                                    //setRightVelocities(require_vel_r, require_vel_r);
                        
                                    m_robot_state = INIT;
                                    setLeftVelocities(30, 30);
                                    setRightVelocities(30, 30);



                                    // stop up-down motion
                                    setLowerVelocity(0);
                                    std::cout << "INIT" << std::endl;
                                }

                            }
                            break;
                        case MOVE_DOWN:
                            {
                                // if reached to target step height, then scan start
                                if(m_curr_dist_b < step_height || m_curr_dist_b < min_vertical_move)
                                {
                                    // stop up-down motion
                                    setLowerVelocity(0);

                                    // wait until robot totally stop
                                    if(std::abs(m_curr_vel) < Ev){
                                        // convert robot state to SCAN 
                                        m_robot_state = SCAN;
                                        m_scan_state_l = SCAN_INIT;
                                        m_scan_state_r = SCAN_INIT;

                                        scan_desired_horizontal_ang_l = initial_horizontal_ang_l;
                                        scan_desired_vertical_ang_l = vertical_max_ang_l/2.0;

                                        scan_desired_horizontal_ang_r = initial_horizontal_ang_r;
                                        scan_desired_vertical_ang_r = vertical_max_ang_r/2.0;

                                        std::cout << "SCAN ANGLE: "  << initial_horizontal_ang_l << "\t" << initial_horizontal_ang_r << std::endl;
                                    }
                                }
                                else{
                                    static int send_vel_count = 0;

                                    // move down 
                                    if(send_vel_count++ % 200 == 0){
                                        setLowerVelocity(move_down_velocity);
                                    }


                                }
                            }
                            break;

                        case INIT:
                            {
                                if((std::abs(m_curr_ang_l[0] - initial_horizontal_ang_l) < Ess) 
                                        && (std::abs(m_curr_ang_l[1] - 0) < Ess)
                                        && (std::abs(m_curr_ang_r[0] - initial_horizontal_ang_r) < Ess) 
                                        && (std::abs(m_curr_ang_r[1] - 0) < Ess))
                                {
                                    // set pan-tilt velocities for left and right
                                    setLeftVelocities(require_vel_l, require_vel_l);
                                    setRightVelocities(require_vel_r, require_vel_r);

                                    m_robot_state = MOVE_DOWN;
                                    step_height = m_curr_dist_b - vertical_move_step;
                                    std::cout << "MOVE DOWN" << std::endl;
                                    // start move down
                                    setLowerVelocity(move_down_velocity);

                                    sid = 0;
                                    left_ssid = 0;
                                    left_sample_id = 0;
                                    right_ssid = 0;
                                    right_sample_id = 0;

                                    // initialize scannig data
                                    left_scan_ptr = std::make_shared<Scan>(Scan(sid));
                                    right_scan_ptr = std::make_shared<Scan>(Scan(sid));
                                    left_subscan_ptr = std::make_shared<Subscan>(Subscan(left_ssid));
                                    right_subscan_ptr = std::make_shared<Subscan>(Subscan(right_ssid));
                                    left_scan_ptr->push_back(left_subscan_ptr);
                                    right_scan_ptr->push_back(right_subscan_ptr);
                                    m_left_measure.push_back(left_scan_ptr);
                                    m_right_measure.push_back(right_scan_ptr);

                                }
                            }
                            break;
                        case SCAN:
                            {
                                // if scan is finished then start step down or save entire scan procedure
                                if(m_scan_state_l == SCAN_IDLE && m_scan_state_r == SCAN_IDLE){
                                    // if scan is totally finished
                                    if(m_curr_dist_b < min_vertical_move){
                                        m_is_scan_start = false;
                                        m_robot_state = IDLE;

                                        // stop up-down motion
                                        setLowerVelocity(0);

                                        std::cout << "SCAN FINISH" << std::endl;

                                        // Open measurement file to save scanning data
#ifdef ROBOT_1
                                        std::ofstream left_measure_file("measure_1_file.txt", std::ofstream::trunc);
                                        std::ofstream right_measure_file("measure_2_file.txt", std::ofstream::trunc);
#elif  ROBOT_2
                                        std::ofstream left_measure_file("measure_3_file.txt", std::ofstream::trunc);
                                        std::ofstream right_measure_file("measure_4_file.txt", std::ofstream::trunc);
#elif  ROBOT_3
                                        std::ofstream left_measure_file("measure_5_file.txt", std::ofstream::trunc);
                                        std::ofstream right_measure_file("measure_6_file.txt", std::ofstream::trunc);
#else 
                                        std::ofstream left_measure_file("left_measure_file.txt", std::ofstream::trunc);
                                        std::ofstream right_measure_file("right_measure_file.txt", std::ofstream::trunc);
#endif
                                        // save scanning data into file
                                        left_measure_file << m_left_measure ;
                                        right_measure_file << m_right_measure << std::endl;

                                        left_measure_file.close();
                                        right_measure_file.close();
                                    }
                                    // Entire scan procedure is unfinished, so prepare to next scan 
                                    else{

                                        std::cout << "MOVE DOWN" << std::endl;

                                        // change robot state to MOVE_DOWN
                                        m_robot_state = MOVE_DOWN;
                                        step_height = step_height - vertical_move_step;

                                        // Move down to next step height
                                        setLowerVelocity(move_down_velocity);
                                        sid++;
                                        left_ssid = 0;
                                        right_ssid = 0;
                                        left_sample_id = 0;
                                        right_sample_id = 0;

                                        // prepare variables to use in next scan step
                                        left_scan_ptr = std::make_shared<Scan>(Scan(sid));
                                        right_scan_ptr = std::make_shared<Scan>(Scan(sid));
                                        left_subscan_ptr = std::make_shared<Subscan>(Subscan(left_ssid));
                                        right_subscan_ptr = std::make_shared<Subscan>(Subscan(right_ssid));

                                        left_scan_ptr->push_back(left_subscan_ptr);
                                        right_scan_ptr->push_back(right_subscan_ptr);
                                        m_left_measure.push_back(left_scan_ptr);
                                        m_right_measure.push_back(right_scan_ptr);

                                    }

                                }
                                // During scanning state
                                else{
                                    // for Left pan-tilt scanning
                                    switch(m_scan_state_l){
                                        case SCAN_INIT:
                                            {
                                                // set panning angle to left-rotate angle
                                                if(((std::abs(m_curr_ang_l[0] - scan_desired_horizontal_ang_l) < Ess) 
                                                    && (std::abs(m_curr_ang_l[1] - scan_desired_vertical_ang_l) < Ess) )
                                                    || m_is_jumping_laser_l)
                                                {
                                                    scan_desired_horizontal_ang_l = initial_horizontal_ang_l + horizontal_max_ang_l;
                                                    m_scan_state_l = SCAN_LEFT;
                                                    vertical_step_l = 0;

                                                }
                                            }
                                            break;
                                        case SCAN_FAIL:
                                            {
                                                // SCAN failure is occured by laser sensor's jumping condition
                                                if(m_is_start_laser_l && !m_is_jumping_laser_l)
                                                {
                                                    // restore target scan angle and state
                                                    scan_desired_horizontal_ang_l = backup_scan_ang_l;
                                                    m_scan_state_l = backup_scan_state_l;
                                                    jumping_count_l = 0;
                                                    jumping_recovery_count_l = 0;
                                                    //jumping_fixed_l = true;
                                                    //std::cout << "SCAN RECOVERY" << std::endl;
                                                }
                                                else{
                                                    // send start sensor command
                                                    startSensor(SID_L);
                                                    /*
                                                    if(jumping_count_l % 400 == 0){
                                                        startSensor(SID_L);
                                                    }
                                                    // send stop sensor command
                                                    else if(jumping_count_l % 400 == 300){
                                                        stopSensor(SID_L);
                                                        jumping_recovery_count_l++;
                                                    }
                                                    */

                                                    // if jumping recovery count is exceed maximum number of count, then move slightly to target scan angle
                                                    {
                                                    //if(jumping_recovery_count_l >= max_jumping_recovery_count){
                                                        jumping_recovery_count_l = 0;
                                                        switch(backup_scan_state_l){
                                                            case SCAN_LEFT  :  scan_desired_horizontal_ang_l += (jumping_recovery_failed_step/distance_to_beam_l)*180.0/M_PI;  break;
                                                            case SCAN_RIGHT :  scan_desired_horizontal_ang_l -= (jumping_recovery_failed_step/distance_to_beam_l)*180.0/M_PI;  break;
                                                            case SCAN_RETURN:  scan_desired_horizontal_ang_l += (jumping_recovery_failed_step/distance_to_beam_l)*180.0/M_PI;  break;
                                                            default         :  scan_desired_horizontal_ang_l += (jumping_recovery_failed_step/distance_to_beam_l)*180.0/M_PI;  break;
                                                        }
                                                    }

                                                    jumping_count_l++;

                                                }

                                            }
                                            break;
                                        case SCAN_LEFT:
                                            {
                                                // Panning left
                                                if(((std::abs(m_curr_ang_l[0] - scan_desired_horizontal_ang_l) < Ess) 
                                                            && (std::abs(m_curr_ang_l[1] - scan_desired_vertical_ang_l) < Ess)) || jumping_fixed_l)
                                                {
                                                    // After finished scan left then, start scan right
                                                    jumping_fixed_l = false;
                                                    scan_desired_horizontal_ang_l = initial_horizontal_ang_l - horizontal_max_ang_l;
                                                    m_scan_state_l = SCAN_RIGHT;
                                                }
                                                else if(m_is_jumping_laser_l){
                                                    // if jumping is occured, then go to SCAN_FAIL state
                                                    backup_scan_ang_l = scan_desired_horizontal_ang_l;
                                                    backup_scan_state_l = m_scan_state_l;
                                                    m_scan_state_l = SCAN_FAIL;
                                                    stopSensor(SID_L);
                                                    scan_desired_horizontal_ang_l = m_curr_ang_l[0] ; 
                                                }
                                            }
                                            break;
                                        case SCAN_RIGHT:
                                            {
                                                // Panning right
                                                if(((std::abs(m_curr_ang_l[0] - scan_desired_horizontal_ang_l) < Ess) 
                                                    && (std::abs(m_curr_ang_l[1] - scan_desired_vertical_ang_l) < Ess)) || jumping_fixed_l)
                                                {
                                                    jumping_fixed_l = false;
                                                    scan_desired_horizontal_ang_l = initial_horizontal_ang_l;
                                                    m_scan_state_l = SCAN_RETURN;
                                                }
                                                else if(m_is_jumping_laser_l){
                                                    // if jumping is occured, then go to SCAN_FAIL state
                                                    backup_scan_ang_l = scan_desired_horizontal_ang_l;
                                                    backup_scan_state_l = m_scan_state_l;
                                                    m_scan_state_l = SCAN_FAIL;
                                                    stopSensor(SID_L);
                                                    scan_desired_horizontal_ang_l = m_curr_ang_l[0] ; 
                                                }
                                            }
                                            break;
                                        case SCAN_RETURN:
                                            {
                                                // Panning initial pos
                                                if(((std::abs(m_curr_ang_l[0] - scan_desired_horizontal_ang_l) < Ess) 
                                                    && (std::abs(m_curr_ang_l[1] - scan_desired_vertical_ang_l) < Ess)) || jumping_fixed_l)
                                                {
                                                    jumping_fixed_l = false;
                                                    if( ++vertical_step_l < vertical_scan_step_size){
                                                        // Tilting to down
                                                        scan_desired_horizontal_ang_l = initial_horizontal_ang_l;
                                                        scan_desired_vertical_ang_l = scan_desired_vertical_ang_l - vertical_scan_step_ang_l;
                                                        m_scan_state_l = SCAN_DOWN;
                                                    }
                                                    else{
                                                        // Scan is finished
                                                        m_scan_state_l = SCAN_IDLE;
                                                    }

                                                }
                                                else if(m_is_jumping_laser_l){
                                                    // if jumping is occured, then go to SCAN_FAIL state
                                                    backup_scan_ang_l = scan_desired_horizontal_ang_l;
                                                    backup_scan_state_l = m_scan_state_l;
                                                    m_scan_state_l = SCAN_FAIL;
                                                    stopSensor(SID_L);
                                                    scan_desired_horizontal_ang_l = m_curr_ang_l[0] ;
                                                }
                                            }
                                            break;
                                        case SCAN_DOWN:
                                            {
                                                // Tilting to down
                                                if((std::abs(m_curr_ang_l[0] - scan_desired_horizontal_ang_l) < Ess) 
                                                    && (std::abs(m_curr_ang_l[1] - scan_desired_vertical_ang_l) < Ess))
                                                {
                                                    // Restart panning to left
                                                    left_sample_id = 0;
                                                    left_ssid++;
                                                    left_subscan_ptr = std::make_shared<Subscan>(Subscan(left_ssid));
                                                    left_scan_ptr->push_back(left_subscan_ptr);
                                                    m_scan_state_l = SCAN_LEFT;
                                                    scan_desired_horizontal_ang_l = initial_horizontal_ang_l + horizontal_max_ang_l;
                                                }
                                            }
                                            break;
                                        case SCAN_IDLE:
                                            {
                                            }
                                            break;
                                    }

                                    // for Right pan-tilt scanning
                                    switch(m_scan_state_r){
                                        case SCAN_INIT:
                                            {
                                                // set panning angle to left-rotate angle
                                                if(((std::abs(m_curr_ang_r[0] - scan_desired_horizontal_ang_r) < Ess) 
                                                    && (std::abs(m_curr_ang_r[1] - scan_desired_vertical_ang_r) < Ess) )
                                                    || m_is_jumping_laser_r)
                                                {
                                                    std::cout << "SCAN LEFT" << std::endl;
                                                    scan_desired_horizontal_ang_r = initial_horizontal_ang_r + horizontal_max_ang_r;
                                                    m_scan_state_r = SCAN_LEFT;
                                                    vertical_step_r = 0;
                                                }
                                            }
                                            break;
                                        case SCAN_FAIL:
                                            {
                                                // SCAN failure is occured by laser sensor's jumping condition
                                                if(m_is_start_laser_r && !m_is_jumping_laser_r)
                                                {
                                                    // restore target scan angle and state
                                                    scan_desired_horizontal_ang_r = backup_scan_ang_r;
                                                    m_scan_state_r = backup_scan_state_r;
                                                    jumping_count_r = 0;
                                                    jumping_recovery_count_r = 0;
                                                    //jumping_fixed_r = true;
                                                    //std::cout << "SCAN RECOVERY" << std::endl;
                                                }
                                                else{
                                                    // send start sensor command
                                                    startSensor(SID_R);
                                                    /*
                                                    if(jumping_count_r % 400 == 0){
                                                        startSensor(SID_R);
                                                    }
                                                    // send stop sensor command
                                                    else if(jumping_count_r % 400 == 300){
                                                        stopSensor(SID_R);
                                                        jumping_recovery_count_r++;
                                                    }
                                                    */

                                                    // if jumping recovery count is exceed maximum number of count, then move slightly to target scan angle
                                                    //if(jumping_recovery_count_r >= max_jumping_recovery_count){
                                                    {
                                                        jumping_recovery_count_r = 0;
                                                        switch(backup_scan_state_r){
                                                            case SCAN_LEFT  :  scan_desired_horizontal_ang_r += (jumping_recovery_failed_step/distance_to_beam_r)*180.0/M_PI;  break;
                                                            case SCAN_RIGHT :  scan_desired_horizontal_ang_r -= (jumping_recovery_failed_step/distance_to_beam_r)*180.0/M_PI;  break;
                                                            case SCAN_RETURN:  scan_desired_horizontal_ang_r += (jumping_recovery_failed_step/distance_to_beam_r)*180.0/M_PI;  break;
                                                            default         :  scan_desired_horizontal_ang_r += (jumping_recovery_failed_step/distance_to_beam_r)*180.0/M_PI;  break;
                                                        }
                                                        
                                                    }

                                                    jumping_count_r++;
                                                }

                                            }
                                            break;


                                        case SCAN_LEFT:
                                            {
                                                // Panning left
                                                if(((std::abs(m_curr_ang_r[0] - scan_desired_horizontal_ang_r) < Ess) 
                                                    && (std::abs(m_curr_ang_r[1] - scan_desired_vertical_ang_r) < Ess)) || jumping_fixed_r)
                                                {
                                                    // After finished scan left then, start scan right
                                                    jumping_fixed_r = false;
                                                    std::cout << "SCAN RIGHT" << std::endl;
                                                    scan_desired_horizontal_ang_r = initial_horizontal_ang_r - horizontal_max_ang_r;
                                                    m_scan_state_r = SCAN_RIGHT;
                                                }
                                                else if(m_is_jumping_laser_r){
                                                    // if jumping is occured, then go to SCAN_FAIL state
                                                    std::cout << "SCAN FAIL" << std::endl;
                                                    backup_scan_ang_r = scan_desired_horizontal_ang_r;
                                                    backup_scan_state_r = m_scan_state_r;
                                                    m_scan_state_r = SCAN_FAIL;
                                                    stopSensor(SID_R);
                                                    scan_desired_horizontal_ang_r = m_curr_ang_r[0] ;
                                                }
                                            }
                                            break;
                                        case SCAN_RIGHT:
                                            {
                                                // Panning right
                                                if(((std::abs(m_curr_ang_r[0] - scan_desired_horizontal_ang_r) < Ess) 
                                                    && (std::abs(m_curr_ang_r[1] - scan_desired_vertical_ang_r) < Ess)) || jumping_fixed_r)
                                                {
                                                    jumping_fixed_r = false;
                                                    std::cout << "SCAN RETURN" << std::endl;
                                                    scan_desired_horizontal_ang_r = initial_horizontal_ang_r;
                                                    m_scan_state_r = SCAN_RETURN;
                                                }
                                                else if(m_is_jumping_laser_r){
                                                    // if jumping is occured, then go to SCAN_FAIL state
                                                    std::cout << "SCAN FAIL" << std::endl;
                                                    backup_scan_ang_r = scan_desired_horizontal_ang_r;
                                                    backup_scan_state_r = m_scan_state_r;
                                                    m_scan_state_r = SCAN_FAIL;
                                                    stopSensor(SID_R);
                                                    scan_desired_horizontal_ang_r = m_curr_ang_r[0] ;
                                                }
                                            }
                                            break;
                                        case SCAN_RETURN:
                                            {
                                                // Panning initial pos
                                                if(((std::abs(m_curr_ang_r[0] - scan_desired_horizontal_ang_r) < Ess) 
                                                    && (std::abs(m_curr_ang_r[1] - scan_desired_vertical_ang_r) < Ess)) || jumping_fixed_r)
                                                {
                                                    jumping_fixed_r = false;
                                                    if( ++vertical_step_r < vertical_scan_step_size){
                                                        // Tilting to down
                                                        std::cout << "SCAN DOWN" << std::endl;
                                                        scan_desired_horizontal_ang_r = initial_horizontal_ang_r;
                                                        scan_desired_vertical_ang_r = scan_desired_vertical_ang_r - vertical_scan_step_ang_r;
                                                        m_scan_state_r = SCAN_DOWN;
                                                    }
                                                    else{
                                                        // Scan is finished
                                                        m_scan_state_r = SCAN_IDLE;
                                                        std::cout << "SCAN IDLE" << std::endl;
                                                    }

                                                }
                                                else if(m_is_jumping_laser_r){
                                                    // if jumping is occured, then go to SCAN_FAIL state
                                                    std::cout << "SCAN FAIL" << std::endl;
                                                    backup_scan_ang_r = scan_desired_horizontal_ang_r;
                                                    backup_scan_state_r = m_scan_state_r;
                                                    m_scan_state_r = SCAN_FAIL;
                                                    stopSensor(SID_R);
                                                    scan_desired_horizontal_ang_r = m_curr_ang_r[0] ; 
                                                }
                                            }
                                            break;
                                        case SCAN_DOWN:
                                            {
                                                // Tilting to down
                                                if((std::abs(m_curr_ang_r[0] - scan_desired_horizontal_ang_r) < Ess) 
                                                    && (std::abs(m_curr_ang_r[1] - scan_desired_vertical_ang_r) < Ess))
                                                {
                                                    // Restart panning to left
                                                    right_sample_id = 0;
                                                    std::cout << "SCAN LEFT" << std::endl;
                                                    right_ssid++;
                                                    right_subscan_ptr = std::make_shared<Subscan>(Subscan(right_ssid));
                                                    right_scan_ptr->push_back(right_subscan_ptr);
                                                    scan_desired_horizontal_ang_r = initial_horizontal_ang_r + horizontal_max_ang_r;
                                                    m_scan_state_r = SCAN_LEFT;
                                                }
                                            }
                                            break;
                                        case SCAN_IDLE:
                                            {
                                            }
                                            break;
                                    }

                                }

                                // Set Left pan-tilt to desired angles
                                setLeftAngles(scan_desired_horizontal_ang_l, scan_desired_vertical_ang_l);
                                // Set Right pan-tilt to desired angles
                                setRightAngles(scan_desired_horizontal_ang_r, scan_desired_vertical_ang_r);
                            }
                            break;
                    }


                    if(m_is_avail_laser_b){
                        if(m_is_avail_laser_l){
                            if( (m_robot_state == SCAN)
                                    && (m_scan_state_l == SCAN_LEFT || m_scan_state_l == SCAN_RIGHT || m_scan_state_l == SCAN_RETURN) )
                            {
                                // Make a sample struct to save
                                std::shared_ptr<Sample> sample_l_ptr(
                                        new Sample(left_sample_id, m_curr_ang_l[0], m_curr_ang_l[1], 
                                            m_curr_dist_l, m_curr_dist_b,
                                            m_curr_roll, m_curr_pitch, m_curr_yaw, 
                                            ((float)control_count)*(float)m_control_time/1000.0/1000.0)
                                        );
                                // Save a sample into subscan
                                left_subscan_ptr->push_back(sample_l_ptr);
                                left_sample_id++;
                                m_is_avail_laser_l = false;
                            }
                        }
                        if(m_is_avail_laser_r){
                            if( (m_robot_state == SCAN)
                                    && (m_scan_state_r == SCAN_LEFT || m_scan_state_r == SCAN_RIGHT || m_scan_state_r == SCAN_RETURN) )
                            {
                                // Make a sample struct to save
                                std::shared_ptr<Sample> sample_r_ptr(
                                        new Sample(right_sample_id, m_curr_ang_r[0], m_curr_ang_r[1], 
                                            m_curr_dist_r, m_curr_dist_b,
                                            m_curr_roll, m_curr_pitch, m_curr_yaw,
                                            ((float)control_count)*(float)m_control_time/1000.0/1000.0)
                                        );
                                // Save a sample into subscan
                                right_subscan_ptr->push_back(sample_r_ptr);
                                right_sample_id++;
                                m_is_avail_laser_r = false;
                            }
                        }

                        m_is_avail_laser_b = false;
                    }


                    // wait control time
                    waitControlTime();

                    control_count++;

                    m_mutex_for_comm.lock_shared();
                    is_stop = m_is_stop;
                    m_mutex_for_comm.unlock_shared();
                }
            }

            // get 3D position obtained from left laser sensor
            ublas::vector<float> getLeftPoint(float theta0, float theta1, float depth){

                float dh_parameters[] = {  
                    -90,          0,           0,     0,   
                    0,            0,           -277,  0,  
                    0,            67.4,        0,     -90,  
                    0,            125.5,       0,     90,  
                    theta0,       42.5,        0,     90,  
                    90.f+theta1,   27.7,        0,     0,  
                    0,            0,           33,    0,  
                    0,            -33,         0,     -90,  
                    0,            49.83,       0,     0,  
                    0,            0,           28,    90,  
                    0,            9.5,         0,     -90,  
                    0,            depth+38.5f, 0,     0      };

                std::size_t node_num = 12;

                // Apply kinematics
                ublas::matrix<float> T = ublas::identity_matrix<float>(4);
                for(std::size_t k = 0; k < node_num; ++k){
                    T = ublas::prod(T,z_transform(dh_parameters[4*k+0]*M_PI/180.0, dh_parameters[4*k+1])); 
                    T = ublas::prod(T,x_transform(dh_parameters[4*k+3]*M_PI/180.0, dh_parameters[4*k+2])); 
                }

                // Extract 3D point
                ublas::vector<float> point(3);
                for(std::size_t i = 0; i < 3; ++i){
                    point(i) = T(i,3)/1000.0;
                }

                return point;
            }

            // get 3D position obtained from Right laser sensor
            ublas::vector<float> getRightPoint(float theta0, float theta1, float depth){

                const float  dh_parameters[] = {
                    0,       0,           277,   -90,
                    0,       125.5,       0,     90,
                    theta0,  67.4,        0,     -90,
                    0,       -27.7,       0,     90,
                    0,       42.5,        0,     -90,
                    theta1,  33,          0,     90,
                    0,       33,          0,     0,
                    0,       0,           49.83, 0,
                    90,      28,          0,     0,
                    0,       0,           9.5,   90,
                    0,       depth+38.5f, 0,     0    };


                std::size_t node_num = 11;

                // Apply kinematics
                ublas::matrix<float> T = ublas::identity_matrix<float>(4);
                for(std::size_t k = 0; k < node_num; ++k){
                    T = ublas::prod(T,z_transform(dh_parameters[4*k+0]*M_PI/180.0, dh_parameters[4*k+1])); 
                    T = ublas::prod(T,x_transform(dh_parameters[4*k+3]*M_PI/180.0, dh_parameters[4*k+2])); 
                }

                // Extract 3D point
                ublas::vector<float> point(3);
                for(std::size_t i = 0; i < 3; ++i){
                    point(i) = T(i,3)/1000.0;
                }

                return point;
            }


            // z transform for DH parameter
            ublas::matrix<float> z_transform(float theta, float d){
                ublas::matrix<float> mat_trans = ublas::identity_matrix<float>(4);
                mat_trans(0,0) = std::cos(theta);
                mat_trans(0,1) = -std::sin(theta);
                mat_trans(1,0) = std::sin(theta);
                mat_trans(1,1) = std::cos(theta);
                mat_trans(2,3) = d;
                return mat_trans;
            }

            // x transform for DH parameter
            ublas::matrix<float> x_transform(float alpha, float a){
                ublas::matrix<float> mat_trans = ublas::identity_matrix<float>(4);
                mat_trans(1,1) = std::cos(alpha);
                mat_trans(1,2) = -std::sin(alpha);
                mat_trans(2,1) = std::sin(alpha);
                mat_trans(2,2) = std::cos(alpha);
                mat_trans(0,3) = a;
                return mat_trans;
            }

            // get Measurement offset 
            MeasureOffset getMeasureOffset() const noexcept{
                return m_measure_offset;
            }

            // set Measurement offset 
            void setMeasureOffset(MeasureOffset measure_offset){

                // Measurement offset is applied only IDLE and OFFSET state
                if(m_robot_state == IDLE || m_robot_state == OFFSET){
                    // adjust measurement offset
                    m_measure_offset = measure_offset;
                    m_robot_state = OFFSET;
                }
                else{
                    throw MeasureOffset::SetFailException();
                }
            }

            // go to initial measurement position (left, right)
            void initialMeasurePos() {
                if(m_robot_state == IDLE || m_robot_state == OFFSET){
                    m_robot_state = OFFSET;
                }
            }


            // get right pan-tilt kinematics offset
            Offset getOffsetRight() const noexcept{
                return m_offset_for_upper_r;
            }

            // get left pan-tilt kinematics offset
            Offset getOffsetLeft() const noexcept{
                return m_offset_for_upper_l;
            }

            // set right pan-tilt kinematics offset
            void setOffsetRight(Offset offset){
                // right kinematics offset is applied only IDLE state
                if(m_robot_state == IDLE){
                    // adjust right pan-tilt kinematics offset
                    m_offset_for_upper_r = offset;
                    setRightAngles(0,0);
                }
                else{
                    throw Offset::SetFailException();
                }
            }

            // set left pan-tilt kinematics offset
            void setOffsetLeft(Offset offset){
                // right kinematics offset is applied only IDLE state
                if(m_robot_state == IDLE){
                    // adjust left pan-tilt kinematics offset
                    m_offset_for_upper_l = offset;
                    setLeftAngles(0,0);
                }
                else{
                    throw Offset::SetFailException();
                }
            }

            // stop entire scan procedure
            void stop(void){
                // stop up-down motion
                setLowerVelocity(0);        
                m_mutex_for_comm.lock_upgrade();
                m_is_stop = true;
                m_mutex_for_comm.unlock_upgrade();
            }

            // start scan procedure
            void scan_start(void){
                m_is_scan_start = true;
            }

            // stop scan procedure
            void scan_stop(void){
                setLowerVelocity(0);
                m_is_scan_start = false;
                m_robot_state = IDLE;
            }

            // start async read operation of each module
            void start_read(){
                m_mutex_for_comm.lock_upgrade();
                m_socket_for_lower.async_read_some(boost::asio::buffer(m_recv_buffer_for_lower, BUFFER_LENGTH),
                        boost::bind(&HyundaiController::handle_read_for_lower, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                m_socket_for_upper_l.async_read_some(boost::asio::buffer(m_recv_buffer_for_upper_l, BUFFER_LENGTH),
                        boost::bind(&HyundaiController::handle_read_for_upper_l, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                m_socket_for_upper_r.async_read_some(boost::asio::buffer(m_recv_buffer_for_upper_r, BUFFER_LENGTH),
                        boost::bind(&HyundaiController::handle_read_for_upper_r, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                m_socket_for_laser_l.async_read_some(boost::asio::buffer(m_recv_buffer_for_laser_l, BUFFER_LENGTH),
                        boost::bind(&HyundaiController::handle_read_for_laser_l, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                m_socket_for_laser_r.async_read_some(boost::asio::buffer(m_recv_buffer_for_laser_r, BUFFER_LENGTH),
                        boost::bind(&HyundaiController::handle_read_for_laser_r, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                m_socket_for_laser_b.async_read_some(boost::asio::buffer(m_recv_buffer_for_laser_b, BUFFER_LENGTH),
                        boost::bind(&HyundaiController::handle_read_for_laser_b, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                m_mutex_for_comm.unlock_upgrade();

            }

            // print Robot information (Sensor data, current angles, etc.)
            void printRobotInfo() const noexcept{
                std::cout << boost::format("\t==== ROBOT ====\n\t- vel lower: %f degree/sec\n\t- ang left: %f degree,  %f degree\n\t- ang right: %f degree,  %f degree\n\t") 
                   % m_curr_vel % m_curr_ang_l[0] % m_curr_ang_l[1] % m_curr_ang_r[0] % m_curr_ang_r[1];
                std::cout << boost::format("- distance left: %f m\n\t- distance right: %f m\n\t- distance bottom: %f m\n\t") 
                    % m_curr_dist_l % m_curr_dist_r % m_curr_dist_b;
                std::cout << boost::format("- roll: %f degree\n\t- pitch: %f degree\n\t- yaw: %f degree ") 
                    % m_curr_roll % m_curr_pitch % m_curr_yaw << std::endl;
            }

            // print 3D point obtained by Left sensor
            void printLeftPoint(){
                ublas::vector<float> point = getLeftPoint(m_curr_ang_l[0], m_curr_ang_l[1], m_curr_dist_l*1000.0);
                std::cout << "LEFT POINT: " << point << std::endl;
            }

            // print 3D point obtained by Right sensor
            void printRightPoint(){
                ublas::vector<float> point = getRightPoint(m_curr_ang_r[0], m_curr_ang_r[1], m_curr_dist_r*1000.0);
                std::cout << "RIGHT POINT: " << point << std::endl;
            }

            // start initialization of left pan-tilt module
            void initializeLeft(){
                PacketInitialize packet(BID::BID_UPPER_LEFT);
                std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                write(m_socket_for_upper_l, packet_stream, m_send_buffer_for_upper_l);
            }

            // start initialization of right pan-tilt module
            void initializeRight(){
                PacketInitialize packet(BID::BID_UPPER_RIGHT);
                std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                write(m_socket_for_upper_r, packet_stream, m_send_buffer_for_upper_r);
            }

            // start initialization of laser sensor modules (left, right, bottom)
            void initializeSensors(){
                std::vector<std::string> commands;
                commands.push_back("uc+2+1\r\n");
                commands.push_back("s\r\n");
                sendSensorCommands(SID_L, commands);
                sendSensorCommands(SID_R, commands);
                sendSensorCommands(SID_B, commands);
            }

            // start all laser sensors to measure distance 
            void startSensors(){
                startSensor(SID_L);
                startSensor(SID_R);
                startSensor(SID_B);
            }

            // stop all laser sensors to measure distance 
            void stopSensors(){
                stopSensor(SID_L);
                stopSensor(SID_R);
                stopSensor(SID_B);
            }

            // start laser sensor to measure distance at higher rate
            void startSensor(SID sid){
                std::vector<std::string> commands;
                commands.push_back("h\r\n");
                sendSensorCommands(sid, commands);
                switch(sid){
                    case SID_L: m_curr_dist_l = 0; m_is_start_laser_l = true; break;
                    case SID_R: m_curr_dist_r = 0; m_is_start_laser_r = true; break;
                    case SID_B: m_curr_dist_b = 0; m_is_start_laser_b = true; break;
                }
            }

            // stop laser sensor to measure distance
            void stopSensor(SID sid){
                std::vector<std::string> commands;
                commands.push_back("c\r\n");
                commands.push_back("p\r\n");
                sendSensorCommands(sid, commands);

                switch(sid){
                    case SID_L: m_curr_dist_l = 0; m_is_start_laser_l = false; break;
                    case SID_R: m_curr_dist_r = 0; m_is_start_laser_r = false; break;
                    case SID_B: m_curr_dist_b = 0; m_is_start_laser_b = false; break;
                }
            }


            // send sensor operating commands to each sensor
            void sendSensorCommands(SID sid, const std::vector<std::string>& commands){
                for(auto it = commands.begin(); it != commands.end(); ++it){
                    std::string command = std::string("s") + boost::lexical_cast<std::string>((int)sid)+*it;
                    std::vector<unsigned char> packet_stream(command.begin(), command.end());
                    switch(sid){
                        case SID_L: write(m_socket_for_laser_l, packet_stream, m_send_buffer_for_laser_l); break;
                        case SID_R: write(m_socket_for_laser_r, packet_stream, m_send_buffer_for_laser_r); break;
                        case SID_B: write(m_socket_for_laser_b, packet_stream, m_send_buffer_for_laser_b); break;
                    }
                }
            }



            // set Left pan-tilt angles
            void setLeftAngles(float angle0, float angle1){
                PacketHostToSlaveAng packet(BID::BID_UPPER_LEFT, angle0+m_offset_for_upper_l.m_ang_0, angle1+m_offset_for_upper_l.m_ang_1);
                std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                write(m_socket_for_upper_l, packet_stream, m_send_buffer_for_upper_l);
            }

            // set Right pan-tilt angles
            void setRightAngles(float angle0, float angle1){
                PacketHostToSlaveAng packet(BID::BID_UPPER_RIGHT, angle0+m_offset_for_upper_r.m_ang_0, angle1+m_offset_for_upper_r.m_ang_1);
                std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                write(m_socket_for_upper_r, packet_stream, m_send_buffer_for_upper_r);
            }

            // set Left pan-tilt velocities
            void setLeftVelocities(float velocity0, float velocity1){
                PacketHostToSlaveVel2 packet(BID::BID_UPPER_LEFT, velocity0, velocity1);
                std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                write(m_socket_for_upper_l, packet_stream, m_send_buffer_for_upper_l);
            }

            // set Right pan-tilt velocities
            void setRightVelocities(float velocity0, float velocity1){
                PacketHostToSlaveVel2 packet(BID::BID_UPPER_RIGHT, velocity0, velocity1);
                std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                write(m_socket_for_upper_r, packet_stream, m_send_buffer_for_upper_r);
            }

            // set up-down velocity
            void setLowerVelocity(float velocity){
                PacketHostToSlaveVel packet(BID::BID_LOWER, velocity);
                std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                write(m_socket_for_lower, packet_stream, m_send_buffer_for_lower);
            }


            private:

            // packet handler routine for echo reply
            void routineForEchoTest(BID bid, const std::shared_ptr<PacketEchoTest<std::vector<unsigned char>>> packet){
                if(m_is_debug) std::cout <<  "[" << bid << "] " << "ECHO TEST: " << packet->getString<std::string>() << std::endl;            

                switch(bid){
                    case BID_UPPER_LEFT:
                        {

                        }
                        break;
                    case BID_UPPER_RIGHT:
                        {

                        }
                        break;
                    case BID_LOWER:
                        {

                        }
                        break;
                }
            }

            // packet handler routine to inform pan-tilt angles 
            void routineForSlaveToHostAng(BID bid, const std::shared_ptr<PacketSlaveToHostAng> packet){
                //std::cout <<  "[" << bid << "] "  << "Slave To Host Angle: \t" << packet->getMotor1Angle() << "\t" << packet->getMotor2Angle() << std::endl;
                switch(bid){
                    case BID_UPPER_LEFT:
                        {
                            // save cur angles 
                            m_curr_ang_l[0] = packet->getMotor1Angle()-m_offset_for_upper_l.m_ang_0;
                            m_curr_ang_l[1] = packet->getMotor2Angle()-m_offset_for_upper_l.m_ang_1;
                        }
                        break;
                    case BID_UPPER_RIGHT:
                        {
                            // save cur angles 
                            m_curr_ang_r[0] = packet->getMotor1Angle()-m_offset_for_upper_r.m_ang_0;
                            m_curr_ang_r[1] = packet->getMotor2Angle()-m_offset_for_upper_r.m_ang_1;
                        }
                        break;
                    case BID_LOWER:
                        {
                            // NOTHING TO DO
                        }
                        break;
                }
            }


            // packet handler routine to inform up-down velocity 
            void routineForSlaveToHostVel(BID bid, const std::shared_ptr<PacketSlaveToHostVel> packet){
                //std::cout <<  "[" << bid << "] "  << "Slave To Host Velocity: \t" << packet->getMotorVelocity() << std::endl;
                switch(bid){
                    case BID_UPPER_LEFT:
                        {
                        }
                        break;
                    case BID_UPPER_RIGHT:
                        {
                        }
                        break;
                    case BID_LOWER:
                        {
                            // save cur up-down velocity
                            m_curr_vel = packet->getMotorVelocity();
                        }
                        break;
                }
            }

            // packet handler routine to inform pan-tilt velocities 
            void routineForSlaveToHostVel2(BID bid, const std::shared_ptr<PacketSlaveToHostVel2> packet){
                if(m_is_debug) std::cout <<  "[" << bid << "] "  << "Slave To Host Velocity2: \t" << packet->getMotor1Velocity() << "\t" << packet->getMotor2Velocity() << std::endl;
                switch(bid){
                    case BID_UPPER_LEFT:
                        {
                            // save cur pan-tilt velocities
                            m_curr_vel_l[0] = packet->getMotor1Velocity();
                            m_curr_vel_l[1] = packet->getMotor2Velocity();
                        }
                        break;
                    case BID_UPPER_RIGHT:
                        {
                            // save cur pan-tilt velocities
                            m_curr_vel_r[0] = packet->getMotor1Velocity();
                            m_curr_vel_r[1] = packet->getMotor2Velocity();
                        }
                        break;
                    case BID_LOWER:
                        {
                            // NOTHING
                        }
                        break;
                }
            }

            // packet handler routine for ACK from module
            void routineForAck(BID bid, const std::shared_ptr<PacketAck> packet){
                if(m_is_debug) std::cout <<  "[" << bid << "] "  << "ACK: \t" << packet->getSID() << "\t" << packet->getPacketType() << std::endl;  
                switch(bid){
                    case BID_UPPER_LEFT:
                        {

                        }
                        break;
                    case BID_UPPER_RIGHT:
                        {

                        }
                        break;
                    case BID_LOWER:
                        {

                        }
                        break;
                }
            }

            // packet handler routine to notify that slave is finished task
            void routineForSlaveComplete(BID bid, const std::shared_ptr<PacketSlaveComplete> packet){
                //std::cout <<  "[" << bid << "] "  << "Slave Complete" << packet->getSID() << "\t" << packet->getPacketType() << std::endl;  
                PacketType complete_type = packet->getCompleteType();
                switch(bid){
                    case BID_UPPER_LEFT:
                        {
                            switch(complete_type){
                                case PACKET_TYPE_INITIALIZE:
                                    {
                                        std::cout << "Initialize Left is completed ... " << std::endl;
                                        m_is_initialize_l = true;
                                        setLeftAngles(0,0);

                                        break;
                                    }
                                case PACKET_TYPE_HOST_TO_SLAVE_ANG:
                                    {

                                        break;
                                    }
                                default:
                                    {

                                        break;
                                    }
                            }

                        }
                        break;
                    case BID_UPPER_RIGHT:
                        {
                            switch(complete_type){
                                case PACKET_TYPE_INITIALIZE:
                                    {
                                        m_is_initialize_r = true;
                                        std::cout << "Initialize Right is completed ... " << std::endl;
                                        setRightAngles(0,0);
                                        break;
                                    }
                                case PACKET_TYPE_HOST_TO_SLAVE_ANG:
                                    {

                                        break;
                                    }
                                default:
                                    {

                                        break;
                                    }
                            }

                        }
                        break;
                    case BID_LOWER:
                        {
                            switch(complete_type){
                                case PACKET_TYPE_INITIALIZE:
                                    {
                                        m_is_initialize_r = true;
                                        std::cout << "Initialize Lower is completed ... " << std::endl;
                                        setRightAngles(0,0);
                                        break;
                                    }
                                case PACKET_TYPE_HOST_TO_SLAVE_VEL:
                                    {

                                        break;
                                    }
                                default:
                                    {

                                        break;
                                    }
                            }

                        }
                        break;
                }
            }

            // routine for laser sensor's jumping condition
            void routineForSensorJumping(SID sid){
                // setting jumping flag
                switch(sid){
                    case SID_L: 
                        {
                            m_is_jumping_laser_l = true;
                            m_is_avail_laser_l = false;
                        }
                        break;
                    case SID_R:
                        {
                            m_is_jumping_laser_r = true;
                            m_is_avail_laser_r = false;
                        }
                        break;
                    case SID_B:
                        {
                            m_is_jumping_laser_b = true;
                            m_is_avail_laser_b = false;
                        }
                        break;
                }

            }


            private:

            // parse received packet stream from laser sensors
            void parsePacketForSensor(SID sid, std::vector<unsigned char>& packet_buffer){
                std::string header;

                switch(sid){
                    case SID_L: header = std::string("g0"); break;
                    case SID_R: header = std::string("g1"); break;
                    case SID_B: header = std::string("g2"); break;
                }

                m_mutex_for_comm.lock_upgrade();
                std::string buffer = std::string(packet_buffer.begin(), packet_buffer.end());
                packet_buffer.clear();
                m_mutex_for_comm.unlock_upgrade();

                std::vector<std::string> splited_packet;

                // split a received packet stream into several streams using header
                boost::split_regex(splited_packet, buffer, boost::regex(header));

                //const float jumping_distance = 5000; // FOR webots
                
                const float jumping_distance = 0.05;    // define software jumping distance condition (m)

                for(auto s = splited_packet.begin(); s!= splited_packet.end(); ++s){
                    try{
                        if(s->size() > 2){
                            if((*s)[0] == 'h' && (*s)[1] == '+' && s->size() == 12){
                                // Normal distance packet is arrived

                                std::string distance_str(s->begin()+2, s->end()-2);
                                float distance = boost::lexical_cast<float>(distance_str)/10000.0;

                                switch(sid){
                                    case SID_L:
                                        {
                                            if(m_curr_dist_l != 0 && std::abs(m_curr_dist_l - distance) > jumping_distance){
                                                // soft jumping is occured
                                                routineForSensorJumping(sid);

                                            }
                                            else{
                                                // save distance value
                                                m_curr_dist_l = distance; 
                                                m_is_avail_laser_l = true; 
                                                m_is_jumping_laser_l = false; 
                                            }
                                            break;
                                        }
                                    case SID_R:
                                        {
                                            if(m_curr_dist_r != 0 && std::abs(m_curr_dist_r - distance) > jumping_distance){
                                                // soft jumping is occured
                                                routineForSensorJumping(sid);
                                            }
                                            else{
                                                // save distance value
                                                m_curr_dist_r = distance; 
                                                m_is_avail_laser_r = true; 
                                                m_is_jumping_laser_r = false; 
                                            }
                                            break;
                                        }
                                    case SID_B:
                                        {
                                            if(m_curr_dist_b != 0 && std::abs(m_curr_dist_b - distance) > jumping_distance){
                                                // soft jumping is occured
                                                routineForSensorJumping(sid);
                                            }
                                            else{
                                                // save distance value
                                                m_curr_dist_b = distance; 
                                                m_is_avail_laser_b = true; 
                                                m_is_jumping_laser_b = false; 
                                            }
                                            break;
                                        }
                                }



                            }
                            else if((*s)[0] == '@' && (*s)[1] == 'E' && s->size() == 7){
                                // real jumping condition is occured
                                //routineForSensorJumping(sid);
                                //std::cout << "ERROR OCCURED: " << sid << "\t" << std::string(s->begin()+2, s->end());
                            }
                            else if(s == splited_packet.end()-1){
                                // remained packet stream
                                packet_buffer.insert(packet_buffer.end(), s->begin(), s->end());
                            }

                        }
                        else if(s == splited_packet.end()-1){
                            // remained packet stream
                            packet_buffer.insert(packet_buffer.end(), s->begin(), s->end());
                        }

                    } catch(boost::bad_lexical_cast&){
                        std::cout << "BAD cast occured" << std::endl;
                        continue;
                    }
                }
            }

            // parse received packet from controller (Left, Right pan-tilt controllers, up-down motion controller)
            void parsePacketForBoard(BID bid, std::vector<unsigned char>& packet_buffer){

                // split a Packet stream into several streams using header
                m_mutex_for_comm.lock_shared();
                std::vector<std::string> splitted_packet = splitPacket(packet_buffer);
                m_mutex_for_comm.unlock_shared();

                m_mutex_for_comm.lock_upgrade();
                packet_buffer.clear();
                m_mutex_for_comm.unlock_upgrade();

                for(auto s = splitted_packet.begin(); s != splitted_packet.end(); ++s){

                    try{
                        std::vector<unsigned char> packet_stream(s->begin(), s->end()); 

                        // Make packet from a packet stream
                        std::shared_ptr<Packet>  packet = parsePacketFromStream(packet_stream);

                        // Conduct handler as per packet's type
                        switch(packet->getPacketType()){
                            case PACKET_TYPE_ECHO_TEST:
                                {
                                    auto packet_echo_test = std::dynamic_pointer_cast<PacketEchoTest<std::vector<unsigned char>>>(packet);
                                    routineForEchoTest(bid, packet_echo_test);
                                    break;
                                }
                            case PACKET_TYPE_SLAVE_TO_HOST_ANG:
                                {
                                    auto packet_ang = std::dynamic_pointer_cast<PacketSlaveToHostAng>(packet);
                                    routineForSlaveToHostAng(bid, packet_ang);
                                    break;
                                }
                            case PACKET_TYPE_SLAVE_TO_HOST_VEL:
                                {
                                    auto packet_vel = std::dynamic_pointer_cast<PacketSlaveToHostVel>(packet);
                                    routineForSlaveToHostVel(bid, packet_vel);
                                    break;
                                }
                            case PACKET_TYPE_SLAVE_TO_HOST_VEL2:
                                {
                                    auto packet_vel = std::dynamic_pointer_cast<PacketSlaveToHostVel2>(packet);
                                    routineForSlaveToHostVel2(bid, packet_vel);
                                    break;
                                }
                            case PACKET_TYPE_ACK:
                                {
                                    auto packet_ack = std::dynamic_pointer_cast<PacketAck>(packet);
                                    routineForAck(bid, packet_ack);
                                    break;
                                }
                            case PACKET_TYPE_SLAVE_COMPLETE:
                                {
                                    auto packet_slave_complete = std::dynamic_pointer_cast<PacketSlaveComplete>(packet);
                                    routineForSlaveComplete(bid, packet_slave_complete);
                                    break;
                                }

                            default: throw UnknownPacketException();
                        }

                    } catch (ShortPacketException& e){
                        if(s->size() != 0){
                            // save un-completed packet stream 
                            if(s ==  splitted_packet.end()-1){
                                m_mutex_for_comm.lock_upgrade();
                                packet_buffer.insert(packet_buffer.begin(), s->begin(), s->end());
                                m_mutex_for_comm.unlock_upgrade();
                            }
                        }
                        continue;
                    } catch (ChecksumFailureException& e){
                        std::cerr << "ChecksumFailureException" << std::endl;
                        continue;
                    } catch (UnknownPacketException& e){
                        std::cerr << "UnknownPacketException" << std::endl;
                        continue;
                    } catch (InvalidDataException& e){
                        std::cerr << "InvalidDataException" << std::endl;
                        continue;
                    }

                }   // for


            }

            // sync write operation 
            void write(SOCKET& socket, const std::vector<unsigned char>& packet, unsigned char* send_buffer){
                if(packet.size() < BUFFER_LENGTH){
                    std::copy(packet.begin(), packet.end(), send_buffer);
                    m_mutex_for_comm.lock_upgrade();
                    socket.write_some(boost::asio::buffer(send_buffer, packet.size()));
                    m_mutex_for_comm.unlock_upgrade();
                }
                else{
                    std::cout << "ERROR, SEND BUFFER Overflow" << std::endl;
                }
            }
            

            // async read handler for up-down controller
            void handle_read_for_lower(const boost::system::error_code& error, size_t bytes_transferred){
                // When receive packet through session
                if(!error){
                    //std::cout << "HANDLE READ: " << bytes_transferred <<  std::endl;
                    m_mutex_for_comm.lock_upgrade();
                    m_recv_packet_for_lower.insert(m_recv_packet_for_lower.end(), std::begin(m_recv_buffer_for_lower), std::begin(m_recv_buffer_for_lower)+bytes_transferred);
                    m_socket_for_lower.async_read_some(boost::asio::buffer(m_recv_buffer_for_lower,BUFFER_LENGTH), 
                            boost::bind(&HyundaiController::handle_read_for_lower, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                    m_mutex_for_comm.unlock_upgrade();
                }
                else{
                }
            }

            // async read handler for left pan-tilt controller
            void handle_read_for_upper_l(const boost::system::error_code& error, size_t bytes_transferred){
                // When receive packet through session
                if(!error){
                    //std::cout << "HANDLE READ: " << bytes_transferred <<  std::endl;
                    m_mutex_for_comm.lock_upgrade();
                    m_recv_packet_for_upper_l.insert(m_recv_packet_for_upper_l.end(), std::begin(m_recv_buffer_for_upper_l), std::begin(m_recv_buffer_for_upper_l)+bytes_transferred);
                    m_socket_for_upper_l.async_read_some(boost::asio::buffer(m_recv_buffer_for_upper_l,BUFFER_LENGTH), 
                            boost::bind(&HyundaiController::handle_read_for_upper_l, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                    m_mutex_for_comm.unlock_upgrade();
                }
                else{
                }
            }

            // async read handler for right pan-tilt controller
            void handle_read_for_upper_r(const boost::system::error_code& error, size_t bytes_transferred){
                // When receive packet through session
                if(!error){
                    //std::cout << "HANDLE READ: " << bytes_transferred <<  std::endl;
                    m_mutex_for_comm.lock_upgrade();
                    m_recv_packet_for_upper_r.insert(m_recv_packet_for_upper_r.end(), std::begin(m_recv_buffer_for_upper_r), std::begin(m_recv_buffer_for_upper_r)+bytes_transferred);
                    m_socket_for_upper_r.async_read_some(boost::asio::buffer(m_recv_buffer_for_upper_r,BUFFER_LENGTH), 
                            boost::bind(&HyundaiController::handle_read_for_upper_r, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                    m_mutex_for_comm.unlock_upgrade();
                }
                else{
                }
            }

            // async read handler for left laser sensor
            void handle_read_for_laser_l(const boost::system::error_code& error, size_t bytes_transferred){
                // When receive packet through session
                if(!error){
                    //std::cout << "HANDLE READ: " << bytes_transferred <<  std::endl;
                    m_mutex_for_comm.lock_upgrade();
                    m_recv_packet_for_laser_l.insert(m_recv_packet_for_laser_l.end(), std::begin(m_recv_buffer_for_laser_l), std::begin(m_recv_buffer_for_laser_l)+bytes_transferred);
                    m_socket_for_laser_l.async_read_some(boost::asio::buffer(m_recv_buffer_for_laser_l,BUFFER_LENGTH), 
                            boost::bind(&HyundaiController::handle_read_for_laser_l, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                    m_mutex_for_comm.unlock_upgrade();
                }
                else{
                }
            }
            // async read handler for right laser sensor
            void handle_read_for_laser_r(const boost::system::error_code& error, size_t bytes_transferred){
                // When receive packet through session
                if(!error){
                    //std::cout << "HANDLE READ: " << bytes_transferred <<  std::endl;
                    m_mutex_for_comm.lock_upgrade();
                    m_recv_packet_for_laser_r.insert(m_recv_packet_for_laser_r.end(), std::begin(m_recv_buffer_for_laser_r), std::begin(m_recv_buffer_for_laser_r)+bytes_transferred);
                    m_socket_for_laser_r.async_read_some(boost::asio::buffer(m_recv_buffer_for_laser_r,BUFFER_LENGTH), 
                            boost::bind(&HyundaiController::handle_read_for_laser_r, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                    m_mutex_for_comm.unlock_upgrade();
                }
                else{
                }
            }
            
            // async read handler for bottom laser sensor
            void handle_read_for_laser_b(const boost::system::error_code& error, size_t bytes_transferred){
                // When receive packet through session
                if(!error){
                    //std::cout << "HANDLE READ: " << bytes_transferred <<  std::endl;
                    m_mutex_for_comm.lock_upgrade();
                    m_recv_packet_for_laser_b.insert(m_recv_packet_for_laser_b.end(), std::begin(m_recv_buffer_for_laser_b), std::begin(m_recv_buffer_for_laser_b)+bytes_transferred);
                    m_socket_for_laser_b.async_read_some(boost::asio::buffer(m_recv_buffer_for_laser_b,BUFFER_LENGTH), 
                            boost::bind(&HyundaiController::handle_read_for_laser_b, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                    m_mutex_for_comm.unlock_upgrade();
                }
                else{
                }
            }


            // wait control time 
            void waitControlTime(){

                boost::timer::nanosecond_type diff = 0;

                static boost::timer::auto_cpu_timer high_resolution_timer;
                static boost::timer::nanosecond_type prior_time_count = high_resolution_timer.elapsed().wall;
                static int accumulated_diff = 0;
                static double control_time_mean = 0;
                static double control_time_variance = 0;
                static double control_time_count = 0;

                const double scale = 0.5; // Scale == 0.3 ÀÏ ¶§, Average = 10 ms

                diff = (high_resolution_timer.elapsed().wall - prior_time_count);   // running¿¡ ÇÊ¿äÇÑ ½Ã°£À» °í·ÁÇÏ¿© Á¦¾î.


                while( diff < (m_control_time-accumulated_diff*scale) ){

                    boost::timer::nanosecond_type wait_time = (m_control_time - accumulated_diff*scale - diff);

                    boost::this_thread::sleep(boost::posix_time::microseconds(wait_time/1000));
                    diff = high_resolution_timer.elapsed().wall - prior_time_count;
                }

                diff = high_resolution_timer.elapsed().wall - prior_time_count;
                accumulated_diff += ((int)diff - m_control_time);

                control_time_mean = (diff/1000.0/1000.0 + control_time_mean*control_time_count) / (control_time_count + 1);
                control_time_variance = ((diff/1000.0/1000.0 - control_time_mean) * (diff/1000.0/1000.0 - control_time_mean)
                        + control_time_variance*control_time_count) / (control_time_count  + 1);
                control_time_count = control_time_count + 1;

                //std::cout << "[" << control_time_count << "] TIME: "<< diff/1000.0/1000.0 << "ms\t" ;
                //std::cout << "\tMean: \t" << control_time_mean << " Variance: \t" << control_time_variance << std::endl;

                prior_time_count = high_resolution_timer.elapsed().wall;
            }




            private:


            // Socket to communicate with each module
            SOCKET& m_socket_for_lower;
            SOCKET& m_socket_for_upper_l;
            SOCKET& m_socket_for_upper_r;
            SOCKET& m_socket_for_laser_l;
            SOCKET& m_socket_for_laser_r;
            SOCKET& m_socket_for_laser_b;


            boost::shared_mutex m_mutex_for_comm;

            std::vector<unsigned char> m_recv_packet_for_lower;
            std::vector<unsigned char> m_recv_packet_for_upper_l;
            std::vector<unsigned char> m_recv_packet_for_upper_r;
            std::vector<unsigned char> m_recv_packet_for_laser_l;
            std::vector<unsigned char> m_recv_packet_for_laser_r;
            std::vector<unsigned char> m_recv_packet_for_laser_b;

            unsigned char m_recv_buffer_for_lower[BUFFER_LENGTH];
            unsigned char m_recv_buffer_for_upper_l[BUFFER_LENGTH];
            unsigned char m_recv_buffer_for_upper_r[BUFFER_LENGTH];
            unsigned char m_recv_buffer_for_laser_l[BUFFER_LENGTH];
            unsigned char m_recv_buffer_for_laser_r[BUFFER_LENGTH];
            unsigned char m_recv_buffer_for_laser_b[BUFFER_LENGTH];

            unsigned char m_send_buffer_for_lower[BUFFER_LENGTH];
            unsigned char m_send_buffer_for_upper_l[BUFFER_LENGTH];
            unsigned char m_send_buffer_for_upper_r[BUFFER_LENGTH];
            unsigned char m_send_buffer_for_laser_l[BUFFER_LENGTH];
            unsigned char m_send_buffer_for_laser_r[BUFFER_LENGTH];
            unsigned char m_send_buffer_for_laser_b[BUFFER_LENGTH];

            private: 

            // Measurement offset 
            MeasureOffset m_measure_offset;

            // Kinematics offset for left pan-tilt
            Offset m_offset_for_upper_l;
            // Kinematics offset for right pan-tilt
            Offset m_offset_for_upper_r;

            // Robot state
            RobotState m_robot_state;

            // Scan state
            ScanState m_scan_state_l;
            ScanState m_scan_state_r;


            // Measurement data from Left sensor
            Measure m_left_measure;
            // Measurement data from Right Sensor
            Measure m_right_measure;


            int m_control_time;

            bool m_is_scan_start;

            bool m_is_stop;
            bool m_is_debug;

            bool m_is_start_laser_l;
            bool m_is_start_laser_r;
            bool m_is_start_laser_b;

            bool m_is_jumping_laser_l;
            bool m_is_jumping_laser_r;
            bool m_is_jumping_laser_b;

            bool m_is_avail_laser_l;
            bool m_is_avail_laser_r;
            bool m_is_avail_laser_b;

            bool m_is_initialize_l;
            bool m_is_initialize_r;


            float m_curr_vel; 
            float m_curr_ang_l[2];
            float m_curr_ang_r[2];
            float m_curr_vel_l[2];
            float m_curr_vel_r[2];

            float m_curr_dist_l;
            float m_curr_dist_r;
            float m_curr_dist_b;

            float m_curr_roll;
            float m_curr_pitch;
            float m_curr_yaw;

        };
}




// Get User's keyboard input with various template 
template <typename T>
T getInput()
{
    T result;
    std::cin >> result;
    if (std::cin.fail() || std::cin.get() != '\n'){
        std::cin.clear();
        while (std::cin.get() != '\n');
        throw std::ios_base::failure("Invalid input.");
    }
    return result;
}



int main(){
    boost::asio::io_service io1;// io2, io3;

    goodguy::MeasureOffset measure_offset;
    goodguy::Offset offset_for_upper_l;
    goodguy::Offset offset_for_upper_r;


    // Load saved measurement offset 
    std::ifstream measure_offset_file("../measure_offset.txt");
    measure_offset_file >> measure_offset;
    measure_offset_file.close();

    // Load saved kinematics offset of left pan-tilt
    std::ifstream offset_file_for_upper_l("../offset_for_left.txt");
    offset_file_for_upper_l >> offset_for_upper_l;
    offset_file_for_upper_l.close();

    // Load saved kinematics offset of right pan-tilt
    std::ifstream offset_file_for_upper_r("../offset_for_right.txt");
    offset_file_for_upper_r >> offset_for_upper_r;
    offset_file_for_upper_r.close();


#ifdef SIMULATION_MODE      // FOR Webots simulation
    // define control time (ns)
    const int control_time_for_hyundai_robot = 2*1000*1000;

    using boost::asio::ip::tcp;

    tcp::resolver resolver1(io1);
    tcp::resolver resolver2(io1);
    tcp::resolver resolver3(io1);

#ifdef ROBOT_1
    tcp::resolver::query query_for_lower(tcp::v4(), "127.0.0.1", "3111");
    tcp::resolver::query query_for_upper_l(tcp::v4(), "127.0.0.1", "3112");
    tcp::resolver::query query_for_upper_r(tcp::v4(), "127.0.0.1", "3113");
    tcp::resolver::query query_for_laser_l(tcp::v4(), "127.0.0.1", "3114");
    tcp::resolver::query query_for_laser_r(tcp::v4(), "127.0.0.1", "3115");
    tcp::resolver::query query_for_laser_b(tcp::v4(), "127.0.0.1", "3116");
#elif ROBOT_2
    tcp::resolver::query query_for_lower(tcp::v4(), "127.0.0.1", "3211");
    tcp::resolver::query query_for_upper_l(tcp::v4(), "127.0.0.1", "3212");
    tcp::resolver::query query_for_upper_r(tcp::v4(), "127.0.0.1", "3213");
    tcp::resolver::query query_for_laser_l(tcp::v4(), "127.0.0.1", "3214");
    tcp::resolver::query query_for_laser_r(tcp::v4(), "127.0.0.1", "3215");
    tcp::resolver::query query_for_laser_b(tcp::v4(), "127.0.0.1", "3216");
#elif ROBOT_3
    tcp::resolver::query query_for_lower(tcp::v4(), "127.0.0.1", "3311");
    tcp::resolver::query query_for_upper_l(tcp::v4(), "127.0.0.1", "3312");
    tcp::resolver::query query_for_upper_r(tcp::v4(), "127.0.0.1", "3313");
    tcp::resolver::query query_for_laser_l(tcp::v4(), "127.0.0.1", "3314");
    tcp::resolver::query query_for_laser_r(tcp::v4(), "127.0.0.1", "3315");
    tcp::resolver::query query_for_laser_b(tcp::v4(), "127.0.0.1", "3316");
#endif
    //tcp::resolver::query query(tcp::v4(), "127.0.0.1", "3113");
    tcp::resolver::iterator iterator_for_lower = resolver1.resolve(query_for_lower);
    tcp::resolver::iterator iterator_for_upper_l = resolver1.resolve(query_for_upper_l);
    tcp::resolver::iterator iterator_for_upper_r = resolver2.resolve(query_for_upper_r);
    tcp::resolver::iterator iterator_for_laser_l = resolver2.resolve(query_for_laser_l);
    tcp::resolver::iterator iterator_for_laser_r = resolver3.resolve(query_for_laser_r);
    tcp::resolver::iterator iterator_for_laser_b = resolver3.resolve(query_for_laser_b);

    tcp::socket socket_for_lower(io1);
    tcp::socket socket_for_upper_l(io1);
    tcp::socket socket_for_upper_r(io1);
    tcp::socket socket_for_laser_l(io1);
    tcp::socket socket_for_laser_r(io1);
    tcp::socket socket_for_laser_b(io1);

    try{
        // Request to connect webots simulator as per module
        boost::asio::connect(socket_for_lower, iterator_for_lower);
        boost::asio::connect(socket_for_upper_l, iterator_for_upper_l);
        boost::asio::connect(socket_for_upper_r, iterator_for_upper_r);
        boost::asio::connect(socket_for_laser_l, iterator_for_laser_l);
        boost::asio::connect(socket_for_laser_r, iterator_for_laser_r);
        boost::asio::connect(socket_for_laser_b, iterator_for_laser_b);
    }catch(std::exception& e){
        std::cerr << "Exception: " << e.what() << std::endl;
        std::cout << "Connection failed" << std::endl;
        return 0;
    }

    std::cout << "Connection Success" << std::endl;

    // Create Control Object using TCP Socket
    goodguy::HyundaiController<tcp::socket> hyundai_robot(
            socket_for_lower,
            socket_for_upper_l,
            socket_for_upper_r,
            socket_for_laser_l,
            socket_for_laser_r,
            socket_for_laser_b,
            measure_offset,
            offset_for_upper_l,
            offset_for_upper_r,
            control_time_for_hyundai_robot);

    // Make thread to control robot
    boost::thread thread_for_hyundai_robot(boost::bind(&goodguy::HyundaiController<tcp::socket>::run, &hyundai_robot));

#else           // FOR Real-robot

    // define control time (ns)
    const int control_time_for_hyundai_robot = 5*1000*1000;

    boost::asio::serial_port serial_for_lower(io1);
    boost::asio::serial_port serial_for_upper_l(io1);
    boost::asio::serial_port serial_for_upper_r(io1);
    boost::asio::serial_port serial_for_laser_l(io1);
    boost::asio::serial_port serial_for_laser_r(io1);
    boost::asio::serial_port serial_for_laser_b(io1);

    try{

        // Open serial device files to communicate each module
#ifdef ODROID
        serial_for_lower.open("/dev/ttyBRD_B");
        serial_for_upper_l.open("/dev/ttyBRD_L");
        serial_for_upper_r.open("/dev/ttyBRD_R");
        serial_for_laser_l.open("/dev/ttyLASER_L");
        serial_for_laser_r.open("/dev/ttyLASER_R");
        serial_for_laser_b.open("/dev/ttyLASER_B");
#else
        serial_for_upper_l.open("/dev/ttyACM0");
        serial_for_upper_r.open("/dev/ttyACM1");
        serial_for_lower.open("/dev/ttyACM2");
        serial_for_laser_l.open("/dev/ttyUSB1");
        serial_for_laser_r.open("/dev/ttyUSB2");
        serial_for_laser_b.open("/dev/ttyUSB3");
#endif
    }catch(std::exception& e){
        std::cerr << "Exception: " << e.what() << std::endl;
        std::cout << "Serial port open failed" << std::endl;
        return 0;
    }


    // setting UART configuration
    serial_for_lower.set_option(boost::asio::serial_port::baud_rate(1000000));
    serial_for_upper_l.set_option(boost::asio::serial_port::baud_rate(1000000));
    serial_for_upper_r.set_option(boost::asio::serial_port::baud_rate(1000000));
    serial_for_laser_l.set_option(boost::asio::serial_port::baud_rate(115200));
    serial_for_laser_r.set_option(boost::asio::serial_port::baud_rate(115200));
    serial_for_laser_b.set_option(boost::asio::serial_port::baud_rate(115200));

    // Create Control Object using Serial Interface
    goodguy::HyundaiController<boost::asio::serial_port> hyundai_robot(
            serial_for_lower,
            serial_for_upper_l,
            serial_for_upper_r,
            serial_for_laser_l,
            serial_for_laser_r,
            serial_for_laser_b,
            measure_offset,
            offset_for_upper_l,
            offset_for_upper_r,
            control_time_for_hyundai_robot);

    // Make thread to control robot
    boost::thread thread_for_hyundai_robot(boost::bind(&goodguy::HyundaiController<boost::asio::serial_port>::run, &hyundai_robot));

#endif

    // start async read operation
    hyundai_robot.start_read();

    // initialize laser sensor
    hyundai_robot.initializeSensors();

    // make thread for Boost.Asio module
    boost::thread thread_for_io1(boost::bind(&boost::asio::io_service::run, &io1));


    bool is_stop = false;

    std::cout << "Controller Program for Hyundai Robot" << std::endl;

    /*************************************************************
     *
     * COMMANDS
     *
     * - p: print robot information (current pan-tilt angles, distances, velocities)
     *
     * - l: Left Pan-Tilt manual control
     *   -> v: velocities setting
     *   -> a: angles setting
     *   -> p: print 3D points
     *
     * - r: Right Pan-Tilt manual control
     *   -> v: velocities setting
     *   -> a: angles setting
     *   -> p: print 3D points
     *
     * - i: initialization step
     *   -> l: request left initialization at control board using Photo-Interrupt
     *   -> r: request right initialization at control board using Photo-Interrupt
     *   -> s: request sensor initialization at each sensor
     *   -> m: request to move measure positions at each control board
     *
     * - o: Adjust offset values for Pan-Tilt modules and Measurement position
     *   -> l: (1-pan ,2-tilt) for Left Pan-Tilt module
     *   -> r: (1-pan ,2-tilt) for Right Pan-Tilt module
     *   -> m: (1,2,3,4,5,6) for starting measurement position
     *
     * - s: start Laser sensors (Left, Right, Bottom)
     *
     * - S: stop Laser sensors (Left, Right, Bottom)
     *
     * - q: quit control program  
     *
     * - 1: start scan motion  
     *
     * - 2: stop scan motion  
     *
     *************************************************************/
    

    // text-based user interface
    while(!is_stop){

        unsigned char input = 0;
        try{
            // get user command
            input = getInput<unsigned char>();
        } catch(std::exception& e){
            input = 0x00;
        }

        switch(input){
            case 'p':
                {
                    // print robot's information
                    hyundai_robot.printRobotInfo();
                    break;
                }
            case 'l':
                {
                    // Manual control for Left Pan-Tilt module
                    try{
                        std::cout << "======= LEFT ARM SETTING ===== \n[v: velocity\ta:angle\tp:get 3D point]" << std::endl;
                        std::cout << "INPUT: " << std::endl;
                        char choice = getInput<unsigned char>();
                        switch(choice){
                            case 'v':
                                {
                                    try{
                                        std::cout << "Set Vel (degree per second) - Motor 0: " ;
                                        float vel0 = getInput<float>();
                                        std::cout << "Set Vel (degree per second) - Motor 1: " ;
                                        float vel1 = getInput<float>();
                                        // Set velocities
                                        hyundai_robot.setLeftVelocities(vel0, vel1);
                                    } catch(std::exception& e){
                                        std::cout << "Invalid Float" << std::endl;
                                    }
                                }
                                break;
                            case 'a':
                                {
                                    try{
                                        std::cout << "Set Ang (degree) - Motor 0: " ;
                                        float ang0 = getInput<float>();
                                        std::cout << "Set Ang (degree) - Motor 1: " ;
                                        float ang1 = getInput<float>();
                                        // Set angles
                                        hyundai_robot.setLeftAngles(ang0, ang1);
                                    } catch(std::exception& e){
                                        std::cout << "Invalid Float" << std::endl;
                                    }
                                }
                                break;
                            case 'p':
                                {
                                    // print a 3D point
                                    hyundai_robot.printLeftPoint();
                                }
                                break;
                        }
                    } catch(std::exception& e){
                        std::cout << "INVALID INPUT" << std::endl;

                    }
                    break;
                }
            case 'r':
                {
                    // Manual control for Right Pan-Tilt module
                    try{
                        std::cout << "======= RIGHT ARM SETTING ===== \n[v: velocity\ta:angle\tp:get 3D point]" << std::endl;
                        std::cout << "INPUT: " << std::endl;
                        char choice = getInput<unsigned char>();
                        switch(choice){
                            case 'v':
                                {
                                    try{
                                        std::cout << "Set Vel (degree per second) - Motor 0: " ;
                                        float vel0 = getInput<float>();
                                        std::cout << "Set Vel (degree per second) - Motor 1: " ;
                                        float vel1 = getInput<float>();
                                        // Set velocities
                                        hyundai_robot.setRightVelocities(vel0, vel1);
                                    } catch(std::exception& e){
                                        std::cout << "Invalid Float" << std::endl;
                                    }
                                }
                                break;
                            case 'a':
                                {
                                    try{
                                        std::cout << "Set Ang (degree) - Motor 0: " ;
                                        float ang0 = getInput<float>();
                                        std::cout << "Set Ang (degree) - Motor 1: " ;
                                        float ang1 = getInput<float>();
                                        // Set angles
                                        hyundai_robot.setRightAngles(ang0, ang1);
                                    } catch(std::exception& e){
                                        std::cout << "Invalid Float" << std::endl;
                                    }
                                }
                                break;
                            case 'p':
                                {
                                    // print a 3D point
                                    hyundai_robot.printRightPoint();
                                }
                                break;
                        }
                    } catch(std::exception& e){
                        std::cout << "INVALID INPUT" << std::endl;

                    }
                    break;
                }
            case 'i':
                {
                    // Initilization Step
                    try{
                        std::cout << "Initialization Step - [ l: left, r: right, s: sensors, m: go to measurement pos]" << std::endl;
                        std::cout << "   choice board to initilize (l,r,s,m) : ";
                        char choice = getInput<unsigned char>();
                        switch(choice){
                            case 'l':
                                {
                                    // request Left Pan-Tilt module initialization
                                    hyundai_robot.initializeLeft();
                                    std::cout << "Left initilization step in progress" << std::endl;
                                }
                                break;
                            case 'r':
                                {
                                    // request Right Pan-Tilt module initialization
                                    hyundai_robot.initializeRight();
                                    std::cout << "Right initilization step in progress" << std::endl;
                                }
                                break;
                            case 's':
                                {
                                    // request Sensor initialization (Left, Right, Bottom)
                                    hyundai_robot.initializeSensors();
                                    std::cout << "Sensors initilization step in progress" << std::endl;
                                }
                                break;
                            case 'm':
                                {
                                    // request go to measuring start position 
                                    hyundai_robot.initialMeasurePos();
                                    std::cout << "Measure  initilization position  in progress" << std::endl;
                                }
                                break;
                        }
                    } catch(std::exception& e){
                        std::cout << "INVALID INPUT" << std::endl;

                    }


                    break;
                }

            case 'o':
                {
                    // Adjustment Offset 
                    try{
                        std::cout << "Offset Adjustment step - [ l: left, r: right, m: measure]" << std::endl;
                        std::cout << "   choice board to initilize (l,r,m) : ";
                        char choice = getInput<unsigned char>();
                        switch(choice){
                            case 'l':       // For Left Pan-Tilt Module
                                {
                                    // Get current offset values
                                    goodguy::Offset offset = hyundai_robot.getOffsetLeft();

                                    // Print Current offset values
                                    std::cout << "[LEFT] CURRENT offset values: \n" << offset << std::endl;

                                    std::cout << "choice joint [1,2]: ";
                                    char choice_joint = getInput<char>();

                                    switch(choice_joint){
                                        case '1':
                                            {
                                                std::cout << "Offset value (degree): " << offset.m_ang_0 << " -> " ;
                                                float ang = getInput<float>();
                                                offset.m_ang_0 = ang;
                                                break;
                                            }
                                        case '2':
                                            {
                                                std::cout << "Offset value (degree): " << offset.m_ang_1 << " -> " ;
                                                float ang = getInput<float>();
                                                offset.m_ang_1 = ang;
                                                break;
                                            }

                                        default: throw goodguy::WrongSelection(); break;
                                    }

                                    // Adjust Offset
                                    hyundai_robot.setOffsetLeft(offset);
                                    std::cout << "Offset is applied" << std::endl;

                                    // Save Offset file
                                    std::ofstream offset_file("../offset_for_left.txt", std::ofstream::trunc);
                                    offset_file << offset;
                                    offset_file.close();

                                }
                                break;
                            case 'r':       // For Left Pan-Tilt Module
                                {
                                    // Get current offset values
                                    goodguy::Offset offset = hyundai_robot.getOffsetRight();

                                    // Print Current offset values
                                    std::cout << "[RIGHT] CURRENT offset values: \n" << offset << std::endl;

                                    std::cout << "choice joint [1,2]: ";
                                    char choice_joint = getInput<char>();

                                    switch(choice_joint){
                                        case '1':
                                            {
                                                std::cout << "Offset value (degree): " << offset.m_ang_0 << " -> " ;
                                                float ang = getInput<float>();
                                                offset.m_ang_0 = ang;
                                                break;
                                            }
                                        case '2':
                                            {
                                                std::cout << "Offset value (degree): " << offset.m_ang_1 << " -> " ;
                                                float ang = getInput<float>();
                                                offset.m_ang_1 = ang;
                                                break;
                                            }

                                        default: throw goodguy::WrongSelection(); break;
                                    }

                                    // Adjust Offset
                                    hyundai_robot.setOffsetRight(offset);
                                    std::cout << "Offset is applied" << std::endl;

                                    // Save Offset file
                                    std::ofstream offset_file("../offset_for_right.txt", std::ofstream::trunc);
                                    offset_file << offset;
                                    offset_file.close();
                                }
                                break;
                            case 'm':       // For Measuring Start Position
                                {
                                    // Get current offset values
                                    goodguy::MeasureOffset measure_offset = hyundai_robot.getMeasureOffset();

                                    // Print Current offset values
                                    std::cout << "[Measure] CURRENT meausre offset values: \n" << measure_offset << std::endl;

                                    std::cout << "choice measure [1,2,3,4,5,6]: ";
                                    char choice_measure = getInput<char>();

                                    switch(choice_measure){
                                        case '1':
                                            {
                                                std::cout << "Offset value (degree): " << measure_offset.m_measure_ang_0 << " -> " ;
                                                float ang = getInput<float>();
                                                measure_offset.m_measure_ang_0 = ang;
                                                break;
                                            }
                                        case '2':
                                            {
                                                std::cout << "Offset value (degree): " << measure_offset.m_measure_ang_1 << " -> " ;
                                                float ang = getInput<float>();
                                                measure_offset.m_measure_ang_1 = ang;
                                                break;
                                            }
                                        case '3':
                                            {
                                                std::cout << "Offset value (degree): " << measure_offset.m_measure_ang_2 << " -> " ;
                                                float ang = getInput<float>();
                                                measure_offset.m_measure_ang_2 = ang;
                                                break;
                                            }
                                        case '4':
                                            {
                                                std::cout << "Offset value (degree): " << measure_offset.m_measure_ang_3 << " -> " ;
                                                float ang = getInput<float>();
                                                measure_offset.m_measure_ang_3 = ang;
                                                break;
                                            }
                                        case '5':
                                            {
                                                std::cout << "Offset value (degree): " << measure_offset.m_measure_ang_4 << " -> " ;
                                                float ang = getInput<float>();
                                                measure_offset.m_measure_ang_4 = ang;
                                                break;
                                            }
                                        case '6':
                                            {
                                                std::cout << "Offset value (degree): " << measure_offset.m_measure_ang_5 << " -> " ;
                                                float ang = getInput<float>();
                                                measure_offset.m_measure_ang_5 = ang;
                                                break;
                                            }

                                        default: throw goodguy::WrongSelection(); break;
                                    }

                                    // Adjust Offset
                                    hyundai_robot.setMeasureOffset(measure_offset);
                                    std::cout << "Offset is applied" << std::endl;

                                    // Save Offset file
                                    std::ofstream offset_file("../measure_offset.txt", std::ofstream::trunc);
                                    offset_file << measure_offset;
                                    offset_file.close();
                                }

                                break;
                        }
                    } catch(std::exception& e){
                        std::cout << "INVALID INPUT" << std::endl;
                    } catch(goodguy::Offset::SetFailException& e){
                        std::cout << "[Failed] offset is not applied (SCAN procedure must be STOP!) " << std::endl;
                    } catch(goodguy::MeasureOffset::SetFailException& e){
                        std::cout << "[Failed] offset is not applied (SCAN procedure must be STOP!) " << std::endl;

                    } catch(goodguy::WrongSelection& e){
                        std::cout << "wrong selection" << std::endl;
                    }
                    break;
                }

            case 's':   
                {
                    // Start Laser Sensors
                    std::cout << "Sensors are started ... " << std::endl;
                    hyundai_robot.startSensors();
                    break;
                }
            case 'S':   
                {
                    // Stop Laser Sensors
                    std::cout << "Sensors are stoped ... " << std::endl;
                    hyundai_robot.stopSensors();
                    break;
                }

            case 'q':   
                {
                    // Quit This Program
                    hyundai_robot.stop();
                    hyundai_robot.stopSensors();
                    io1.stop();
                    is_stop = true;
                    break;
                }

            case '1':
                {
                    // Start Data Acquisition
                    hyundai_robot.startSensors();
                    hyundai_robot.scan_start();
                    break;
                }
            case '2':
                {
                    // Stop Data Acquisition
                    hyundai_robot.scan_stop();
                    hyundai_robot.stopSensors();
                    break;
                }

        }



    }

    std::cout << "Program EXIT [wait thread...]" << std::endl;
    thread_for_hyundai_robot.join();
    thread_for_io1.join();

    std::cout << "Program EXIT" << std::endl;


    return 0;
}

