#include "packet.hpp"
#include "controller.hpp"


#include <boost/asio.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/timer/timer.hpp>
#include <boost/thread/thread.hpp>

#include <iostream>
#include <string>
#include <vector>


namespace goodguy{

    template<class SOCKET>
        // Lower controller using controller class
        class LowerController : public goodguy::Controller<SOCKET>{

            public:
                // Constructor
                LowerController(SOCKET& socket, int control_time, goodguy::SID sid)
                    : Controller<SOCKET>(socket, control_time, sid), m_is_run(true)
                { }


                // main control thread
                void run(void){

                    while(m_is_run){

                        // parsing received packet stream
                        this->parsePacket();

                        // wait control time
                        this->waitControlTime();
                    }

                }

                // stop main control thread
                void stop(void){
                    m_is_run = false;
                }



                // set up-down velocity to target velocity
                void setVelocity(float velocity){
                    // make packet for command to adjust target up-down velocity
                    PacketHostToSlaveVel packet(this->m_sid, velocity);

                    // generate packet stream
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();

                    // send packet stream to control board
                    this->write(packet_stream);
                }

            private:
                void routineForEchoTest(const std::shared_ptr<PacketEchoTest<std::vector<unsigned char>>> packet){
                    std::cout << "REPLY: " << packet->getString<std::string>() << std::endl;
                }

            protected:

            private: 

                bool m_is_run;

        };

}


// get keyboard input in various template
template <typename T>
T getInput()
{
    T result;
    // get keyboard input using standard I/O
    std::cin >> result;
    if (std::cin.fail() || std::cin.get() != '\n'){
        std::cin.clear();
        while (std::cin.get() != '\n');
        throw std::ios_base::failure("Invalid input.");
    }
    return result;
}


int main(){
    boost::asio::io_service io;


#ifdef SIMULATION_MODE      // To use this code in webots simulation
    // define control time
    const int control_time_for_lower_body = 10*1000*1000;

    using boost::asio::ip::tcp;

    // connect to bottom controller in webots simulator
    tcp::resolver resolver(io);
    tcp::resolver::query query(tcp::v4(), "127.0.0.1", "3111");
    tcp::resolver::iterator iterator = resolver.resolve(query);

    tcp::socket socket(io);
    try{
        // request connection to webots
        boost::asio::connect(socket, iterator);
    }catch(std::exception& e){
        std::cerr << "Exception: " << e.what() << std::endl;
        std::cout << "Connection failed" << std::endl;
        return 0;
    }

    std::cout << "Connection Success" << std::endl;

    // create lower controller object operating in webots simulator
    goodguy::LowerController<tcp::socket> lower_body(socket, control_time_for_lower_body, goodguy::SID::LOWER);
    // make control thread
    boost::thread thread_for_lower_body(boost::bind(&goodguy::LowerController<tcp::socket>::run, &lower_body));

#else                       // To use this code in real robot 
    // define control time
    const int control_time_for_lower_body = 5*1000*1000; 

    // connect to bottom controller in real robot 
    boost::asio::serial_port serial(io);
    serial.open("/dev/ttyBRD_B");

    // setting UART configuration
    serial.set_option(boost::asio::serial_port::baud_rate(1000000UL));
    serial.set_option(boost::asio::serial_port::flow_control());
    serial.set_option(boost::asio::serial_port::stop_bits());
    serial.set_option(boost::asio::serial_port::character_size());
    serial.set_option(boost::asio::serial_port::parity());

    // create lower controller object operating in real robot 
    goodguy::LowerController<boost::asio::serial_port> lower_body(serial, control_time_for_lower_body, goodguy::SID::LOWER);
    // make control thread
    boost::thread thread_for_lower_body(boost::bind(&goodguy::LowerController<boost::asio::serial_port>::run, &lower_body));
#endif

    // start async read operation using boost.asio
    lower_body.start_read();

    // make asio thread
    boost::thread thread_for_io(boost::bind(&boost::asio::io_service::run, &io));


    bool is_stop = false;

    // text-based user interface
    while(!is_stop){

        /*****************************
        * q: quit 
        * e: send echo message 
        * s: setting up-down velocity 
        ******************************/

        std::cout << "Controller Program for Lower Body" << std::endl;

        unsigned char input = 0;
        try{
            // get user's command 
            input = getInput<unsigned char>();
        } catch(std::exception& e){
            input = 0x00;
        }

        switch(input){
            case 'q':   
                {
                    // stop up-down motion 
                    lower_body.stop();
                    io.stop();
                    // and quit
                    is_stop = true;
                    break;
                }
            case 'e':
                {
                    // send echo message to lower controller
                    std::cout << "Echo Message: " ;
                    std::string echo_str = getInput<std::string>();
                    lower_body.sendEcho(echo_str);
                    break;
                }
            case 's':
                {
                    // set up-down velocity
                    std::cout << "Set Velocity: " ;
                    try{
                        // get up-down velocity
                        float velocity = getInput<float>();
                        // set velocity
                        lower_body.setVelocity(velocity);
                    } catch(std::exception& e){
                        std::cout << "Invalid Input" << std::endl;

                    }
                    break;
                }

        }



    }

    thread_for_lower_body.join();
    thread_for_io.join();
    std::cout << "Program EXIT" << std::endl;
}

