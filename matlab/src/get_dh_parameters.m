function dh_parameters = get_dh_parameters(pitch, roll, height, theta_1, theta_2, depth, measure)

if measure==1 || measure==3 || measure==5
    dh_parameters = [
        0	-266.2	0	0;
        0	0	40	-90;
        90	40	0	0;
        -pitch	0	0	90;
        roll	0	0	-90;
        -90	-40	0	0;
        0	0	-40	90;
        0	266.2	0	0;
        -90	height	0	0;
        0	0	-277	0;
        0	67.4	0	-90;
        0	125.5	0	90;
        theta_1	42.5	0	90;
        90+theta_2	27.7	0	0;
        0	0	33	0;
        0	-33	0	-90;
        0	49.83	0	0;
        0	0	28	90;
        0  9.5	0 -90;
        0	38.5+depth	0	0];
    
else
    dh_parameters = [
        0	-266.2	0	0;
        0	0	40	-90;
        90	40	0	0;
        -pitch	0	0	90;
        roll	0	0	-90;
        -90	-40	0	0;
        0	0	-40	90;
        0	266.2	0	0;
        0	height	277	-90;
        0	125.5	0	90;
        theta_1	67.4	0	-90;
        0	-27.7	0	90;
        0	42.5	0	-90;
        theta_2 33	0	90;
        0	33	0	0;
        0	0	49.83	0;
        90	28	0	0;
        0	0	9.5	90;
        0	38.5+depth	0	0];
end


if measure>=5
    %dh_parameters = [0  0 12225 0;
    dh_parameters = [0  0 12000 0;
        90 0 0 0;
        dh_parameters];
end

if measure>=3
    %dh_parameters = [0 0 6000 0;    
    %dh_parameters = [0 0 2460 0;
    dh_parameters = [0 0 2400 0;
        90 0 0 0;
        dh_parameters];
end


end

