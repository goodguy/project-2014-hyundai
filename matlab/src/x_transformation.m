function X = x_transformation(alpha, a)
X = eye(4,4);

X(2,2) = cos(alpha);
X(2,3) = -sin(alpha);
X(3,2) = sin(alpha);
X(3,3) = cos(alpha);
X(1,4) = a;

end
