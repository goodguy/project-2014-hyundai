function [corner, indexInlier] = adaptive_ransac(points, maxIter, sigmaB, sigmaS)
% TODO 2D line --> 3D line
z = mean(points(3,:));
points(3,:) = 1;

CONFIDENCE = 0.99;
SIGMA_FACTOR = 2.56;
NUM_DATA = size(points,2); % points: 3xNUM_DATA matrix

% 1. Find the major line
numIter = maxIter;
bestScore = 0;
bestLine1 = [];
bestInlier = [];
iter = 0;
while iter < numIter
    iter = iter + 1;

    % 1.1. Generate a model
    % - Model: [a, b, c] (ax + by + c = 0 and a^2 + b^2 = 1)
    s = randi([1, NUM_DATA], 2, 1);
    while s(2) == s(1)
        s(2) = randi([1, NUM_DATA]);
    end
    model = line_lsm(points(:,s)');

    % 1.2. Evaluate the model
    dist = model * points;
    score = sum(exp(-dist.^2 / sigmaB^2));
    if score > bestScore
        bestScore = score;
        bestLine1 = model;
        bestInlier = abs(dist) < (SIGMA_FACTOR * sigmaB);
        gamma = sum(bestInlier) / NUM_DATA; % Calculate the inlier ratio
        numIter = min([log(1 - CONFIDENCE) / log(1 - gamma^2), maxIter]); % Terminate adaptively
%disp(['Step1 numIter = ', num2str(numIter)])
    end
end
bestLine1 = line_lsm(points(:, bestInlier)'); % Refine the best model

index = 1:NUM_DATA;
indexInlier = index(bestInlier == 1);
indexOutlier = index(bestInlier == 0);
numOutlier = length(indexOutlier);

% 2. Extract the corner
numIter = maxIter;
bestScore = 0;
bestLine2 = [];
iter = 0;
while iter < numIter
    iter = iter + 1;

    % 1.1. Generate a model
    % - Model: [-b, a, d] ('a' and 'b' are from 'bestLine1'.)
    s = randi([1, numOutlier]);
    model = [-bestLine1(2), bestLine1(1), ...
              bestLine1(2) * points(1,indexOutlier(s)) - bestLine1(1) * points(2,indexOutlier(s))];
    if model * points(:,indexInlier(1)) > 0
        model = -model;
    end

    % 1.2. Evaluate the model
    dist = model * points(:,indexOutlier);
    density = ones(1,numOutlier);
    density(dist > 0) = exp(-dist(dist > 0).^2 / sigmaB^2);
    density(dist < 0) = exp(-dist(dist < 0).^2 / sigmaS^2);
    score = sum(density);
    if score > bestScore
        bestScore = score;
        bestLine2 = model;
        bestInlier = abs(dist) < (SIGMA_FACTOR * sigmaS);
        gamma = sum(bestInlier) / numOutlier; % Calculate the inlier ratio
        numIter = min([log(1 - CONFIDENCE) / log(1 - gamma), maxIter]); % Terminate adaptively
%disp(['Step2 numIter = ', num2str(numIter)])
    end
end

corner = -pinv([bestLine1(1:2); bestLine2(1:2)]) * [bestLine1(3); bestLine2(3)];
corner = [corner', z];
indexInlier = indexInlier - 1; % To be compatible to the MEX file

%close all;
%line_plot([bestLine1; bestLine2], points');
%disp(corner);
%pause;
