function kinematics

clear all;
close all;

%518,25



data = load('../dataset/webots_1/measure_1_file.txt');
samples = size(data,1)

points = zeros(3,samples);

    roll = 0;
    pitch = -0;
for(k = 1:samples)

    
    theta_1 = data(k,5);
    theta_2 = data(k,6);
    depth = data(k,7)*1000;
    
    height = data(k,8)*1000 + 518.25;

    dh_parameters = [
    0	-266.2	0	0;
    0	0	40	-90;
    90	40	0	0;
    -pitch	0	0	90;
    roll	0	0	-90;
    -90	-40	0	0;
    0	0	-40	90;
    0	266.2	0	0;
    -90	height	0	0; 
    0	0	-277	0;
    0	67.4	0	-90;
    0	125.5	0	90;
    theta_1	42.5	0	90;
    90+theta_2	27.7	0	0;
    0	0	33	0;
    0	-33	0	-90;
    0	49.83	0	0;
    0	0	28	90;
    0  9.5	0 -90;
    0	38.5+depth	0	0];

    cols = size(dh_parameters,1);
    
    T = eye(4,4);
    for(a = 1:cols)
        T = T*z_transformation(dh_parameters(a,1)*pi/180,dh_parameters(a,2));
        T = T*x_transformation(dh_parameters(a,4)*pi/180,dh_parameters(a,3));    
    end
    
    points(:,k) = T(1:3,4);
end

figure(1)
scatter3(points(1,:)/1000, points(2,:)/1000, points(3,:)/1000);



hold on

data = load('../dataset/webots_1/measure_2_file.txt');

samples = size(data,1);

points = zeros(3,samples);

for(k = 1:samples)

    theta_1 = data(k,5);
    theta_2 = data(k,6);
    depth = data(k,7)*1000;
    height = data(k,8)*1000 + 518.25;

    dh_parameters = [
    0	-266.2	0	0;
    0	0	40	-90;
    90	40	0	0;
    -pitch	0	0	90;
    roll	0	0	-90;
    -90	-40	0	0;
    0	0	-40	90;
    0	266.2	0	0;    
    0	height	277	-90;
    0	125.5	0	90;
    theta_1	67.4	0	-90;
    0	-27.7	0	90;
    0	42.5	0	-90;
    theta_2 33	0	90;
    0	33	0	0;
    0	0	49.83	0;
    90	28	0	0;
    0	0	9.5	90;
    0	38.5+depth	0	0];

    cols = size(dh_parameters,1);
    
    T = eye(4,4);
    for(a = 1:cols)
        T = T*z_transformation(dh_parameters(a,1)*pi/180,dh_parameters(a,2));
        T = T*x_transformation(dh_parameters(a,4)*pi/180,dh_parameters(a,3));    
        
    end
    
    points(:,k) = T(1:3,4);
end


% figure(2)
scatter3(points(1,:)/1000, points(2,:)/1000, points(3,:)/1000);
%axis([4.0 4.2 0.05 0.25 ])
end




