%% Ground Truth for webots 7
% [beam_1_bot, beam_1_top, beam_2_bot, beam_2_top, beam_3_bot, beam_3_top, beam_4_bot, beam_4_top];
ground_truth = zeros(3,8);

beam_height = 18.0;
beam_offset = 0.0075;


beam_center_1 = 12;
beam_pitch_offset_rad_1 = 0.00;
beam_roll_offset_rad_1 = 0.03;
beam_x_bottom_1 = beam_center_1*tan(beam_pitch_offset_rad_1) + beam_offset;
beam_y_bottom_1 = beam_center_1*tan(beam_roll_offset_rad_1) + beam_offset;
beam_x_top_1 = -(beam_height-beam_center_1)*tan(beam_pitch_offset_rad_1) + beam_offset;
beam_y_top_1 = -(beam_height-beam_center_1)*tan(beam_roll_offset_rad_1) + beam_offset;

beam_center_4 = 12;
beam_pitch_offset_rad_4 = 0.00;
beam_roll_offset_rad_4 = 0.03;
beam_x_bottom_4 = beam_center_4*tan(beam_pitch_offset_rad_4) + beam_offset;
beam_y_bottom_4 = 12-(beam_center_4*tan(beam_roll_offset_rad_4) + beam_offset);
beam_x_top_4 = -(beam_height-beam_center_4)*tan(beam_pitch_offset_rad_4) + beam_offset;
beam_y_top_4 = 12-(-(beam_height-beam_center_4)*tan(beam_roll_offset_rad_4) + beam_offset);



ground_truth(:,1) = [beam_x_bottom_1; beam_y_bottom_1; 0];
ground_truth(:,2) = [beam_x_top_1; beam_y_top_1; beam_height];

ground_truth(:,3) = [2.4 - beam_offset; 0 + beam_offset; 0];
ground_truth(:,4) = [2.4 - beam_offset; 0 + beam_offset; beam_height];

ground_truth(:,5) = [2.4 - beam_offset; 12 - beam_offset; 0];
ground_truth(:,6) = [2.4 - beam_offset; 12 - beam_offset; beam_height];

ground_truth(:,7) = [beam_x_bottom_4; beam_y_bottom_4; 0];
ground_truth(:,8) = [beam_x_top_4; beam_y_top_4; beam_height];

for k = 1:4
   X = [ground_truth(1,2*k-1:2*k)]; 
   Y = [ground_truth(2,2*k-1:2*k)]; 
   Z = [ground_truth(3,2*k-1:2*k)]; 
   line(X, Y, Z, 'linewidth', 3, 'color', [0.7 0.7 0.7])

end
