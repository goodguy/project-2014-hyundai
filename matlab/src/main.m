clear all;
close all;


tic

dataDir = '../dataset/webots_case_1_0';
%dataDir = '../dataset/hyundai_2_2';
%dataDir = '/home/goodguy/project/hyundai/matlab/test_2014_10_28';


% myCluster = parcluster('local');
% myCluster.NumWorkers = 8;  % 'Modified' property now TRUE
% saveProfile(myCluster);    % 'local' profile now updated,
%                            % 'Modified' property now FALSE    

% p = gcp;
% delete(p);
% parpool(6)

fileInfo = dir(fullfile(dataDir, '*.txt'));

roll = 0;
pitch = -0;

filtered_max_points =[  1.30 13.30; 
                        3.6 1.15;
                        1.30 13.30;
                        3.6 13.35;
                        1.30 1.15;
                        1.30 13.30;
                      ]*1000;
filtered_min_points =[  -1.8 8.70;
                        1.20 -1.30;    
                        -1.3 8.70;
                        1.20 8.70;
                        -1.8 -2.15;
                        -1.8 8.70;
                      ]*1000;



measurementID = length(fileInfo);

corners_pose = cell(1,measurementID);

skip_scan = [-1 -1 ];


%offset_angle = [ 0 +0 +1.2 +0.5 +0.5 -0. ];
offset_angle = zeros(6,1);

measrement_rot_z = [-90, 90, -90, 180, 0, -90]*pi/180;



%% Corner detection
for ii = 1:measurementID
    close all
    
    data = load(fullfile(dataDir,fileInfo(ii).name));
    
    sid = unique(data(:,2));
    ssid = unique(data(:,3));
    sscanID = length(ssid);
    scanID=length(sid);
    
    beam_corners = zeros(3,scanID,sscanID);
    disp(['[START] Corner detection for Measurement #' int2str(ii) ]);
    
    scan_num_idx = 1;
    

    
    for jj = 1:scanID
        scan_idx = data(:,2)==sid(jj);
        scan_data = data(scan_idx,:);
        ssid = unique(scan_data(:,3));
        sscanID = length(ssid);
        
        sscan_num_idx = 1;

        for ss = 1:sscanID
            disp(['[START] Corner detection for Measurement #' int2str(ii) ' - ' int2str(jj) ' - ' int2str(ss)]);        
            idx = scan_data(:,3)==ssid(ss);
            sample=sum(idx); 
            data_ssid = scan_data(idx,:);
            points = zeros(3,sample);
            [theta_1, theta_2, depth, height] = get_estimated_value(data_ssid);
            
            for kk = 1:sample;
                %DH = get_dh_parameters(pitch, roll, height(kk), theta_1(kk), theta_2(kk), depth(kk), ii);
                DH = get_dh_parameters(pitch, roll, height(kk), theta_1(kk) + offset_angle(ii), 0, depth(kk), ii);
                T = eye(4,4);
                for a = 1:size(DH,1)
                    T = T*z_transformation(DH(a,1)*pi/180,DH(a,2));
                    T = T*x_transformation(DH(a,4)*pi/180,DH(a,3));
                end

                points(:,kk) = T(1:3,4);            
            end

            %% Filtering 

            points = points(:, (points(1,:)<filtered_max_points(ii,1))&(points(2,:)<filtered_max_points(ii,2))&(points(1,:)>filtered_min_points(ii,1))&(points(2,:)>filtered_min_points(ii,2)));
            temp = points'/1000;
            temp = temp'*1000;
            scatter3(temp(1,:)/1000, temp(2,:)/1000, temp(3,:)/1000,20, 'r');   
            axis equal 

            hold on
            %[corner, x1, x2, idx_line1] = mex_cornerEstimate_projection(points'/1000, 10000, 0.01, 0.08, 0.0001);  
            [corner, idx_line1] = adaptive_ransac(points/1000, 10000, 0.010, 0.0001);  
            idx_line1=idx_line1+1; 
            
            scatter3(temp(1,idx_line1)/1000, temp(2,idx_line1)/1000, temp(3,idx_line1)/1000,20, 'k');
            scatter3(corner(1), corner(2), corner(3),100,'b','filled');
            axis equal 



           beam_corners(:,scan_num_idx,sscan_num_idx) = corner';   
           sscan_num_idx = sscan_num_idx + 1;


           disp(['[FINISH] Corner detection for Measurement #' int2str(ii) ' - ' int2str(jj) ' - ' int2str(ss)]);         
        end
        scan_num_idx = scan_num_idx + 1;
        
        
       
    end
    
    real_corners = beam_corners(:,:,1);
    for jj = 1:scanID
        for ss = 1:sscanID
            if norm(real_corners(:,jj)) > norm(beam_corners(:,jj,ss))
               real_corners(:,jj) = beam_corners(:,jj,ss);
            end
        end
    end
    
    
    scatter3(real_corners(1,:), real_corners(2,:), real_corners(3,:),200,'g','filled');

    
    %corners_pose{ii} = beam_corners(:,:,1);
    
    corners_pose{ii} = zeros(4,4*length(real_corners));
    for k = 1:length(real_corners)
        corners_pose{ii}(:,4*(k-1)+1:4*k) = eye(4,4);
        corners_pose{ii}(1:2,4*(k-1)+1:4*k-2) = [cos(measrement_rot_z(ii)) -sin(measrement_rot_z(ii)); sin(measrement_rot_z(ii)) cos(measrement_rot_z(ii))];
        corners_pose{ii}(1:3,4*k) = real_corners(:,k);
    end
        
    
    disp(['[FINISH] Corner detection for Measurement #' int2str(ii)  ]);
end



min_num = min([length(corners_pose{1}) length(corners_pose{2}) length(corners_pose{3}) ...
                length(corners_pose{4}) length(corners_pose{5}) length(corners_pose{6}) ]);
for k = 1:6
    corners_pose{k} = corners_pose{k}(:,1:min_num);
end
            

data = load(fullfile(dataDir,fileInfo(1).name));    
sid = unique(data(:,2));

scan_num = min_num/4;




save('corners.mat');



toc

post_processing;










