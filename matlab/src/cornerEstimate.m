function [finalXc, x1, x2] = cornerEstimate(data, maxIter, ec, ea, epsilon)
% -- description --
% RANSAC ������� corner�� 3D point�� ã�� �˰?��.

% -- input --
% 1. data: N(# of data) by m(dim.)
% 2. max_iter: max_iteration
% 3. ec: error classifying INLIER and OUTLIER
% 4. ea: error of angle. �� line�� 90���� �Ǿ�� �ϹǷ�, abs(angle-90)<ea�� �ǵ��� ����.
% 4. epsilon: termination critera

% -- output --
% 1. finalXc: �������� �ڳ��� 3D point.
% written by Yong-Ho Yoo @KAIST. Jul. 2014

[numData, numDim] = size(data);
finalXc = zeros(1,3);
emin = 0.1;

for ii = 1:maxIter
    
    while true
        
        while true
            A = data(randperm(numData,2),:);
            p1 = A(1,:);
            p2 = A(2,:);
            
            cross0 = cross((data-repmat(p1,numData,1)),(data-repmat(p2,numData,1)));
            dist0 = sqrt(sum(cross0.^2,2))/norm(p1-p2);
            
            idx_in_line = find(dist0<0.02);
            idx_out_line = find(dist0>=0.02);
            if numel(idx_in_line)>=0.5*numData && numel(idx_out_line)>=0.1*numData
                break; 
            end
            
        end
        %% Random sampling
        
        si = randperm(numel(idx_in_line), 2);
        
        sj = randperm(numel(idx_out_line), 1);
        
        xc = data(idx_in_line(si(1)), :);
        x1 = data(idx_in_line(si(2)), :);
        x2 = data(idx_out_line(sj(1)),:);
        
        cosAngle=((xc-x1)*(xc-x2)')/(norm(xc-x1)*norm(xc-x2));
        
        if abs((pi/2-acos(cosAngle)))<ea; break; end
    end
    
    
    
    %% Verifying Model Parameters
    cross1 = cross((data-repmat(xc,numData,1)),(data-repmat(x1,numData,1)));
    cross2 = cross((data-repmat(xc,numData,1)),(data-repmat(x2,numData,1)));
    dist1 = sqrt(sum(cross1.^2,2))/norm(xc-x1);
    dist2 = sqrt(sum(cross2.^2,2))/norm(xc-x1);
    
    
    dist = min(dist1, dist2);
    inLier = data(dist<ec,:);
    
    error = mean(dist(dist<ec));
    %     if ii==1; emin=error; continue; end
    
    % inLier�� ������ �ʹ� ���� �� ����.
    if size(inLier, 1)<numData*0.1
        continue;
    end
    
    %% termination condition
    
    if error<emin
        emin = error;
        finalXc = xc;        
        if emin<epsilon; break; end
    end
end

if sum(finalXc==0)==3;finalXc=xc;end

% ii
% emin

end