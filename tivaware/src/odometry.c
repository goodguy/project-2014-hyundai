#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/pwm.h"
#include "driverlib/qei.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "debug.h"


// Define Control frequency
#define CONTROL_FREQ (1000.0)

// Define PWM frequency
//#define PWM_FREQ (100*1000)

// Define resolution of motor's encoder
#define RESOLUTION 500 
// Define gear-ratio of motor
#define GEAR_RATIO 1 
#define BUFFER_LENGTH 256

/***************************
 * GPIO setting
 *  - PB4 : PWM1A
 *  - PB5 : PWM2A
 *  - PE4 : PWM1B
 *  - PE5 : PWM2B
 *  - PD6 : PHA0
 *  - PF1 : PHB0
 *  - PC5 : PHA1
 *  - PC6 : PHB1
 *  - PD2 : Photo emission1
 *  - PD3 : Photo emission2
 *  - PE1 : Photo interrupt1
 *  - PE2 : Photo interrupt2
 *
 *************************/

char uart_recv_buffer[BUFFER_LENGTH]; 
volatile uint8_t uart_recv_size = 0;

char run_packet_buffer[BUFFER_LENGTH]; 
uint8_t run_packet_idx = 0;

// Get Direction using QEI Module 
int32_t getDirection(unsigned int selected_qei_base);
// Get Velocity using QEI Module 
float getVelocity(unsigned int selected_qei_base);
// Get Angle using QEI Module 
float getAngle(unsigned int selected_qei_base);

// Main Control Function
void run(void);

// UART Interrupt Handler to communicate with Odroid Board
void UARTIntHandler(void);

// Timer Handler to control motors
void Timer0IntHandler(void);

// Configuration of Timer Module
void TIMER0Configure();
// Configuration of UART Module
void UARTConfigure();
// Configuration of LED Module
void LEDConfigure();
// Configuration of PWM Module
//void PWMConfigure();
// Configuration of QEI Module
void QEIModuleConfigure();
// Configuration of Photo-Interrupt Module
void PhotoIntConfigure();


// Get direction from QEI module
int32_t getDirection(unsigned int selected_qei_base){
    int32_t dir = QEIDirectionGet(selected_qei_base);
    return dir;
}

// Get velocity from QEI module
float getVelocity(unsigned int selected_qei_base){
    float clock = SysCtlClockGet();
    float veldiv = 16;
    float speed = (float)QEIVelocityGet(selected_qei_base);

    float load = SysCtlClockGet()/4;
    float ppr = RESOLUTION*GEAR_RATIO;
    float edges = 4;

    int32_t dir = getDirection(selected_qei_base);

    
    float rpm = (clock * veldiv * speed * 60) * dir/(load * ppr * edges);


    return rpm;
}

// Get angle from QEI module
float getAngle(unsigned int selected_qei_base){
    uint32_t val = QEIPositionGet(selected_qei_base);
    int32_t initial_angle = 0x7fffffff;
    uint32_t max_angle = RESOLUTION*4*GEAR_RATIO;
    
    float angle; 
    angle = ((int)(val-initial_angle))*360/(float)max_angle;
    //if (angle>180) angle-=360; 
    return angle;
}



// Main Control Function
void run(void){

    // stacking received char array 
    for(uint8_t i = 0; i < uart_recv_size; ++i){
        run_packet_buffer[run_packet_idx+i] = uart_recv_buffer[i];
    }
    run_packet_idx += uart_recv_size;
    uart_recv_size = 0;

    // parsing packet from received char array
    //parsingPacket(run_packet_buffer, &run_packet_idx);

    // get motor velocity and angle for motor 0
    //float rpm0 = getVelocity(QEI0_BASE);
    //float rpm1 = getVelocity(QEI1_BASE);

    //float vel0 = rpm0 * 360.0 / 60.0;
    //float vel1 = rpm1 * 360.0 / 60.0;
    //

    // 상수자리: 6자리, 소수자리: 2자리, 부호: 1자리

    

    float ang0 = getAngle(QEI0_BASE) *100;
    float ang1 = getAngle(QEI1_BASE) *100;

    UARTprintf("%d, %d\n\r", (int)ang0, (int)ang1);

}

int main(void){

    // enable Floating point unit
    FPUEnable();
    FPULazyStackingEnable();

    // set board clock to 80 MHz
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    // configuration of LED
    LEDConfigure();

    // configuration of UART to communicate with Odroid board
    UARTConfigure();

    UARTprintf("START MOTOR PWM PROGRAM\n");

    // configuration to measure motor status (Pos, Vel)
    QEIModuleConfigure();
    
    // configuration for PWM module to operate motor
    //PWMConfigure();

    // configuration to control with precise frequency
    TIMER0Configure();

    // configuration of Photo-interrupt GPIO 
    PhotoIntConfigure();

    IntMasterEnable();

    while(1){

    }
}


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//  HW DEPENDENT PART //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

// GPIO Interrupt handler for Photo-interrupt sensor
void GPIOIntHandler(void){
    IntMasterDisable();
    IntMasterEnable();
}


// UART Interrupt handler to communicate with Odoroid board
void UARTIntHandler(void){

    uint32_t ui32Status;
    // Get the interrrupt status.
    ui32Status = UARTIntStatus(UART0_BASE, true);

    // Clear the asserted interrupts.
    UARTIntClear(UART0_BASE, ui32Status);
    IntMasterDisable();
    // Loop while there are characters in the receive FIFO.

    while(UARTCharsAvail(UART0_BASE)){
        unsigned char input = UARTCharGetNonBlocking(UART0_BASE);

        if(uart_recv_size < BUFFER_LENGTH){
            uart_recv_buffer[uart_recv_size] = input;
            uart_recv_size = uart_recv_size + 1;
        }
    }

    IntMasterEnable();
}

// Timer interrupt handler to control motor
void Timer0IntHandler(void){

    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    IntMasterDisable();
    // execute run() control function
    run();
    IntMasterEnable();

}

// Configuration for timer 0 to control motor
void TIMER0Configure(){

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet()/CONTROL_FREQ);

    IntEnable(INT_TIMER0A);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    TimerEnable(TIMER0_BASE, TIMER_A);

}

// Configuration for UART to commnunicate with Odroid board
void UARTConfigure(){
    // Enable the peripherals used by this example.
    // GPIO PA[1:0]'s default state is UART0. 
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Set GPIO A0 and A1 as UART pins.
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Use the internal 16MHz oscillator as the UART clock source.
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    // Initialize the UART for console I/O.
    UARTStdioConfig(0, 115200, 16000000);
    //UARTStdioConfig(0, 1000000, 16000000);
    // Set baudrate for seral communication.

    // Enable the UART interrupt.
    IntEnable(INT_UART0);
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);

}

void LEDConfigure(){

    // Enable the GPIO port that is used for the on-board LED.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Enable the GPIO pins for the LED (PF2).
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);

}

// Configuration for PWM to operate motors 
/*
void PWMConfigure(){
    uint32_t period = SysCtlClockGet()/(PWM_FREQ);

    // We will use GPIO E,B as PWM0. 
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    GPIOPinConfigure(GPIO_PB4_M0PWM2);
    GPIOPinConfigure(GPIO_PB5_M0PWM3);
    GPIOPinConfigure(GPIO_PE4_M0PWM4);
    GPIOPinConfigure(GPIO_PE5_M0PWM5);

    GPIOPinTypePWM(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);
    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_4 | GPIO_PIN_5);

    PWMClockSet(PWM0_BASE, PWM_SYSCLK_DIV_1);
    // PWM Count-Down Mode 
    PWMGenConfigure(PWM0_BASE,PWM_GEN_1,PWM_GEN_MODE_DOWN|PWM_GEN_MODE_GEN_SYNC_GLOBAL);
    PWMGenConfigure(PWM0_BASE,PWM_GEN_2,PWM_GEN_MODE_DOWN|PWM_GEN_MODE_GEN_SYNC_GLOBAL);

    // PWM_GEN_1 -> PWM_OUT_2,3
    // PWM_GEN_2 -> PWM_OUT_4,5
    PWMGenPeriodSet(PWM0_BASE,PWM_GEN_1, period);
    PWMGenPeriodSet(PWM0_BASE,PWM_GEN_2, period);

    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_2, 1);
    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_3, 1);

    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_4, 1);
    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_5, 1);

    PWMGenEnable(PWM0_BASE,PWM_GEN_1);
    PWMGenEnable(PWM0_BASE,PWM_GEN_2);

    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_2,PWM_OUTPUT_MODE_SYNC_GLOBAL);
    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_3,PWM_OUTPUT_MODE_SYNC_GLOBAL);
    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_4,PWM_OUTPUT_MODE_SYNC_GLOBAL);
    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_5,PWM_OUTPUT_MODE_SYNC_GLOBAL);

    PWMSyncUpdate(PWM0_BASE,PWM_GEN_1_BIT);
    PWMSyncUpdate(PWM0_BASE,PWM_GEN_2_BIT);

    PWMOutputState(PWM0_BASE,PWM_OUT_2_BIT,true);
    PWMOutputState(PWM0_BASE,PWM_OUT_3_BIT,true);
    PWMOutputState(PWM0_BASE,PWM_OUT_4_BIT,true);
    PWMOutputState(PWM0_BASE,PWM_OUT_5_BIT,true);

}
*/

// Configuration for PWM to operate motors 
void QEIModuleConfigure(){
    // Related to Encoder.
    // Detect angle and speed.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI1);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    GPIOPinConfigure(GPIO_PC5_PHA1);
    GPIOPinConfigure(GPIO_PC6_PHB1);
    GPIOPinConfigure(GPIO_PC4_IDX1);

    GPIOPinConfigure(GPIO_PD6_PHA0);
    //GPIOPinConfigure(GPIO_PD7_PHB0);    // NOT WORKING!!
    GPIOPinConfigure(GPIO_PF1_PHB0);
    GPIOPinConfigure(GPIO_PF4_IDX0);
    
    GPIOPinTypeQEI(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6);
    GPIOPinTypeQEI(GPIO_PORTD_BASE, GPIO_PIN_6 );
    GPIOPinTypeQEI(GPIO_PORTF_BASE, GPIO_PIN_4 | GPIO_PIN_1);

    uint32_t initial_angle = 0x7fffffff;
    uint32_t max_angle = 0xffffffff; //RESOLUTION*4*GEAR_RATIO;// max value counted in encoder. 
    //uint32_t vel_period = SysCtlClockGet()/(4);

    // QEI_CONFIG_CAPTURE_A_B: Count on chA and chB edges
    // QEI_CONFIG_NO_RESET: Do not reset on index pulse
    // QEI_CONFIG_QUADRATURE: chA and chB are quadratures
    // QEI_CONFIG_NO_SWAP: Do not swap chA and chB
    QEIConfigure(QEI0_BASE, QEI_CONFIG_CAPTURE_A_B|QEI_CONFIG_NO_RESET|QEI_CONFIG_QUADRATURE|QEI_CONFIG_NO_SWAP, max_angle);
    QEIConfigure(QEI1_BASE, QEI_CONFIG_CAPTURE_A_B|QEI_CONFIG_NO_RESET|QEI_CONFIG_QUADRATURE|QEI_CONFIG_NO_SWAP, max_angle);
    //QEIVelocityConfigure(QEI0_BASE, QEI_VELDIV_16, vel_period);
    //QEIVelocityConfigure(QEI1_BASE, QEI_VELDIV_16, vel_period);

    //QEIVelocityEnable(QEI0_BASE);
    //QEIVelocityEnable(QEI1_BASE);

    QEIPositionSet(QEI0_BASE,initial_angle);
    QEIPositionSet(QEI1_BASE,initial_angle);

    QEIEnable(QEI0_BASE);
    QEIEnable(QEI1_BASE);
}

// Configuration for Photo-interrupt sensors to read its' values
void PhotoIntConfigure(void){

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2);
    GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_2 | GPIO_PIN_3);

    GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2, GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD_WPU);
    GPIOPadConfigSet(GPIO_PORTD_BASE, GPIO_PIN_2 | GPIO_PIN_3, GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD_WPU);


    GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2, GPIO_RISING_EDGE);
    GPIOIntRegister(GPIO_PORTE_BASE, GPIOIntHandler);

    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2|GPIO_PIN_3,  GPIO_PIN_2|GPIO_PIN_3);

    GPIOIntEnable(GPIO_PORTE_BASE, GPIO_INT_PIN_1 | GPIO_INT_PIN_2);

    IntEnable(INT_GPIOE);

}
