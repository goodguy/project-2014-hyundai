#include "packet.h"


/***********************************
 * Get Packet Function from Stream
 * Argument:
 *    Input  - packet
 *    Return - FALSE (If packet is not valid)
 *           - TRUE  (If packet is valid)
 *
 * ***********************************/
bool isValid(const packet* pk){
    uint8_t data_size = pk->length - 8;
    uint8_t sum = 0; 

    for(int i = 0; i < 4; ++i)
        sum += pk->header[i];
    sum += pk->length;
    sum += pk->sid;
    sum += pk->type;
    for(int i = 0; i < data_size; ++i)
        sum += pk->data[i];
    sum += pk->checksum;

    if(sum == 0)
        return true;
    else
        return false;
}

/***********************************
 * Get Packet Function from Stream
 * Argument:
 *    Input  - stream
 *           - stream_size
 *    Return - Not NULL (Packet)
 *           - NULL (If size is not equal)
 *
 * !!CAUSION!! - packet and packet.data must be freed
 *
 * ***********************************/
bool getPacket(const char* stream, uint8_t size, packet* packet_temp){
    if(size >= 8){ // 8 = header(4) + length(1) + sID(1) + type(1) + checksum(1)
        uint8_t length =  stream[4];
        if(length == size){

            uint8_t data_size = length - 8;

            for(uint8_t i = 0; i < 4; ++i)
                packet_temp->header[i] = stream[i];
            packet_temp->length = length;
            packet_temp->sid = stream[5];
            packet_temp->type = stream[6];
            for(uint8_t i = 0; i < data_size; ++i)
                packet_temp->data[i] = stream[i+7];
            packet_temp->checksum = stream[length-1];
            
            return true;

        }

    }

    return false;
}

/***********************************
 * Split Packet Function
 * Argument:
 *    Input  - stream
 *           - header
 *           - stream_size
 *           - header_size
 *    Output - result_size 
 *    Return - Not NULL (Packet start location)
 *           - NULL (If packet is not found)
 *
 * ***********************************/

const char* splitPacket(const char* stream, const char* header, uint8_t stream_size, uint8_t header_size, uint8_t* result_size){

    static const char* origin_stream;
    static uint8_t previous_idx = 0;
    static uint8_t next_pos = 0;
    static uint8_t origin_stream_size = 0;

    if(stream != NULL){
        origin_stream = stream;
        origin_stream_size = stream_size;
        previous_idx = 0;
        next_pos = 0;
    }

    if(previous_idx >= origin_stream_size) return NULL;

    for(uint8_t i = 0; i < origin_stream_size-previous_idx; ++i){
        for(uint8_t j = 0; j < header_size; ++j){
            if((i+j+previous_idx) < origin_stream_size){
                if(header[j] != origin_stream[i+j+previous_idx])    break;
                if((j == header_size-1)){
                    uint8_t result_pos = next_pos;
                    *(result_size) = i+previous_idx - next_pos;
                    next_pos += *result_size;
                    previous_idx = previous_idx + i + 1;
                    return &origin_stream[result_pos];
                }
            }
        }
    }


    uint8_t result_pos = next_pos;
    *(result_size) = origin_stream_size - next_pos;
    previous_idx = origin_stream_size;

    return &origin_stream[result_pos];
}


/***********************************
 * Packet generator from Packet structure
 * Argument:
 *    Input  - packet      
 *    Output - stream      (Required enough space)
 *           - stream_size 
 *    Return - Not NULL (Packet start location)
 *           - NULL (If packet is not found)
 *
 * ***********************************/
void generatePacketStream(const packet* pk, char* stream, uint8_t* stream_size){
    uint8_t index = 0;
    for(uint8_t i = 0; i < 4; ++i){
        stream[index++] = pk->header[i];
    }
    stream[index++] = pk->length;
    stream[index++] = pk->sid;
    stream[index++] = (unsigned char)pk->type;
    uint8_t data_size = pk->length - 8;
    for(uint8_t i = 0; i < data_size; ++i){
        stream[index++] = pk->data[i];
    }
    stream[index++] = pk->checksum;

    *stream_size = index;
}

/***********************************
 * Packet wapper 
 * Argument:
 *    Input&Output  - packet (all parts must be filled except header and checksum)     
 *
 * ***********************************/
void wrapPacket(packet* pk){

    char header[4] = {0xff, 0xff, 0xff, 0x01};
    for(uint8_t i = 0; i < 4; ++i){
        pk->header[i] = header[i];
    }

    uint8_t sum = 0;
    uint8_t data_size = pk->length - 8;

    for(uint8_t i = 0; i < 4; ++i){
        sum += pk->header[i];
    }
    sum += pk->length;
    sum += pk->sid;
    sum += pk->type;
    for(uint8_t i = 0; i < data_size; ++i){
        sum += pk->data[i];
    }

    pk->checksum = ~sum+1;
}


/*
typedef enum{                         // packet의 동작을 나타낸다.
    PACKET_UART_ECHO              =0, // echo 전송을 위한 packet
    PACKET_UART_ECHO_TEST         =1, // echo 전달을 위한 packet
    PACKET_INITIALIZE             =2, // Motor 초기화, 두 모터의 angle을 0으로 설정
    PACKET_HOST_TO_SLAVE_ANG      =3, // PC에서 제어보드로 각도를 보냄
    PACKET_SLAVE_TO_HOST_ANG      =4, // 제어보드에서 PC로 각도를 보냄
    PACKET_HOST_TO_SLAVE_VEL      =5, // PC에서 제어보드로 속도를 보냄
    PACKET_SLAVE_TO_HOST_VEL      =6, // 제어보드에서 PC로 속도를 보냄
    PACKET_ACK                    =7, // 명령이 왔을 경우, 확인 메세지를 보냄
    PACKET_SLAVE_COMPLETE         =8  // 제어보드에서 제어가 완료됨을 알림
} PACKET_TYPE;
*/

/*************************************
 * Get echo string from a packet
 * Argument:
 *    Input  - packet 
 *    Output - echo string 
 *
 * ***********************************/
void getEchoStringFromPacket(const packet* pk, char* str, uint8_t* str_size){

    *str_size = pk->length - 8;
    for(uint8_t i = 0; i < *str_size; ++i)
        str[i] = (char)pk->data[i];
}

/*************************************
 * Get motor velocity from a packet
 * Argument:
 *    Input  - packet 
 *    Output - velocity 
 *
 * ***********************************/
void getVelocityFromPacket(const packet* pk, float* motor_vel){

    *(motor_vel) = *((float*)&(pk->data[0]));
}

void getVelocityFromPacket2(const packet* pk, float* motor1_vel, float* motor2_vel){

    uint8_t data_size = pk->length - 8;

    *(motor1_vel) = *((float*)&(pk->data[0]));
    *(motor2_vel) = *((float*)&(pk->data[data_size/2]));
}
/*************************************
 * Get 2-DOF angles from a packet
 * Argument:
 *    Input  - packet 
 *    Output - positions of 2-DOF motor 
 *
 * ***********************************/
void getAnglesFromPacket(const packet* pk, float* motor1_ang, float* motor2_ang){

    uint8_t data_size = pk->length - 8;

    *(motor1_ang) = *((float*)&(pk->data[0]));
    *(motor2_ang) = *((float*)&(pk->data[data_size/2]));
}


/*************************************
 * Packet generator for sending Complete signal 
 * Argument:
 *    Return - generated packet
 *
 * !!CAUSION!! - packet must be freed
 *
 * ***********************************/
void generatePacketForSlaveComplete(PACKET_TYPE type, packet* pk){

    pk->length = 8+1;
    pk->sid = SID;
    pk->type = PACKET_SLAVE_COMPLETE;

    pk->data[0] = (unsigned char)type;

    wrapPacket(pk);
}


/*************************************
 * Packet generator for sending ACK 
 * Argument:
 *    Return - generated packet
 *
 * !!CAUSION!! - packet must be freed
 *
 * ***********************************/
void generatePacketForAck(packet* pk){

    pk->length = 8;
    pk->sid = SID;
    pk->type = PACKET_ACK;

    wrapPacket(pk);
}


/*************************************
 * Packet generator for sending velocity from Slave to Host
 * Argument:
 *    Input  - velocity for bottom motor 
 *    Return - generated packet
 *
 * !!CAUSION!! - packet and packet.data must be freed
 *
 * ***********************************/
void generatePacketForSlaveToHostVel(float motor1_vel, packet* pk){

    uint8_t data_size = sizeof(float);
    pk->length = data_size + 8;
    pk->sid = SID;
    pk->type = PACKET_SLAVE_TO_HOST_VEL;

    *((float*)&(pk->data[0])) = motor1_vel;

    wrapPacket(pk);
}

/*************************************
 * Packet generator for sending velocity from Slave to Host
 * Argument:
 *    Input  - velocities for bottom motor 
 *    Return - generated packet
 *
 * !!CAUSION!! - packet and packet.data must be freed
 *
 * ***********************************/
void generatePacketForSlaveToHostVel2(float motor1_vel, float motor2_vel,  packet* pk){

    uint8_t data_size = sizeof(float)*2;
    pk->length = data_size + 8;
    pk->sid = SID;
    pk->type = PACKET_SLAVE_TO_HOST_VEL2;

    *((float*)&(pk->data[0])) = motor1_vel;
    *((float*)&(pk->data[data_size/2])) = motor2_vel;

    wrapPacket(pk);
}
/*************************************
 * Packet generator for sending angles from Slave to Host
 * Argument:
 *    Input  - positions of 2-DOF motor 
 *    Return - generated packet
 *
 * !!CAUSION!! - packet and packet.data must be freed
 *
 * ***********************************/
void generatePacketForSlaveToHostAng(float motor1, float motor2, packet* pk){

    uint8_t data_size = sizeof(float)*2;
    pk->length = data_size + 8;
    pk->sid = SID;
    pk->type = PACKET_SLAVE_TO_HOST_ANG;

    *((float*)&(pk->data[0])) = motor1;
    *((float*)&(pk->data[data_size/2])) = motor2;

    wrapPacket(pk);
}


/***********************************
 * Packet generator for ECHO test
 * Argument:
 *    Input  - string 
 *    Return - generated packet
 *
 * !!CAUSION!! - packet and packet.data must be freed
 *
 * ***********************************/
void generatePacketForEchoTest(const char* str, uint8_t str_size, packet* pk){

    pk->length = str_size + 8;
    pk->sid = SID;
    pk->type = PACKET_UART_ECHO_TEST;

    for(uint8_t i = 0; i < str_size; ++i)
        pk->data[i] = str[i];

    wrapPacket(pk);
}

/***********************************
 * Packet generator for ECHO 
 * Argument:
 *    Input  - string 
 *    Return - generated packet
 *
 * !!CAUSION!! - packet and packet.data must be freed
 *
 * ***********************************/
void generatePacketForEcho(const char* str, uint8_t str_size, packet* pk){


    pk->length = str_size + 8;
    pk->sid = SID;
    pk->type = PACKET_UART_ECHO;

    for(uint8_t i = 0; i < str_size; ++i)
        pk->data[i] = str[i];

    wrapPacket(pk);
}
