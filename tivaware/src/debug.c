#include "debug.h"

void printFloat(float target){

    if(target < 0){
        UARTprintf("-");
    }

    int f1 = fabs((int)target);
    int f2 = fabs((int)((target-(int)target)*10));
    int f3 = fabs((int)((target*10-(int)(target*10))*10));
    int f4 = fabs((int)((target*100-(int)(target*100))*10));
    UARTprintf("%d.%d%d%d", f1,f2,f3,f4);
}

void printPacket(packet* pk){

    size_t data_size = pk->length - 8;

    for(int i = 0; i < 4; ++i){
        UARTprintf("%u\t", pk->header[i]);
    }
    UARTprintf("%u\t", pk->length);
    UARTprintf("%u\t", pk->sid);
    UARTprintf("%u\t", pk->type);

    for(int i = 0; i < data_size; ++i){
        UARTprintf("%u\t", pk->data[i]);
    }

    UARTprintf("%u\n", pk->checksum);
}



