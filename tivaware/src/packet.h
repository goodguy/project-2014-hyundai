#ifndef __TIVAWARE_PACKET__
#define __TIVAWARE_PACKET__


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#define MAX_DATA (128)

#ifdef LOWER
#define SID (0)
#elif  LEFT
#define SID (1)
#elif RIGHT
#define SID (2)
#endif

// Packet struct
typedef struct packet_{
    uint8_t header[4];
    uint8_t length;
    uint8_t sid;
    uint8_t type;
    uint8_t data[MAX_DATA];
    uint8_t checksum;
} packet;


// Define PACKET types
typedef enum{                          // packet의 동작을 나타낸다.
    PACKET_UART_ECHO              =0,  // echo 전송을 위한 packet
    PACKET_UART_ECHO_TEST         =1,  // echo 전달을 위한 packet
    PACKET_INITIALIZE             =2,  // Motor 초기화, 두 모터의 angle을 0으로 설정
    PACKET_HOST_TO_SLAVE_ANG      =3,  // PC에서 제어보드로 각도를 보냄
    PACKET_SLAVE_TO_HOST_ANG      =4,  // 제어보드에서 PC로 각도를 보냄
    PACKET_HOST_TO_SLAVE_VEL      =5,  // PC에서 제어보드로 속도를 보냄
    PACKET_HOST_TO_SLAVE_VEL2     =6,  // PC에서 제어보드로 속도를 보냄
    PACKET_SLAVE_TO_HOST_VEL      =7,  // 제어보드에서 PC로 속도를 보냄
    PACKET_SLAVE_TO_HOST_VEL2     =8,  // 제어보드에서 PC로 속도를 보냄
    PACKET_ACK                    =9,  // 명령이 왔을 경우, 확인 메세지를 보냄
    PACKET_SLAVE_COMPLETE         =10,  // 제어보드에서 제어가 완료됨을 알림
} PACKET_TYPE;

// Packet functions
bool isValid(const packet* pk);

// Get Packet from char array
bool getPacket(const char* stream, uint8_t size, packet* packet_temp);

// Split char array from raw char array
const char* splitPacket(const char* stream, const char* header, uint8_t stream_size, uint8_t header_size, uint8_t* result_size);

// Parsing Packet from char array
void parsingPacket(char* stream, uint8_t* stream_size);

// Generate char array from Packet struct
void generatePacketStream(const packet* pk, char* stream, uint8_t* stream_size);
// Wrapping packet with header and tail
void wrapPacket(packet* pk);

// Get echo string from packet struct
void getEchoStringFromPacket(const packet* pk, char* str, uint8_t* str_size);
// Get Velovity from packet struct
void getVelocityFromPacket(const packet* pk, float* motor_vel);
// Get Velovities from packet struct
void getVelocityFromPacket2(const packet* pk, float* motor1_vel, float* motor2_vel);
// Get Angles from packet struct
void getAnglesFromPacket(const packet* pk, float* motor1_ang, float* motor2_ang);

// Generate Slave Complete packet to indicate that slave completed task 
void generatePacketForSlaveComplete(PACKET_TYPE type, packet* pk);
// Generate Slave Ack packet to indicate that slave received packet 
void generatePacketForAck(packet* pk);
// Generate pakcet of sending velocity of motor 
void generatePacketForSlaveToHostVel(float motor1_vel, packet* pk);
// Generate pakcet of sending velocities of motors 
void generatePacketForSlaveToHostVel2(float motor1_vel, float motor2_vel, packet* pk);
// Generate pakcet of sending angles of motors 
void generatePacketForSlaveToHostAng(float motor1, float motor2, packet* pk);
// Generate packet of sending echo reply message
void generatePacketForEchoTest(const char* str, uint8_t str_size, packet* pk);
// Generate packet of sending echo message
void generatePacketForEcho(const char* str, uint8_t str_size, packet* pk);







#endif
