#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/pwm.h"
#include "driverlib/qei.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "packet.h"
#include "debug.h"


// Define Control frequency
#define CONTROL_FREQ (200.0)

// Define PWM frequency
#define PWM_FREQ (100*1000)

// Define resolution of motor's encoder
#define RESOLUTION 1024 
// Define gear-ratio of motor
#define GEAR_RATIO 161 
#define BUFFER_LENGTH 256

/***************************
 * GPIO setting
 *  - PB4 : PWM1A
 *  - PB5 : PWM2A
 *  - PE4 : PWM1B
 *  - PE5 : PWM2B
 *  - PD6 : PHA0
 *  - PF1 : PHB0
 *  - PC5 : PHA1
 *  - PC6 : PHB1
 *  - PD2 : Photo emission1
 *  - PD3 : Photo emission2
 *  - PE1 : Photo interrupt1
 *  - PE2 : Photo interrupt2
 *
 *************************/

// Define state of initialize step using photo-interrupts
typedef enum{
    INIT_IDLE,
    INIT_CW,
    INIT_CCW,
    INIT_FINISH
} InitState;

char uart_recv_buffer[BUFFER_LENGTH]; 
char run_packet_buffer[BUFFER_LENGTH]; 

volatile uint8_t uart_recv_size = 0;
uint8_t run_packet_idx = 0;

float run_time_diff = 1.0/CONTROL_FREQ;

// Ideal angles to be 
float run_ideal_ang_0 = 0;
float run_ideal_ang_1 = 0;

// Ideal velocities to be 
float run_motor_vel_0 = 1.0; // degree per sec
float run_motor_vel_1 = 1.0; // degree per sec

const float run_default_vel = 1.0;

float run_set_ang_0 = 0;
float run_set_ang_1 = 0;

float run_initial_ang_0 = 0;
float run_initial_ang_1 = 0;

bool run_initialize = false;
bool run_initialize_found_0 = false;
bool run_initialize_found_1 = false;

// Define Initial state variables each motor
InitState init_state_0 = INIT_IDLE;
InitState init_state_1 = INIT_IDLE;

// Parsing packet arrived from Odroid
void parsingPacket(char* stream, uint8_t* stream_size);
// Get Direction using QEI Module 
int32_t getDirection(unsigned int selected_qei_base);
// Get Velocity using QEI Module 
float getVelocity(unsigned int selected_qei_base);
// Get Angle using QEI Module 
float getAngle(unsigned int selected_qei_base);

// Main Control Function
void run(void);

// UART Interrupt Handler to communicate with Odroid Board
void UARTIntHandler(void);

// Timer Handler to control motors
void Timer0IntHandler(void);

// Configuration of Timer Module
void TIMER0Configure();
// Configuration of UART Module
void UARTConfigure();
// Configuration of LED Module
void LEDConfigure();
// Configuration of PWM Module
void PWMConfigure();
// Configuration of QEI Module
void QEIModuleConfigure();
// Configuration of Photo-Interrupt Module
void PhotoIntConfigure();

// Send angles to Odroid board
void sendAngles(float angle_0, float angle_1){
    packet pk;
    
    // generate packet to send
    generatePacketForSlaveToHostAng(angle_0, angle_1, &pk);
    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // generate char arrary from packet struct
    generatePacketStream(&pk, packet_stream, &packet_stream_size);

    // send char arrary to odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

// Send angles to Odroid board
void sendVelocities(float vel_0, float vel_1){
    packet pk;
    
    // generate packet to send
    generatePacketForSlaveToHostVel2(vel_0, vel_1, &pk);
    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // generate char arrary from packet struct
    generatePacketStream(&pk, packet_stream, &packet_stream_size);

    // send char arrary to odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

// Send velocity to Odroid board
void sendVelocity(float velocity){
    packet pk;
    
    // generate packet to send
    generatePacketForSlaveToHostVel(velocity, &pk);
    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // generate char arrary from packet struct
    generatePacketStream(&pk, packet_stream, &packet_stream_size);

    // send char arrary to odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

// Send ack to Odroid board
void sendAck(){
    packet pk;
    
    // generate packet to send
    generatePacketForAck(&pk);
    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // generate char arrary from packet struct
    generatePacketStream(&pk, packet_stream, &packet_stream_size);

    // send char arrary to odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

// Send complete to Odroid board
void sendComplete(PACKET_TYPE type){
    packet pk;
    
    // generate packet to send
    generatePacketForSlaveComplete(type, &pk);
    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // generate char arrary from packet struct
    generatePacketStream(&pk, packet_stream, &packet_stream_size);

    // send char arrary to odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

// Send echo reply to Odroid board
void sendEchoTest(char* str, uint8_t str_size){

    packet pk_send;
    
    // generate packet to send
    generatePacketForEchoTest(str, str_size, &pk_send);

    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // generate char arrary from packet struct
    generatePacketStream(&pk_send, packet_stream, &packet_stream_size);

    // send char arrary to odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

// Packet receive handler to process echo packet
void packet_uart_echo_routine(packet* pk){

    char str[128];
    uint8_t str_size = 0;
    getEchoStringFromPacket(pk, str, &str_size);

    packet pk_send;
    
    // generate packet to send
    generatePacketForEchoTest(str, str_size, &pk_send);

    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // generate char arrary from packet struct
    generatePacketStream(&pk_send, packet_stream, &packet_stream_size);

    // send char arrary to odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

// Packet receive handler to process initialization packet
void packet_initialize_routine(packet* pk){
    run_ideal_ang_0 = 0;
    run_ideal_ang_1 = 0;

    // set initialization flag to TRUE
    run_initialize = true;

    // send ack packet
    sendAck();
}

// Packet receive handler to process velocities packet
void packet_host_to_slave_vel2_routine(packet* pk){

    // set velocity variables to be
    getVelocityFromPacket2(pk, &run_motor_vel_0, &run_motor_vel_1);
    sendAck();
}

// Packet receive handler to process angles packet
void packet_host_to_slave_ang_routine(packet* pk){

    // set angles variables to be
    getAnglesFromPacket(pk, &run_ideal_ang_0, &run_ideal_ang_1);
    sendAck();
}

// Classfy packets as per type
void classifyPacket(packet* pk){
    switch(pk->type){
        case PACKET_UART_ECHO:           packet_uart_echo_routine(pk);          break;
        case PACKET_INITIALIZE:          packet_initialize_routine(pk);         break;
        case PACKET_HOST_TO_SLAVE_ANG:   packet_host_to_slave_ang_routine(pk);  break;
        case PACKET_HOST_TO_SLAVE_VEL2:  packet_host_to_slave_vel2_routine(pk);  break;
        default: break;
    }
}

// Parsing received packet stream
void parsingPacket(char* stream, uint8_t* stream_size){

    char header[4] = {0xff, 0xff, 0xff, 0x01};
    uint8_t header_size = 4;

    uint8_t result_size = 0;

    const char* remain_stream = NULL;
    uint8_t remain_stream_size = 0;


    // split char array using header array
    const char* result = splitPacket(stream, header, *stream_size, header_size, &result_size);

    // Handling a first Splited char array
    if(result != NULL){
        packet packet_temp;

        // Get a Packet from char array
        if(getPacket(result, result_size, &packet_temp)){

            // check validity 
            if(isValid(&packet_temp)){
                // Handling packet as per type
                classifyPacket(&packet_temp);
            }
        }
        else{
            // save remain char array
            remain_stream = result;
            remain_stream_size = result_size;
        }
    }
    else{
        // save remain char array
        remain_stream = stream;
        remain_stream_size = *stream_size;
    }

    // Handling another Splited char array
    while((result = splitPacket(NULL, header, 0, header_size, &result_size)) != NULL){
        packet packet_temp;

        // Get a Packet from char array
        if(getPacket(result, result_size, &packet_temp)){

            // check validity 
            if(isValid(&packet_temp)){
                // Handling packet as per type
                classifyPacket(&packet_temp);
            }
        }
        else{
            // save remain char array
            remain_stream = result;
            remain_stream_size = result_size;
        }
    }

    // if remained char array is exist (Not completely arrived packet)
    if(remain_stream != NULL){
        // save remained char array
        *stream_size = remain_stream_size;
        for(int i = 0; i < remain_stream_size; ++i){
            stream[i] = remain_stream[i];
        }
    }
    else{
        *stream_size = 0;
    }
}

// Get direction from QEI module
int32_t getDirection(unsigned int selected_qei_base){
    int32_t dir = QEIDirectionGet(selected_qei_base);
    return dir;
}

// Get velocity from QEI module
float getVelocity(unsigned int selected_qei_base){
    float clock = SysCtlClockGet();
    float veldiv = 16;
    float speed = (float)QEIVelocityGet(selected_qei_base);

    float load = SysCtlClockGet()/4;
    float ppr = RESOLUTION*GEAR_RATIO;
    float edges = 4;

    int32_t dir = getDirection(selected_qei_base);

    
    float rpm = (clock * veldiv * speed * 60) * dir/(load * ppr * edges);


    return rpm;
}

// Get angle from QEI module
float getAngle(unsigned int selected_qei_base){
    uint32_t val = QEIPositionGet(selected_qei_base);
    uint32_t max_angle = RESOLUTION*4*GEAR_RATIO;
    
    float angle; 
    angle = val*360/(float)max_angle;
    if (angle>180) angle-=360; 
    return angle;
}


#ifdef LEFT         // PID gain for LEFT motor controller
float Kp0 = 300.00;
float Kd0 = 0.0;
float Ki0 = 400; 

float Kp1 = 400.00;
float Kd1 = 0.0;
float Ki1 = 500; 

float Ki_gain_weight = 0.999;

int32_t minimum_bw = 10;
int32_t minimum_control_input = 60;

int32_t maximum_control_input = 390;

#elif RIGHT         // PID gain for RIGHT motor controller
float Kp0 = 300.00;
float Kd0 = 0.0;
float Ki0 = 400; 

float Kp1 = 700.00;
float Kd1 = 0.0;
float Ki1 = 600; 

float Ki_gain_weight = 0.999;

int32_t minimum_bw = 10;
int32_t minimum_control_input = 60;

int32_t maximum_control_input = 390;
#endif

// PID controller for motor 0 - panning motor
void pid_controller_for_motor_0(float cur_rpm, float cur_ang, float ideal_ang){

    static float e[3];      // error
    static float u[3];      // control input

    // save previous error and control input
    for (int t = 2; t > 0; --t){
        e[t] = e[t-1];
        u[t] = u[t-1]; 
    }

    int32_t control_input = 0;

    // calculate position error
    e[0] = ideal_ang - cur_ang;

    static float integral = 0;
    integral = integral*Ki_gain_weight + e[0] * run_time_diff;
    float derivative = (e[0] - e[1])/run_time_diff;

    // calculate control input using PID gain
    u[0] = Kp0*e[0] + Ki0*integral + Kd0*derivative;

    // get PWM frequency
    int32_t pwm_period = SysCtlClockGet()/(PWM_FREQ);

    control_input = (int32_t)(u[0]);

    // prevent overflowing boundaries of PWM signal
    if(control_input < -maximum_control_input) control_input = -maximum_control_input;
    if(control_input >  maximum_control_input) control_input =  maximum_control_input;

    if(control_input >  minimum_bw/2 && control_input <  minimum_control_input) control_input =  minimum_control_input;
    if(control_input < -minimum_bw/2 && control_input > -minimum_control_input) control_input = -minimum_control_input;

    // Divide PWM control input into two stages
    uint32_t pwm_width_0 = (uint32_t)(pwm_period/2 + control_input);
    uint32_t pwm_width_1 = (uint32_t)pwm_period - pwm_width_0;

    // Set PWM to control motor
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3,  pwm_width_0);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2,  pwm_width_1);
    PWMSyncUpdate(PWM0_BASE,PWM_GEN_1_BIT);
}


void pid_controller_for_motor_1(float cur_rpm, float cur_ang, float ideal_ang){

    static float e[3];      // error
    static float u[3];      // control input

    // save previous error and control input
    for (int t = 2; t > 0; --t){
        e[t] = e[t-1];
        u[t] = u[t-1]; 
    }

    int32_t control_input = 0;

    // calculate position error
    e[0] = ideal_ang - cur_ang;

    static float integral = 0;
    integral = integral*Ki_gain_weight + e[0] * run_time_diff;
    float derivative = (e[0] - e[1])/run_time_diff;

    // calculate control input using PID gain
    u[0] = Kp1*e[0] + Ki1*integral + Kd1*derivative;

    // get PWM frequency
    int32_t pwm_period = SysCtlClockGet()/(PWM_FREQ);

    control_input = (int32_t)(u[0]);

    // prevent overflowing boundaries of PWM signal
    if(control_input < -maximum_control_input) control_input = -maximum_control_input;
    if(control_input >  maximum_control_input) control_input =  maximum_control_input;

    if(control_input >  minimum_bw/2 && control_input <  minimum_control_input) control_input =  minimum_control_input;
    if(control_input < -minimum_bw/2 && control_input > -minimum_control_input) control_input = -minimum_control_input;

    // Divide PWM control input into two stages
    uint32_t pwm_width_0 = (uint32_t)(pwm_period/2 + control_input);
    uint32_t pwm_width_1 = (uint32_t)pwm_period - pwm_width_0;

    // Set PWM to control motor
#ifdef LEFT
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_5,  pwm_width_1);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4,  pwm_width_0);
#elif  RIGHT
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_5,  pwm_width_0);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4,  pwm_width_1);
#endif
    PWMSyncUpdate(PWM0_BASE,PWM_GEN_1_BIT);
}


// Initialize motor using Photo-interrupt sensor
void initialize_motor(float ang0, float ang1){

    const float d_theta = 10;

    static uint32_t init_step_0 = 0;
    static uint32_t init_step_1 = 0;

    // set initialization maximum step 
    const uint32_t max_init_step = 3;

    // set initialization velocity each step
    const float init_step_velocities_cw[] = {2.0, 0.1, 0.05};
    const float init_step_velocities_ccw[] = {2.0, 0.2, 0.1};

    if(run_initialize_found_0){
        run_initialize_found_0 = false;
        run_set_ang_0 = ang0;
    }

    if(run_initialize_found_1){
        run_initialize_found_1 = false;
        run_set_ang_1 = ang1;
    }

    
    // for panning motor
    switch(init_state_0){
        case INIT_IDLE:
            {   
                // start initialization step
                if(run_initialize){
                    // read Photo-interrupt value
                    int32_t photo_val = GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_1);
                    init_step_0 = 0;

                    // set motor velocity according to photo-interrupt value 
                    if(photo_val == 0){
                        run_motor_vel_0 = init_step_velocities_cw[init_step_0];
                        init_state_0 = INIT_CW;
                    }
                    else{
                        run_motor_vel_0 = init_step_velocities_ccw[init_step_0];
                        init_state_0 = INIT_CCW;
                    }
                }
            }
            break;
        case INIT_CW:
            {
                // read Photo-interrupt value
                int32_t photo_val = GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_1);

                // set motor velocity according to photo-interrupt value 
                if(init_step_0 < max_init_step){
                    if(photo_val != 0){
                        init_state_0 = INIT_CCW;
                        run_motor_vel_0 = init_step_velocities_ccw[init_step_0];
                    }
                    else{
                        run_ideal_ang_0 = ang0+d_theta;
                    }
                }
                else{
                    run_ideal_ang_0 = 0;
                    init_state_0 = INIT_FINISH;

                }
            }
            break;
        case INIT_CCW:
            {
                // read Photo-interrupt value
                int32_t photo_val = GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_1);

                // set motor velocity according to photo-interrupt value 
                if(photo_val == 0){
                    init_state_0 = INIT_CW;
                    run_motor_vel_0 = init_step_velocities_cw[init_step_0];
                    init_step_0++;
                }
                else{
                    run_ideal_ang_0 = ang0-d_theta;
                }
            }
            break;
        case INIT_FINISH:
            {
                if(init_state_0 == INIT_FINISH && init_state_1 == INIT_FINISH){
                    run_motor_vel_0 = run_default_vel;
                    run_motor_vel_1 = run_default_vel;
                    init_state_0 = INIT_IDLE;
                    init_state_1 = INIT_IDLE;
                    run_initialize = false;
                    sendComplete(PACKET_INITIALIZE);
                }
            }
            break;
    }

    // for tilting motor
    switch(init_state_1){
        case INIT_IDLE:
            {
                // start initialization step
                if(run_initialize){
                    // read Photo-interrupt value
                    int32_t photo_val = GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_2);
                    init_step_1 = 0;
                    // set motor velocity according to photo-interrupt value 
                    if(photo_val == 0){
                        run_motor_vel_1 = init_step_velocities_cw[init_step_1];
                        init_state_1 = INIT_CW;
                    }
                    else{
                        run_motor_vel_1 = init_step_velocities_ccw[init_step_1];
                        init_state_1 = INIT_CCW;
                    }
                }
            }
            break;
        case INIT_CW:
            {
                // read Photo-interrupt value
                int32_t photo_val = GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_2);

                // set motor velocity according to photo-interrupt value 
                if(init_step_1 < max_init_step){
                    if(photo_val != 0){
                        init_state_1 = INIT_CCW;
                        run_motor_vel_1 = init_step_velocities_ccw[init_step_1];
                    }
                    else{
                        run_ideal_ang_1 = ang1+d_theta;
                    }
                }
                else{
                    run_ideal_ang_1 = 0;
                    init_state_1 = INIT_FINISH;

                }
            }
            break;
        case INIT_CCW:
            {
                // read Photo-interrupt value
                int32_t photo_val = GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_2);

                // set motor velocity according to photo-interrupt value 
                if(photo_val == 0){
                    init_state_1 = INIT_CW;
                    run_motor_vel_1 = init_step_velocities_cw[init_step_1];
                    init_step_1++;
                }
                else{
                    run_ideal_ang_1 = ang1-d_theta;
                }
            }
            break;
        case INIT_FINISH:
            {
                if(init_state_0 == INIT_FINISH && init_state_1 == INIT_FINISH){
                    run_motor_vel_0 = run_default_vel;
                    run_motor_vel_1 = run_default_vel;
                    init_state_0 = INIT_IDLE;
                    init_state_1 = INIT_IDLE;
                    run_initialize = false;
                    sendComplete(PACKET_INITIALIZE);
                }
            }
            break;
    }

}


// Main Control Function
void run(void){

    // stacking received char array 
    for(uint8_t i = 0; i < uart_recv_size; ++i){
        run_packet_buffer[run_packet_idx+i] = uart_recv_buffer[i];
    }
    run_packet_idx += uart_recv_size;
    uart_recv_size = 0;

    // parsing packet from received char array
    parsingPacket(run_packet_buffer, &run_packet_idx);

    // get motor velocity and angle for motor 0
    float rpm0 = getVelocity(QEI0_BASE);
    float ang0 = getAngle(QEI0_BASE) - run_initial_ang_0;

    // get motor velocity and angle for motor 1
#ifdef  LEFT
    float rpm1 = -getVelocity(QEI1_BASE);
    float ang1 = -getAngle(QEI1_BASE) - run_initial_ang_1;
#elif   RIGHT
    float rpm1 = getVelocity(QEI1_BASE);
    float ang1 = getAngle(QEI1_BASE) - run_initial_ang_1;
#endif

    // conduct initialization step if odroid initilize packet is arrived 
    initialize_motor(ang0, ang1);


    // divide ideal angles with ideal velocities
    const float ang_per_step0 = run_motor_vel_0/CONTROL_FREQ;
    const float ang_per_step1 = run_motor_vel_1/CONTROL_FREQ;

    // set ideal angles for PID position control with velocity control
    if(fabs(run_set_ang_0 - run_ideal_ang_0) < fabs(ang_per_step0))   
        run_set_ang_0 = run_ideal_ang_0;
    else if((run_ideal_ang_0 - run_set_ang_0) > 0)
        run_set_ang_0 += ang_per_step0;
    else if((run_ideal_ang_0 - run_set_ang_0) < 0)
        run_set_ang_0 -= ang_per_step0;

    // set ideal angles for PID position control with velocity control
    if(fabs(run_set_ang_1 - run_ideal_ang_1) < fabs(ang_per_step1))   
        run_set_ang_1 = run_ideal_ang_1;
    else if((run_ideal_ang_1 - run_set_ang_1) > 0)
        run_set_ang_1 += ang_per_step1;
    else if((run_ideal_ang_1 - run_set_ang_1) < 0)
        run_set_ang_1 -= ang_per_step1;


    // pid control for motor 0
    pid_controller_for_motor_0(rpm0, ang0, run_set_ang_0);
    // pid control for motor 1
    pid_controller_for_motor_1(rpm1, ang1, run_set_ang_1);

    float vel0 = rpm0 * 360.0 / 60.0;
    float vel1 = rpm1 * 360.0 / 60.0;

    // send Status Packet To Host
    sendVelocities(vel0, vel1);
    sendAngles(ang0, ang1);
}

int main(void){

    // enable Floating point unit
    FPUEnable();
    FPULazyStackingEnable();

    // set board clock to 80 MHz
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    // configuration of LED
    LEDConfigure();

    // configuration of UART to communicate with Odroid board
    UARTConfigure();

    UARTprintf("START MOTOR PWM PROGRAM\n");

    // configuration to measure motor status (Pos, Vel)
    QEIModuleConfigure();
    
    // configuration for PWM module to operate motor
    PWMConfigure();

    // configuration to control with precise frequency
    TIMER0Configure();

    // configuration of Photo-interrupt GPIO 
    PhotoIntConfigure();

    IntMasterEnable();

    while(1){

    }
}


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//  HW DEPENDENT PART //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

// GPIO Interrupt handler for Photo-interrupt sensor
void GPIOIntHandler(void){
    IntMasterDisable();
    uint32_t ui32Status;
    ui32Status = GPIOIntStatus(GPIO_PORTE_BASE, true);
    GPIOIntClear(GPIO_PORTE_BASE, ui32Status);

    if(ui32Status & GPIO_INT_PIN_1){
        // if edges is detected
        if(init_state_0 == INIT_CW){
            run_initialize_found_0 = true;
            run_initial_ang_0 = getAngle(QEI0_BASE);
        }

    }
    if(ui32Status & GPIO_INT_PIN_2){
        // if edges is detected
        if(init_state_1 == INIT_CW){
            run_initialize_found_1 = true;
#ifdef LEFT
            run_initial_ang_1 = -getAngle(QEI1_BASE);
#elif  RIGHT
            run_initial_ang_1 = getAngle(QEI1_BASE);
#endif
        }
    }

    sendAck();
    IntMasterEnable();
}


// UART Interrupt handler to communicate with Odoroid board
void UARTIntHandler(void){

    uint32_t ui32Status;
    // Get the interrrupt status.
    ui32Status = UARTIntStatus(UART0_BASE, true);

    // Clear the asserted interrupts.
    UARTIntClear(UART0_BASE, ui32Status);
    IntMasterDisable();
    // Loop while there are characters in the receive FIFO.

    while(UARTCharsAvail(UART0_BASE)){
        unsigned char input = UARTCharGetNonBlocking(UART0_BASE);

        if(uart_recv_size < BUFFER_LENGTH){
            uart_recv_buffer[uart_recv_size] = input;
            uart_recv_size = uart_recv_size + 1;
        }
    }

    IntMasterEnable();
}

// Timer interrupt handler to control motor
void Timer0IntHandler(void){

    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    IntMasterDisable();
    // execute run() control function
    run();
    IntMasterEnable();

}

// Configuration for timer 0 to control motor
void TIMER0Configure(){

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet()/CONTROL_FREQ);

    IntEnable(INT_TIMER0A);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    TimerEnable(TIMER0_BASE, TIMER_A);

}

// Configuration for UART to commnunicate with Odroid board
void UARTConfigure(){
    // Enable the peripherals used by this example.
    // GPIO PA[1:0]'s default state is UART0. 
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Set GPIO A0 and A1 as UART pins.
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Use the internal 16MHz oscillator as the UART clock source.
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    // Initialize the UART for console I/O.
    //UARTStdioConfig(0, 115200, 16000000);
    UARTStdioConfig(0, 1000000, 16000000);
    // Set baudrate for seral communication.

    // Enable the UART interrupt.
    IntEnable(INT_UART0);
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);

}

void LEDConfigure(){

    // Enable the GPIO port that is used for the on-board LED.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Enable the GPIO pins for the LED (PF2).
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);

}

// Configuration for PWM to operate motors 
void PWMConfigure(){
    uint32_t period = SysCtlClockGet()/(PWM_FREQ);

    // We will use GPIO E,B as PWM0. 
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    GPIOPinConfigure(GPIO_PB4_M0PWM2);
    GPIOPinConfigure(GPIO_PB5_M0PWM3);
    GPIOPinConfigure(GPIO_PE4_M0PWM4);
    GPIOPinConfigure(GPIO_PE5_M0PWM5);

    GPIOPinTypePWM(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);
    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_4 | GPIO_PIN_5);

    PWMClockSet(PWM0_BASE, PWM_SYSCLK_DIV_1);
    // PWM Count-Down Mode 
    PWMGenConfigure(PWM0_BASE,PWM_GEN_1,PWM_GEN_MODE_DOWN|PWM_GEN_MODE_GEN_SYNC_GLOBAL);
    PWMGenConfigure(PWM0_BASE,PWM_GEN_2,PWM_GEN_MODE_DOWN|PWM_GEN_MODE_GEN_SYNC_GLOBAL);

    // PWM_GEN_1 -> PWM_OUT_2,3
    // PWM_GEN_2 -> PWM_OUT_4,5
    PWMGenPeriodSet(PWM0_BASE,PWM_GEN_1, period);
    PWMGenPeriodSet(PWM0_BASE,PWM_GEN_2, period);

    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_2, 1);
    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_3, 1);

    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_4, 1);
    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_5, 1);

    PWMGenEnable(PWM0_BASE,PWM_GEN_1);
    PWMGenEnable(PWM0_BASE,PWM_GEN_2);

    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_2,PWM_OUTPUT_MODE_SYNC_GLOBAL);
    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_3,PWM_OUTPUT_MODE_SYNC_GLOBAL);
    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_4,PWM_OUTPUT_MODE_SYNC_GLOBAL);
    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_5,PWM_OUTPUT_MODE_SYNC_GLOBAL);

    PWMSyncUpdate(PWM0_BASE,PWM_GEN_1_BIT);
    PWMSyncUpdate(PWM0_BASE,PWM_GEN_2_BIT);

    PWMOutputState(PWM0_BASE,PWM_OUT_2_BIT,true);
    PWMOutputState(PWM0_BASE,PWM_OUT_3_BIT,true);
    PWMOutputState(PWM0_BASE,PWM_OUT_4_BIT,true);
    PWMOutputState(PWM0_BASE,PWM_OUT_5_BIT,true);

}

// Configuration for PWM to operate motors 
void QEIModuleConfigure(){
    // Related to Encoder.
    // Detect angle and speed.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI1);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    GPIOPinConfigure(GPIO_PC5_PHA1);
    GPIOPinConfigure(GPIO_PC6_PHB1);
    GPIOPinConfigure(GPIO_PC4_IDX1);

    GPIOPinConfigure(GPIO_PD6_PHA0);
    //GPIOPinConfigure(GPIO_PD7_PHB0);    // NOT WORKING!!
    GPIOPinConfigure(GPIO_PF1_PHB0);
    GPIOPinConfigure(GPIO_PF4_IDX0);
    
    GPIOPinTypeQEI(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6);
    GPIOPinTypeQEI(GPIO_PORTD_BASE, GPIO_PIN_6 );
    GPIOPinTypeQEI(GPIO_PORTF_BASE, GPIO_PIN_4 | GPIO_PIN_1);

    uint32_t max_angle = RESOLUTION*4*GEAR_RATIO;// max value counted in encoder. 
    uint32_t vel_period = SysCtlClockGet()/(4);

    // QEI_CONFIG_CAPTURE_A_B: Count on chA and chB edges
    // QEI_CONFIG_NO_RESET: Do not reset on index pulse
    // QEI_CONFIG_QUADRATURE: chA and chB are quadratures
    // QEI_CONFIG_NO_SWAP: Do not swap chA and chB
    QEIConfigure(QEI0_BASE, QEI_CONFIG_CAPTURE_A_B|QEI_CONFIG_NO_RESET|QEI_CONFIG_QUADRATURE|QEI_CONFIG_NO_SWAP, max_angle);
    QEIConfigure(QEI1_BASE, QEI_CONFIG_CAPTURE_A_B|QEI_CONFIG_NO_RESET|QEI_CONFIG_QUADRATURE|QEI_CONFIG_NO_SWAP, max_angle);
    QEIVelocityConfigure(QEI0_BASE, QEI_VELDIV_16, vel_period);
    QEIVelocityConfigure(QEI1_BASE, QEI_VELDIV_16, vel_period);

    QEIVelocityEnable(QEI0_BASE);
    QEIVelocityEnable(QEI1_BASE);

    QEIEnable(QEI0_BASE);
    QEIEnable(QEI1_BASE);

}

// Configuration for Photo-interrupt sensors to read its' values
void PhotoIntConfigure(void){

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2);
    GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_2 | GPIO_PIN_3);

    GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2, GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD_WPU);
    GPIOPadConfigSet(GPIO_PORTD_BASE, GPIO_PIN_2 | GPIO_PIN_3, GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD_WPU);


    GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2, GPIO_RISING_EDGE);
    GPIOIntRegister(GPIO_PORTE_BASE, GPIOIntHandler);

    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_2|GPIO_PIN_3,  GPIO_PIN_2|GPIO_PIN_3);

    GPIOIntEnable(GPIO_PORTE_BASE, GPIO_INT_PIN_1 | GPIO_INT_PIN_2);

    IntEnable(INT_GPIOE);

}
