#ifndef __TIVAWARE__DEBUG__
#define __TIVAWARE__DEBUG__

#include <math.h>
#include <stdint.h>

#include "utils/uartstdio.h"
#include "packet.h"

// Visualization
void printFloat(float target);
void printPacket(packet* pk);

#endif
