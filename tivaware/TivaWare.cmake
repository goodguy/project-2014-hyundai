
set(TOOLCHAIN_ROOT_PATH ~/arm-tivaware-eabi/)


################ Do not touch below if you don't need fix ############
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)

# Setting cross compiler 
set(CMAKE_TOOLCHAIN_PREFIX  arm-tivaware-eabi)
set(CMAKE_C_COMPILER    ${CMAKE_TOOLCHAIN_PREFIX}-gcc)
set(CMAKE_CXX_COMPILER  ${CMAKE_TOOLCHAIN_PREFIX}-g++)

# Setting root path of Cross compiler
set(CMAKE_FIND_ROOT_PATH  ${TOOLCHAIN_ROOT_PATH}/arm-none-eabi/sysroot )

# Setting OpenOCD root path to automatically write binary file
set(OPENOCD_ROOT_PATH /usr/share/openocd/)

set(CMAKE_C_FLAGS "-march=armv7e-m -mtune=cortex-m4 -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -mthumb -ffunction-sections -fdata-sections -MD -std=c99 -Wall -pedantic -DPART_TM4C123GH6PM -Dgcc -g -Wl,--gc-sections " CACHE STRING "c flags")
set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} CACHE STRING "c++ flags")

